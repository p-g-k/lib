//---------------------------------------------------------------------------
//!
//!	@file	App.h
//!	@brief	アプリケーション
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#pragma once

// --------------------------------------------------------------------------
// include
// --------------------------------------------------------------------------
#include "../Lib/Lib.h"

#include "App.System.h"
#include "App.Input.h"
#include "App.Game.Character.h"
#include "App.Game.Player.h"
#include "App.Scene.h"

namespace App
{
#ifdef	_DEBUG
	extern Lib::FontManager::Font* pDebugFont;
#endif	// ~#if _DEBUG

	//	シーン管理クラス
	extern Lib::SceneManager::SceneManager* pSceneManager;
	//	インプット
	extern App::Input::Input* pInput;

	//!	シーン
	namespace ESceneMode
	{
		const int TEST = -1;
		const int TITLE = 0;
		const int GAME = 1;
		const int CREAR = 2;
	}

	//!	キーコード
	namespace EKeyCode
	{
		const int CROSS = 0;
		const int CIRCLE = 1;
		const int SQUARE = 2;
		const int TRIANGLE = 3;
		const int L1 = 4;
		const int R1 = 5;
		const int L2 = 6;
		const int R2 = 7;
		const int SELECT = 8;
		const int START = 9;
		const int UP = 10;
		const int DOWN = 11;
		const int LEFT = 12;
		const int RIGHT = 13;
	}

	//!	プレイヤー
	namespace EPlayer
	{
		//!	状態
		namespace EState
		{
			const int WAIT = 0;
			const int MOVE = 1;
		}

		//!	モーション
		namespace EMotion
		{
			const int WAIT = 0;
			const int WALK = 1;
			const int RUN = 2;
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================