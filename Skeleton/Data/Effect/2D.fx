//---------------------------------------------------------------------------
//
//	2D.fx
//	エフェクトファイル
//
//---------------------------------------------------------------------------

//===========================================================================
//	テクスチャー
//===========================================================================
//	環境マップ
texture DiffuseMap;
sampler DiffuseSamp = sampler_state
{
	Texture		= <DiffuseMap>;
	MinFilter	= LINEAR;
	MagFilter	= LINEAR;
	MipFilter	= NONE;

	AddressU	= Wrap;
	AddressV	= Wrap;
};

//===========================================================================
//	頂点フォーマット
//===========================================================================
// 入力
struct VS_INPUT
{
	float4 Pos		: POSITION;
	float4 Color	: COLOR0;
	float2 Tex		: TEXCOORD0;
};

//===========================================================================
//	頂点フォーマット
//===========================================================================
// 入力
struct VS_OUTPUT
{
	float4 Pos		: POSITION;
	float4 Color	: COLOR0;
	float2 Tex		: TEXCOORD0;
};

//===========================================================================
//	頂点シェーダー
//===========================================================================
// 基本
VS_OUTPUT VS_Basic( VS_INPUT In )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;
	
	Out.Pos			= In.Pos;
	Out.Tex			= In.Tex;
	Out.Color.rgba	= In.Pos;
	return Out;
}
//===========================================================================
//	ピクセルシェーダー
//===========================================================================
// 基本
float4	PS_Basic( VS_OUTPUT Out) : COLOR
{
	float4 fDiff = Out.Color;
	//return float4(Out.Color.r, Out.Color.g, 0.0f, 1.0f);
	return fDiff;// * tex2D(DiffuseSamp, Tex);
}

//===========================================================================
//	テクニック
//===========================================================================
// スプライト
technique Basic
{
	pass P0	// 線形合成
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;
		
		VertexShader	= compile vs_3_0 VS_Basic();
		PixelShader		= compile ps_2_0 PS_Basic();
	}
	pass P1	// 加算合成
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = One;
		ZWriteEnable     = true;
		
		VertexShader	= compile vs_3_0 VS_Basic();
		PixelShader		= compile ps_2_0 PS_Basic();
	}
	pass P2	// 減算合成
	{
		AlphaBlendEnable = true;
		BlendOp          = RevSubtract;
		SrcBlend         = SrcAlpha;
		DestBlend        = One;
		ZWriteEnable     = true;
		
		VertexShader	= compile vs_3_0 VS_Basic();
		PixelShader		= compile ps_2_0 PS_Basic();
	}
	pass P3 // 乗算合成
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = Zero;
		DestBlend        = SrcColor;
		ZWriteEnable     = true;
		
		VertexShader	= compile vs_3_0 VS_Basic();
		PixelShader		= compile ps_2_0 PS_Basic();
	}
}

//============================================================================
//	END OF FILE
//============================================================================