//---------------------------------------------------------------------------
//
//	hlsl.fx
//	エフェクトファイル
//
//---------------------------------------------------------------------------

//===========================================================================
//	グローバル変数
//===========================================================================
// ワールド変換行列
float4x4 mWorld;
// ビュー、射影変換行列
float4x4 mViewProjection;

// メッシュの環境光
float4 Ambient = { 0.2f, 0.2f, 0.2f, 1.0f };
// メッシュの拡散光
float4 Diffuse = { 0.8f, 0.8f, 0.8f, 1.0f };
// 透過値
float  fAlpha = 1.0f;

// 環境光
float4 lAmbient = { 0.3f, 0.3f, 0.3f, 1.0f };

//---------------------------------------------------------------------------
//	平行光源関連
//---------------------------------------------------------------------------
// 方向
float3 vLightDir = { 0.5f, -0.5f, 0.0f };
// 色
float4 DirLightColor = { 0.6f, 0.6f, 0.6f, 0.0f };

//---------------------------------------------------------------------------
//	点光源関連
//---------------------------------------------------------------------------
// 個数
int	   iPLightNum = 0;
// 位置
float3 vPLightPos[8];
// 色
float3 PLightColor[8];
// 距離
float  fPLightDist[8];

//---------------------------------------------------------------------------
//	フォグ関連
//---------------------------------------------------------------------------
float	fFogNear = 600.0f;
float	fFogFar  = 800.0f;
float4	FogColor = { 1.0f, 1.0f, 1.0f, 1.0f };

//---------------------------------------------------------------------------
//	バンプマッピング関連
//---------------------------------------------------------------------------
float3	vEyePos;

//---------------------------------------------------------------------------
//	シャドウ関連
//---------------------------------------------------------------------------
// ライト用ビュー、射影変換行列
float4x4 mlViewProjection;
// 影の度合い
float fShadowRate = 0.3f;

//---------------------------------------------------------------------------
//	トゥーン関連
//---------------------------------------------------------------------------
float	fOutlineSize = 3.0f;
float4	OutlineColor = { 0.0f, 0.0f, 0.0f, 1.0f };

//===========================================================================
//	テクスチャー
//===========================================================================
//	環境マップ
texture DiffuseMap;
sampler DiffuseSamp = sampler_state
{
	Texture		= <DiffuseMap>;
	MinFilter	= Linear;
	MagFilter	= Linear;
	MipFilter	= None;

	AddressU	= Wrap;
	AddressV	= Wrap;
};

//	法線マップ
texture NormalMap;
sampler NormalSamp = sampler_state
{
	Texture		= <NormalMap>;
	MinFilter	= Linear;
	MagFilter	= Linear;
	MipFilter	= None;

	AddressU	= Wrap;
	AddressV	= Wrap;
};

//	シャドウマップ
texture ShadowMap;
sampler ShadowSamp = sampler_state
{
	Texture		= <ShadowMap>;
	MinFilter	= Linear;
	MagFilter	= Linear;
	MipFilter	= None;
	
	AddressU	= Border;
	AddressV	= Border;
	BorderColor	= 0xffffffff;
};

//===========================================================================
//	頂点フォーマット
//===========================================================================
// 入力
struct VS_INPUT
{
	float4 Pos		: POSITION;
	float3 Normal	: NORMAL;
	float2 Tex		: TEXCOORD0;
};

// 出力
struct VS_OUTPUT
{
	float4 Pos		: POSITION;
	float4 Color	: COLOR0;
	float2 Tex		: TEXCOORD0;
	float  Fog		: TEXCOORD1;//FOG;
	float3 Normal	: TEXCOORD2;
};

//===========================================================================
//	関数群
//===========================================================================
//---------------------------------------------------------------------------
//	平行光
//---------------------------------------------------------------------------
inline float4 LightDir(float3 L, float3 N)
{
	return DirLightColor * max(0, dot(-L, N));
}

//---------------------------------------------------------------------------
//	フォグ
//---------------------------------------------------------------------------
inline float Fog(float Z)
{
	return saturate((fFogFar - Z) / (fFogFar - fFogNear));
}

//===========================================================================
//	頂点シェーダー
//===========================================================================
// 基本
VS_OUTPUT VS_Basic( VS_INPUT In )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;
	
	Out.Pos = mul(In.Pos, mWorld);
	Out.Pos	= mul(Out.Pos, mViewProjection);
	Out.Tex	= In.Tex;
	
	float3 N = mul(In.Normal, mWorld);
	N = normalize(N);
	
	float3 L = normalize(vLightDir);
	
	Out.Color = lAmbient + LightDir(L, N);
	Out.Color.a = fAlpha;
	Out.Normal = In.Normal;
	
	Out.Fog = Fog(Out.Pos.z);
	
	return Out;
}

//===========================================================================
//	ピクセルシェーダー
//===========================================================================
// 基本
float4	PS_Basic( VS_OUTPUT In ) : COLOR
{
	float4 Color;
	
	Color = In.Color * tex2D(DiffuseSamp, In.Tex);
	
	Color = Color * In.Fog + FogColor * (1.0f - In.Fog);
	return Color;//float4(In.Normal, 1.0f);
}

//===========================================================================
//	テクニック
//===========================================================================
// 基本
technique Basic
{
	pass P0	// 線形合成
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;
		
		VertexShader	= compile vs_3_0 VS_Basic();
		PixelShader		= compile ps_3_0 PS_Basic();
		
		//FogEnable		= true;
		//FogVertexMode	= Linear;
		//FogColor		= 0xffffffff;
	}
	pass P1	// 加算合成
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = One;
		ZWriteEnable     = true;
		
		VertexShader	= compile vs_2_0 VS_Basic();
		PixelShader		= compile ps_2_0 PS_Basic();
	}
	pass P2	// 減算合成
	{
		AlphaBlendEnable = true;
		BlendOp          = RevSubtract;
		SrcBlend         = SrcAlpha;
		DestBlend        = One;
		ZWriteEnable     = true;
		
		VertexShader	= compile vs_2_0 VS_Basic();
		PixelShader		= compile ps_2_0 PS_Basic();
	}
	pass P3 // 乗算合成
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = Zero;
		DestBlend        = SrcColor;
		ZWriteEnable     = true;
		
		VertexShader	= compile vs_2_0 VS_Basic();
		PixelShader		= compile ps_2_0 PS_Basic();
	}
}

//===========================================================================
//	頂点フォーマット
//===========================================================================
// 点光源
struct VS_POINT
{
	float4 Pos		: POSITION;
	float4 Color	: COLOR0;
	float2 Tex		: TEXCOORD0;
	float4 World	: TEXCOORD1;
	float3 Normal	: TEXCOORD2;
	float  Fog		: TEXCOORD3;
};

//===========================================================================
//	頂点シェーダー
//===========================================================================
// 点光源
VS_POINT VS_PointLight( VS_INPUT In )
{
	VS_POINT Out = (VS_POINT)0;
	
	Out.World = mul(In.Pos, mWorld);
	Out.Pos	= mul(Out.World, mViewProjection);
	Out.Tex	= In.Tex;
	
	Out.Normal = mul(In.Normal, mWorld);
	Out.Normal = normalize(Out.Normal);
	
	float3 L = normalize(vLightDir);
	float3 N = Out.Normal;
	
	Out.Color = lAmbient + LightDir(L, N);
	Out.Color.a = fAlpha;
	
	Out.Fog = Fog(Out.Pos.z);
	
	return Out;
}

//===========================================================================
//	ピクセルシェーダー
//===========================================================================
// 点光源
float4 PS_PointLight( VS_POINT In ) : COLOR
{
	float4 Color = In.Color;
	
	Color = Color * tex2D(DiffuseSamp, In.Tex);
	
	for(int i=0; i < iPLightNum; i++)
	{
		float3 L = vPLightPos[i] - In.World.xyz;
		float fDist = length(L);
		if( fDist < fPLightDist[i] )
		{
			L = normalize(L);
			float fLight = dot(L, In.Normal) * (1.0f - fDist / fPLightDist[i]);
			Color.rgb += PLightColor[i] * max(0, fLight);
		}
	}
	
	Color = Color * In.Fog + FogColor * (1.0f - In.Fog);
	
	return Color;
}

//===========================================================================
//	テクニック
//===========================================================================
// 点光源
technique PointLight
{
	pass P0
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;
		
		VertexShader	= compile vs_2_0 VS_PointLight();
		PixelShader		= compile ps_3_0 PS_PointLight();
	}
}

//===========================================================================
//	頂点フォーマット
//===========================================================================
// 点光源
struct VS_BUMP
{
	float4 Pos		: POSITION;
	float4 Color	: COLOR0;
	float2 Tex		: TEXCOORD0;
	float3 Eye		: TEXCOORD1;
	float3 Light	: TEXCOORD2;
	float  Fog		: TEXCOORD3;
};

//===========================================================================
//	頂点シェーダー
//===========================================================================
// バンプマッピング
VS_BUMP VS_BumpMap( VS_INPUT In )
{
	VS_BUMP Out = (VS_BUMP)0;
	
	In.Pos = mul(In.Pos, mWorld);
	Out.Pos	= mul(In.Pos, mViewProjection);
	Out.Tex	= In.Tex;
	
	float3 N = mul(In.Normal, mWorld);
	N = normalize(N);
	
	Out.Color = lAmbient;
	Out.Color.a = fAlpha;
	
	float3 T = { 0.0f, 1.0f, 0.01f };
	T = cross(N, T);
	T = normalize(T);
	
	float3 B = cross(N, T);
	B = normalize(B);
	
	float3 E = vEyePos - In.Pos.xyz;
	Out.Eye.x = dot(E, T);
	Out.Eye.y = dot(E, B);
	Out.Eye.z = dot(E, N);
	
	float3 L = vLightDir;
	Out.Light.x = dot(L, T);
	Out.Light.y = dot(L, B);
	Out.Light.z = dot(L, N);
	
	Out.Fog = Fog(Out.Pos.z);
	
	return Out;
}

//===========================================================================
//	ピクセルシェーダー
//===========================================================================
// バンプマッピング
float4 PS_BumpMap( VS_BUMP In ) : COLOR
{
	float3 N = 2.0f * tex2D(NormalSamp, In.Tex).xyz - 1.0f;
	float3 L = normalize(In.Light);
	float3 R = reflect(-normalize(In.Eye), N);
	float4 Color = In.Color + LightDir(L, N);
	
	Color = Color * tex2D(DiffuseSamp, In.Tex) + 0.3f * pow(max(0, dot(R, L)), 8);
	
	Color = Color * In.Fog + FogColor * (1.0f - In.Fog);
	return Color;
}

//===========================================================================
//	テクニック
//===========================================================================
// バンプマッピング
technique BumpMap
{
	pass P0
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;
		
		VertexShader	= compile vs_2_0 VS_BumpMap();
		PixelShader		= compile ps_2_0 PS_BumpMap();
	}
}

//===========================================================================
//	頂点フォーマット
//===========================================================================
// 影
struct VS_SHADOW
{
	float4 Pos		: POSITION;
	float4 Color	: COLOR0;
	float2 Tex		: TEXCOORD0;
	float4 ShadowUV	: TEXCOORD1;
	float4 Depth	: TEXCOORD2;
	float  Fog		: TEXCOORD3;
};

//===========================================================================
//	頂点シェーダー
//===========================================================================
// シャドウマップ
VS_SHADOW VS_ShadowMap( VS_INPUT In )
{
	VS_SHADOW Out = (VS_SHADOW)0;
	
	Out.Pos	= mul(In.Pos, mWorld);
	Out.Pos	= mul(Out.Pos, mlViewProjection);
	
	Out.ShadowUV = Out.Pos.zzzw;
	return Out;
}

// 影
VS_SHADOW VS_Shadow( VS_INPUT In )
{
	VS_SHADOW Out = (VS_SHADOW)0;
	
	In.Pos = mul(In.Pos, mWorld);
	Out.Pos	= mul(In.Pos, mViewProjection);
	Out.Tex	= In.Tex;
	
	float3 L = normalize(vLightDir);
	float3 N = mul(In.Normal, mWorld);
	N = normalize(N);
	
	Out.Color = lAmbient + LightDir(L, N);
	Out.Color.a = fAlpha;
	
	Out.Depth = mul(In.Pos, mlViewProjection);
	Out.ShadowUV = Out.Depth;
	Out.ShadowUV.y = -Out.ShadowUV.y;
	Out.ShadowUV.xy = Out.ShadowUV.xy * 0.5f + 0.5f;
	
	Out.Fog = Fog(Out.Pos.z);
	
	return Out;
}

//===========================================================================
//	ピクセルシェーダー
//===========================================================================
// シャドウマップ
float4 PS_ShadowMap( VS_SHADOW In ) : COLOR
{
	return In.ShadowUV / In.ShadowUV.w;
}

//	影
float4 PS_Shadow( VS_SHADOW In ) : COLOR
{
	float4 Color;
	Color = In.Color * tex2D(DiffuseSamp, In.Tex);
	
	float fShadow = tex2D(ShadowSamp, In.ShadowUV);
	Color.rgb *= ((fShadow * In.Depth.w < In.Depth.z - 0.0011f)? fShadowRate: 1.0f);
	
	Color = Color * In.Fog + FogColor * (1.0f - In.Fog);
	
	return Color;
}

//===========================================================================
//	テクニック
//===========================================================================
// 影
technique ShadowMapping
{
	pass P0
	{
		AlphaBlendEnable = false;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;
		CullMode		 = none;
		
		VertexShader	= compile vs_2_0 VS_ShadowMap();
		PixelShader		= compile ps_2_0 PS_ShadowMap();
	}
	pass P1
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;
		
		VertexShader	= compile vs_2_0 VS_Shadow();
		PixelShader		= compile ps_2_0 PS_Shadow();
	}
}

//===========================================================================
//	頂点シェーダー
//===========================================================================
// 外枠
VS_OUTPUT VS_Outline( VS_INPUT In )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;
	
	In.Pos.xyz += In.Normal * fOutlineSize;
	Out.Pos	= mul(In.Pos, mWorld);
	Out.Pos	= mul(Out.Pos, mViewProjection);
	
	Out.Fog = Fog(Out.Pos.z);
	
	Out.Color = OutlineColor;
	Out.Color.a = fAlpha;
	
	return Out;
}

//===========================================================================
//	ピクセルシェーダー
//===========================================================================
// 外枠
float4 PS_Outline( VS_OUTPUT In ) : COLOR
{
	float4 Color;
	Color = In.Color * In.Fog + FogColor * (1.0f - In.Fog);
	return Color;
}

//===========================================================================
//	テクニック
//===========================================================================
// トゥーン
technique Toon
{
	pass P0
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;
		CullMode		 = cw;
		
		VertexShader	= compile vs_2_0 VS_Outline();
		PixelShader		= compile ps_2_0 PS_Outline();
	}
	pass P1
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;
		
		VertexShader	= compile vs_2_0 VS_Basic();
		PixelShader		= compile ps_2_0 PS_Basic();
	}
}

//===========================================================================
//	頂点フォーマット
//===========================================================================
// フル
struct VS_FULL
{
	float4 Pos		: POSITION;
	float4 Color	: COLOR0;
	float2 Tex		: TEXCOORD0;
	float4 ShadowUV	: TEXCOORD1;
	float4 Depth	: TEXCOORD2;
	float3 Eye		: TEXCOORD3;
	float3 Light	: TEXCOORD4;
	float4 World	: TEXCOORD5;
	float3 Normal	: TEXCOORD6;
	float  Fog		: TEXCOORD7;
};

//===========================================================================
//	頂点シェーダー
//===========================================================================
// フル
VS_FULL VS_Full( VS_INPUT In )
{
	VS_FULL Out = (VS_FULL)0;
	
	Out.World = mul(In.Pos, mWorld);
	Out.Pos	= mul(Out.World, mViewProjection);
	Out.Tex	= In.Tex;
	
	float3 N = mul(In.Normal, mWorld);
	N = normalize(N);
	Out.Normal = N;
	
	Out.Color = lAmbient;
	Out.Color.a = fAlpha;
	
	Out.Depth = mul(Out.World, mlViewProjection);
	Out.ShadowUV = Out.Depth;
	Out.ShadowUV.y = -Out.ShadowUV.y;
	Out.ShadowUV.xy = Out.ShadowUV.xy * 0.5f + 0.5f;
	
	float3 T = { 0.0f, 1.0f, 0.01f };
	T = cross(N, T);
	T = normalize(T);
	
	float3 B = cross(N, T);
	B = normalize(B);
	
	float3 E = vEyePos - In.Pos.xyz;
	Out.Eye.x = dot(E, T);
	Out.Eye.y = dot(E, B);
	Out.Eye.z = dot(E, N);
	
	float3 L = vLightDir;
	Out.Light.x = dot(L, T);
	Out.Light.y = dot(L, B);
	Out.Light.z = dot(L, N);
	
	Out.Fog = Fog(Out.Pos.z);
	
	return Out;
}

// フル
VS_FULL VS_Full2( VS_INPUT In )
{
	VS_FULL Out = (VS_FULL)0;
	
	Out.World = mul(In.Pos, mWorld);
	Out.Pos	= mul(Out.World, mViewProjection);
	Out.Tex	= In.Tex;
	
	float3 N = mul(In.Normal, mWorld);
	N = normalize(N);
	Out.Normal = N;
	
	Out.Color = lAmbient;
	Out.Color.a = fAlpha;
	
	float3 T = { 0.0f, 1.0f, 0.01f };
	T = cross(N, T);
	T = normalize(T);
	
	float3 B = cross(N, T);
	B = normalize(B);
	
	float3 E = vEyePos - In.Pos.xyz;
	Out.Eye.x = dot(E, T);
	Out.Eye.y = dot(E, B);
	Out.Eye.z = dot(E, N);
	
	float3 L = vLightDir;
	Out.Light.x = dot(L, T);
	Out.Light.y = dot(L, B);
	Out.Light.z = dot(L, N);
	
	Out.Fog = Fog(Out.Pos.z);
	
	return Out;
}

//===========================================================================
//	ピクセルシェーダー
//===========================================================================
// フル
float4 PS_Full( VS_FULL In ) : COLOR
{
	float3 N = 2.0f * tex2D(NormalSamp, In.Tex).xyz - 1.0f;
	float3 L = normalize(In.Light);
	float3 R = reflect(-normalize(In.Eye), N);
	float4 Color = In.Color + LightDir(L, N);
	
	Color = Color * tex2D(DiffuseSamp, In.Tex) + 0.3f * pow(max(0, dot(R, L)), 8);
	
	float fShadow = tex2D(ShadowSamp, In.ShadowUV);
	Color.rgb *= ((fShadow * In.Depth.w < In.Depth.z - 0.0011f)? fShadowRate: 1.0f);
	
	for(int i=0; i < iPLightNum; i++)
	{
		L = vPLightPos[i] - In.World.xyz;
		float fDist = length(L);
		if( fDist < fPLightDist[i] )
		{
			L = normalize(L);
			float fLight = dot(L, In.Normal) * (1.0f - fDist / fPLightDist[i]);
			Color.rgb += PLightColor[i] * max(0, fLight);
		}
	}
	
	Color = Color * In.Fog + FogColor * (1.0f - In.Fog);
	return Color;
}

// フル
float4 PS_Full2( VS_FULL In ) : COLOR
{
	float3 N = 2.0f * tex2D(NormalSamp, In.Tex).xyz - 1.0f;
	float3 L = normalize(In.Light);
	float3 R = reflect(-normalize(In.Eye), N);
	float4 Color = In.Color + LightDir(L, N);
	
	Color = Color * tex2D(DiffuseSamp, In.Tex) + 0.3f * pow(max(0, dot(R, L)), 8);
	
	for(int i=0; i < iPLightNum; i++)
	{
		L = vPLightPos[i] - In.World.xyz;
		float fDist = length(L);
		if( fDist < fPLightDist[i] )
		{
			L = normalize(L);
			float fLight = dot(L, In.Normal) * (1.0f - fDist / fPLightDist[i]);
			Color.rgb += PLightColor[i] * max(0, fLight);
		}
	}
	
	Color = Color * In.Fog + FogColor * (1.0f - In.Fog);
	return Color;
}

//===========================================================================
//	テクニック
//===========================================================================
// フル
technique Full
{
	pass P0
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;
		
		VertexShader	= compile vs_2_0 VS_Full();
		PixelShader		= compile ps_3_0 PS_Full();
	}
}

// フル
technique Full2
{
	pass P0
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;
		
		VertexShader	= compile vs_2_0 VS_Full2();
		PixelShader		= compile ps_3_0 PS_Full2();
	}
}

//============================================================================
//	END OF FILE
//============================================================================