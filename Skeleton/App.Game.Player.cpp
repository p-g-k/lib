//---------------------------------------------------------------------------
//!
//!	@file	App.Game.Player.cpp
//!	@brief	プレイヤークラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
// --------------------------------------------------------------------------
// include
// --------------------------------------------------------------------------
#include "App.h"

namespace App
{
	namespace Game
	{
		// --------------------------------------------------------------------------
		//	コンストラクタ
		// --------------------------------------------------------------------------
		Player::Player(void)
		{
		}

		// --------------------------------------------------------------------------
		//	デストラクタ
		// --------------------------------------------------------------------------
		Player::~Player(void)
		{
			Cleanup();
		}

		// --------------------------------------------------------------------------
		//	初期化
		//!	@return 結果
		// --------------------------------------------------------------------------
		bool	Player::Init(void)
		{
			//	スキンメッシュ生成
			_pSkinMesh = new Lib::MeshManager::SkinMesh;
			_pSkinMesh->Load("Data\\Mesh\\Player\\Player.kam");
			//_pSkinMesh->SetMotion(1, true);
			_pSkinMesh->SetMotionBlend(0, 1, 0, true);

			//	変数初期化
			_fScale = 0.01f;
			_Pos = Lib::Vector3(0.0f, 0.0f, 0.0f);
			_iLife = 10;
			_iMode = EPlayer::EState::WAIT;

			return true;
		}

		// --------------------------------------------------------------------------
		//	解放
		// --------------------------------------------------------------------------
		void	Player::Cleanup(void)
		{
			Lib::SafeDelete(_pSkinMesh);
		}

		// --------------------------------------------------------------------------
		//	更新
		// --------------------------------------------------------------------------
		void	Player::Update(void)
		{
			//	回転
			_fAngle += D3DXToRadian(0.5f);
			if( _fAngle > D3DXToRadian(360) )
			{
				_fAngle -= D3DXToRadian(360);
			}

			Lib::Matrix World, Rotation, Scale;
			//	変換行列生成
			Lib::Math::Translation(&World, _Pos);
			Lib::Math::RotationY(&Rotation, _fAngle);
			Lib::Math::Scaling(&Scale, _fScale);
			World = Scale * Rotation * World;

			_pSkinMesh->SetBlendPower((1.0f+sin(_fAngle/D3DXToRadian(360)*D3DX_PI*2.0f))*0.5f);
			//	スキンメッシュ更新
			_pSkinMesh->SetTransMatrix(World);
			_pSkinMesh->Update(1.0f);
		}

		// --------------------------------------------------------------------------
		//	描画
		//!	@param pShader [in] シェーダー
		// --------------------------------------------------------------------------
		void	Player::Render(Lib::Shader::Shader* pShader)
		{
			_pSkinMesh->Render(pShader, "Toon");
			_pSkinMesh->Render(pShader, "Basic");

#ifdef	_DEBUG
			pDebugFont->Draw(0, 24, Lib::CharSet::Format("Power:%f", (1.0f+sin(_fAngle/D3DXToRadian(360)*D3DX_PI*2.0f))*0.5f), 0xff00ff00);
#endif
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================