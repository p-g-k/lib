//---------------------------------------------------------------------------
//!
//!	@file	App.Scene.SceneGame.cpp
//!	@brief	ゲームシーン
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "App.h"

namespace App
{
	namespace Scene
	{
		Lib::Camera::Camera*		g_pCamera;	//!< カメラ
		Lib::Shader::Shader*		m_pShader;
		Lib::SpriteManager::Sprite*	m_pSprite[2];

		//---------------------------------------------------------------------------
		//	コンストラクタ
		//---------------------------------------------------------------------------
		SceneGame::SceneGame(void)
			: _pPlayer(null)
			, _pShader(null)
			, _pCamera(null)
		{
			m_pShader	 = NULL;
			m_pSprite[0] = NULL;
			m_pSprite[1] = NULL;
		}

		//---------------------------------------------------------------------------
		//	デストラクタ
		//---------------------------------------------------------------------------
		SceneGame::~SceneGame(void)
		{
		}

		//---------------------------------------------------------------------------
		//	初期化
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	SceneGame::Init(void)
		{
			m_pShader = new Lib::Shader::Shader;
			m_pShader->Init("Data\\Effect\\2D.fx");

			m_pSprite[0] = new Lib::SpriteManager::Sprite;
			m_pSprite[0]->Create(64, 64);
			m_pSprite[0]->SetPosition(640, 64);
			m_pSprite[0]->SetSize(640, 640);
			m_pSprite[0]->SetColor(0xff0000ff);

			m_pSprite[1] = new Lib::SpriteManager::Sprite;
			m_pSprite[1]->Create(64, 64);
			m_pSprite[1]->SetPosition(96, 96);
			m_pSprite[1]->SetSize(64, 64);
			m_pSprite[1]->SetColor(0xff7fff7f);

			//	シェーダー生成
			_pShader = new Lib::Shader::Shader;
			_pShader->Init("Data\\Effect\\3D.fx");

			//	カメラ生成
			_pCamera = new Lib::Camera::Camera;
			_pCamera->Init();
			_pCamera->SetProjection(D3DXToRadian(45), 1.0f, 1000.0f);
			_pCamera->SetPos(Lib::Vector3(0, 3, -10));
			_pCamera->SetTarget(Lib::Vector3(0, 1, 0));
			g_pCamera = _pCamera;

			//	プレイヤー生成
			_pPlayer = new App::Game::Player;
			_pPlayer->Init();

			return true;
		}

		//---------------------------------------------------------------------------
		//	解放
		//---------------------------------------------------------------------------
		void	SceneGame::Cleanup(void)
		{
			Lib::SafeDelete(_pShader);
			Lib::SafeDelete(_pCamera);
			Lib::SafeDelete(_pPlayer);

			Lib::SafeDelete(m_pSprite[1]);
			Lib::SafeDelete(m_pSprite[0]);
			Lib::SafeDelete(m_pShader);
		}

		//---------------------------------------------------------------------------
		//	初期化(ロード用)
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	SceneGame::InitLoad(void)
		{
			return true;
		}

		//---------------------------------------------------------------------------
		//	解放(ロード用)
		//---------------------------------------------------------------------------
		void	SceneGame::CleanupLoad(void)
		{
		}

		//---------------------------------------------------------------------------
		//	更新
		//!	@param dElapsedTime [in] 経過時間
		//---------------------------------------------------------------------------
		void	SceneGame::Update(double dElapsedTime)
		{
			//	カメラ更新
			_pCamera->Update();

			//	シェーダーに情報セット
			_pShader->SetVector3("vEyePos", _pCamera->GetPos());
			_pShader->SetMatrix("mViewProjection", _pCamera->GetView() * _pCamera->GetProjection());

			//	プレイヤー更新
			_pPlayer->Update();

			Lib::Vector3 pos = _pPlayer->GetPosition();

			Lib::Vector4 out;
			Lib::Matrix mat = _pPlayer->_pSkinMesh->GetTransMatrix() * _pCamera->GetView() * _pCamera->GetProjection();
			D3DXVec3Transform(&out, &pos, &mat);

			//float fWidth	= 32.0f / 1280.0f;
			//float fHeight	= 32.0f / 720.0f;
			//m_pSprite[0]->SetPosition(out.x, out.y);
			//m_pSprite[0]->SetSize(fWidth, fHeight);

			//	シーン移行
			if( pInput->IsKeyPush(EKeyCode::START) )
				pSceneManager->JumpScene(ESceneMode::TITLE);
		}

		//---------------------------------------------------------------------------
		//	描画
		//!	@param dElapsedTime [in] 経過時間
		//---------------------------------------------------------------------------
		void	SceneGame::Render(double dElapsedTime)
		{
			//	プレイヤー描画
			_pPlayer->Render(_pShader);

			//m_pSprite[0]->Render(m_pShader, "Basic", 0);
			//m_pSprite[1]->Render(m_pShader, "Basic", 1);

#if	_DEBUG
			pDebugFont->Draw(1280, 0, "Scene:Game", 0xff00ff00, Lib::EFormat::RIGHT);
#endif	// ~#if _DEBUG
		}

		//---------------------------------------------------------------------------
		//	更新(ロード用)
		//!	@param dElapsedTime [in] 経過時間
		//---------------------------------------------------------------------------
		void	SceneGame::UpdateLoad(double dElapsedTime)
		{
		}

		//---------------------------------------------------------------------------
		//	描画(ロード用)
		//!	@param dElapsedTime [in] 経過時間
		//---------------------------------------------------------------------------
		void	SceneGame::RenderLoad(double dElapsedTime)
		{
#if	_DEBUG
			pDebugFont->Draw(1280, 0, "Scene:Game(Load)", 0xff00ff00, Lib::EFormat::RIGHT);
#endif	// ~#if _DEBUG
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================