//---------------------------------------------------------------------------
//!
//!	@file	main.cpp
//!	@brief	メイン
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "App.h"
#ifdef _DEBUG
#include <crtdbg.h>
#endif

//---------------------------------------------------------------------------
// library
//---------------------------------------------------------------------------
//#ifdef _DEBUG
//#pragma comment(lib, "../Lib/Debug/Libd.lib")
//#else	// ~#if _DEBUG
//#pragma comment(lib, "../Lib/Release/Lib.lib")
//#endif	// ~#if _DEBUG

//---------------------------------------------------------------------------
//!	エントリーポイント
//!	@return 実行結果
//---------------------------------------------------------------------------
int	APIENTRY WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
#ifdef _DEBUG
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	//	システム
	App::System::AppSystem System;
	//	ウィンドウプロシージャーの設定
	System.SetWindowProc(App::System::WindowProc);

	//	初期化
	if( System.InitSystem("ゲーム", false, true) )
		//	メインループ
		System.MainLoop();
	//	解放
	System.CleanupSystem();

	return System.GetExitCode();
}

//============================================================================
//	END OF FILE
//============================================================================