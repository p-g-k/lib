//---------------------------------------------------------------------------
//!
//!	@file	Lib.TextureManager.cpp
//!	@brief	テクスチャー管理
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace TextureManager
	{
		//!	テクスチャー管理用構造体
		struct	TextureData
		{
			char	cFileName[256];			//!< ファイル名
			dword	dwCount;				//!< 使用している数

			LPDIRECT3DTEXTURE9	pTexture;	//!< テクスチャー情報
		};

		//!	テクスチャー管理データ
		std::vector<TextureData> TextureManage;

		//---------------------------------------------------------------------------
		//	テクスチャーの作成
		//!	@param ppTexture [out] テクスチャーポインタ
		//!	@param pFileName [in] ファイル名
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool Create(LPDIRECT3DTEXTURE9* ppTexture, const char* pFileName)
		{
			dword i;
			//	既に読み込まれていないか確認
			for(i = 0; i < TextureManage.size(); i++)
			{
				if( strcmp(TextureManage[i].cFileName, pFileName) == 0 )
					break;
			}

			if( i == TextureManage.size() )
			{
				HRESULT hr;
				//	テクスチャー情報の設定
				TextureData Texture;
				strcpy(Texture.cFileName, pFileName);
				Texture.dwCount = 0;
//#if	0
//				//	テクスチャーの読み込み
//				hr = D3DXCreateTextureFromFileEx(Lib::System::System::GetInstance()->GetDevice(),
//												 pFileName,
//												 D3DX_DEFAULT,
//												 D3DX_DEFAULT,
//												 1,
//												 0,
//												 D3DFMT_A8R8G8B8,
//												 D3DPOOL_MANAGED,
//												 D3DX_FILTER_POINT,
//												 D3DX_FILTER_POINT,
//												 0x00000000,
//												 null,
//												 null,
//												 &Texture.pTexture);
//#else	// 1
				FileIO::File xFile;
				void* pData = xFile.Read(pFileName);
				int iSize	= xFile.GetSize();
				hr = D3DXCreateTextureFromFileInMemoryEx(	Lib::System::System::GetInstance()->GetDevice(),
															pData,
															iSize,
															D3DX_DEFAULT,
															D3DX_DEFAULT,
															1,
															0,
															D3DFMT_A8R8G8B8,
															D3DPOOL_MANAGED,
															D3DX_FILTER_POINT,
															D3DX_FILTER_POINT,
															0x00000000,
															null,
															null,
															&Texture.pTexture);
//#endif	//	1

				if( FAILED(hr) )
				{
					char cTemp[256];
					sprintf(cTemp, "%sの読み込みに失敗しました(Texture)", pFileName);
					Message(cTemp, "エラー");
					return false;
				}
				TextureManage.push_back(Texture);
			}

			//	テクスチャーアドレスのコピー
			(*ppTexture) = TextureManage[i].pTexture;
			//	管理数の追加
			TextureManage[i].dwCount++;
			return true;
		}

		//---------------------------------------------------------------------------
		//	テクスチャーの作成
		//!	@param ppTexture [out] テクスチャーポインタ
		//!	@param fWidth [in] 横幅
		//!	@param fHeight [in] 縦幅
		//!	@param iFormat [in] フォーマット
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool Create(LPDIRECT3DTEXTURE9* ppTexture, dword dwWidth, dword dwHeight, int iFormat)
		{
			dword		usage	= 0;
			D3DFORMAT	fmt		= D3DFMT_A8R8G8B8;
			D3DPOOL		pool	= D3DPOOL_MANAGED;

			//	テクスチャーのフォーマットを設定する
			switch( iFormat )
			{
			case ETextureFormat::RENDERTARGET:
				usage	= D3DUSAGE_RENDERTARGET;
				fmt		= Lib::System::System::GetInstance()->GetDirectX()->GetPresentParameters()->BackBufferFormat;
				pool	= D3DPOOL_DEFAULT;
				break;
			case ETextureFormat::SHADOWMAP:
				usage	= D3DUSAGE_RENDERTARGET;
				fmt		= D3DFMT_R32F;
				pool	= D3DPOOL_DEFAULT;
				break;
			}

			//	テクスチャーを生成
			D3DXCreateTexture(	Lib::System::System::GetInstance()->GetDevice(),
								dwWidth,
								dwHeight,
								0,
								usage,
								fmt,
								pool,
								ppTexture );
			if( !*ppTexture )
				return false;

			return true;
		}

		//---------------------------------------------------------------------------
		//	テクスチャーの削除
		//!	@param ppTexture [out] テクスチャーポインタ
		//---------------------------------------------------------------------------
		void Delete(LPDIRECT3DTEXTURE9* ppTexture)
		{
			if( *ppTexture == null ) return;

			std::vector<TextureData>::iterator it;
			//	テクスチャーの管理されている場所を探す
			for(it = TextureManage.begin(); it != TextureManage.end(); it++)
			{
				if( it->pTexture == (*ppTexture) )
					break;
			}

			//	テクスチャーが管理されているか確認
			if( it != TextureManage.end() )
			{
				//	管理数を減らし、使用数が0なら解放する
				if( --it->dwCount == 0 )
				{
					SafeRelease(it->pTexture);
					TextureManage.erase(it);
				}
			}
			else
				SafeRelease(*ppTexture);

			*ppTexture = null;
		}

		//---------------------------------------------------------------------------
		//!	解放
		//---------------------------------------------------------------------------
		void	Cleanup(void)
		{
			dword i;
			//	未解放のメッシュがあれば解放しておく
			for(i = 0; i < TextureManage.size(); i++)
			{
				char cTemp[256];
				sprintf(cTemp, "%sが%d個解放されていません", TextureManage[i].cFileName, TextureManage[i].dwCount);
				Message(cTemp, "警告");
				Delete(&TextureManage[i].pTexture);
			}
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================