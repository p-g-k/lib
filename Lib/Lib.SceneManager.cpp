//---------------------------------------------------------------------------
//!
//!	@file	Lib.SceneManager.cpp
//!	@brief	シーン管理
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace SceneManager
	{
		//!	シーン情報
		struct SceneData
		{
			SceneBase*	pScene;	//!< シーンデータ
			bool		bLoad;	//!< ロードフラグ
		};

		//!	シーン管理用
		std::map<int, SceneData> SceneManage;
		//!	ロード用スレッド
		Lib::Thread::Thread LoadThread;

		//---------------------------------------------------------------------------
		//	ロード
		//!	@param pArg [out] ロードするシーン
		//!	@return 結果
		//---------------------------------------------------------------------------
		unsigned int __stdcall LoadFunction(void* pArg)
		{
			SceneBase* pScene = (SceneBase*)pArg;
			pScene->Init();
			return 0;
		}

		//---------------------------------------------------------------------------
		//	コンストラクタ
		//---------------------------------------------------------------------------
		SceneManager::SceneManager(void)
			: _pCurrentScene(null), _pBeforeScene(null)
			, _bLoad(false)
		{
		}

		//---------------------------------------------------------------------------
		//	デストラクタ
		//---------------------------------------------------------------------------
		SceneManager::~SceneManager(void)
		{
			if( _pCurrentScene )
			{
				if( _pBeforeScene )
					_pBeforeScene->Cleanup();

				//	ロード中なら終わるまで待つ
				if(	!LoadThread.IsEnd() )
				{
					LoadThread.Shutdown(true);
					_pCurrentScene->CleanupLoad();
				}
				_pCurrentScene->Cleanup();
			}
		}

		//---------------------------------------------------------------------------
		//	更新
		//!	@param dElapsedTime [in] 経過時間
		//---------------------------------------------------------------------------
		void	SceneManager::Update(double dElapsedTime)
		{
			if( _bLoad )
			{
				//	ロード終了確認
				if( LoadThread.IsEnd() )
				{
					LoadThread.Shutdown();
					_pCurrentScene->CleanupLoad();
					_bLoad = false;
				}
				else
					_pCurrentScene->UpdateLoad(dElapsedTime);
			}
			else
				_pCurrentScene->Update(dElapsedTime);
		}

		//---------------------------------------------------------------------------
		//	描画
		//!	@param dElapsedTime [in] 経過時間
		//---------------------------------------------------------------------------
		void	SceneManager::Render(double dElapsedTime)
		{
			if( _bLoad )
				_pCurrentScene->RenderLoad(dElapsedTime);
			else
				_pCurrentScene->Render(dElapsedTime);
		}

		//---------------------------------------------------------------------------
		//	シーン変更
		//!	@param iSceneNo [in] シーン番号
		//---------------------------------------------------------------------------
		void	SceneManager::JumpScene(int iSceneNo)
		{
			if( _pBeforeScene )
			{
				_pBeforeScene->Cleanup();
				_pBeforeScene = null;
			}

			if( _pCurrentScene )
				_pCurrentScene->Cleanup();

			//	新しいシーンのセット
			_pCurrentScene = SceneManage[iSceneNo].pScene;

			if( SceneManage[iSceneNo].bLoad )
			{
				//	ロード用スレッドの起動
				LoadThread.Luncher(LoadFunction, _pCurrentScene);
				ASSERT( _pCurrentScene->InitLoad(), "Loadシーンの初期化に失敗しました");
				_bLoad = true;
			}
			else
				ASSERT( _pCurrentScene->Init(), "シーンの初期化に失敗しました");
		}

		//---------------------------------------------------------------------------
		//	現在のシーンを保存したままシーン変更する
		//!	@param iSceneNo [in] シーン番号
		//---------------------------------------------------------------------------
		void	SceneManager::CallScene(int iSceneNo)
		{
			if( _pBeforeScene )
				_pBeforeScene->Cleanup();

			_pBeforeScene = _pCurrentScene;

			//	新しいシーンのセット
			_pCurrentScene = SceneManage[iSceneNo].pScene;

			if( SceneManage[iSceneNo].bLoad )
			{
				//	ロード用スレッドの起動
				LoadThread.Luncher(LoadFunction, _pCurrentScene);
				ASSERT(_pCurrentScene->InitLoad(), "ロードの初期化に失敗しました");
				_bLoad = true;
			}
			else
				ASSERT(_pCurrentScene->Init(), "シーンの初期化に失敗しました");
		}

		//---------------------------------------------------------------------------
		//	保存したシーンに戻る
		//---------------------------------------------------------------------------
		void	SceneManager::ReturnScene(void)
		{
			if( _pCurrentScene )
				_pCurrentScene->Cleanup();
			//	保存されているシーンをセット
			_pCurrentScene = _pBeforeScene;

			_pBeforeScene = null;
		}

		//---------------------------------------------------------------------------
		//	シーンのセット
		//!	@param iSceneNo [in] シーン番号
		//!	@param pScene [in] シーン
		//!	@param bLoad [in] ロードフラグ
		//---------------------------------------------------------------------------
		void	SetScene(int iSceneNo, SceneBase* pScene, bool bLoad)
		{
			//	既にシーンが存在していれば消しておく
			if( SceneManage.find(iSceneNo) != SceneManage.end() )
				SafeDelete(SceneManage[iSceneNo].pScene);

			//	シーンのセット
			SceneManage[iSceneNo].pScene = pScene;
			SceneManage[iSceneNo].bLoad = bLoad;
		}

		//---------------------------------------------------------------------------
		//	解放
		//---------------------------------------------------------------------------
		void	Cleanup(void)
		{
			std::map<int, SceneData>::iterator it;
			it = SceneManage.begin();
			while( SceneManage.size() )
			{
				SafeDelete(it->second.pScene);
				it = SceneManage.erase(it);
			}
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================