//---------------------------------------------------------------------------
//!
//!	@file	Lib.FileIO.h
//!	@brief	入出力関連
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#pragma once

//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace FileIO
	{
		//===========================================================================
		//!	ファイルクラス
		//===========================================================================
		class File
		{
		public:
			//!	コンストラクタ
			File(void);
			//!	デストラクタ
			~File(void);

			//!	読み込み
			void*	Read(const char* pPath);
			//!	参照して読み込み
			void*	Read(void);
			//!	書き込み
			void	Write(const char* pPath, const void* pBuffer, int iSize);
			//!	参照して書き込み
			void	Write(const void* pBuffer, int iSize);

			//!	読み込み開始
			bool	ReadingFile(const char* pPath);
			//!	読み込み
			void	Reading(void* pBuffer, int iSize);
			//!	書き込み開始
			bool	WrittingFile(const char* pPath);
			//!	書き込み
			void	Writting(const void* pBuffer, int iSize);

			//!	参照時に使用するオリジナルフォーマットの設定
			bool	SetOrigin(const char* pName, const char* pExtension);
			//!	オリジナルフォーマットのリセット
			void	ResetOrigin(void);

			//!	現在位置のセット
			int		SetPointer(int iMove, int iStart = EFilePointer::BEGIN);
			//!	参照時の初期ディレクトリー設定
			void	SetDirectory(char* pDirectory);
			//!	参照可能なファイルの設定
			void	SetReferenceFormat(int iFormat);

			//!	読み込み中のデータを取得
			void*	GetFile(void);
			//!	現在位置の取得
			int		GetPointer(void);
			//!	読み込み中のデータサイズ取得
			int		GetSize(void);
			//!	参照時の初期ディレクトリー取得
			char*	GetDirectory(void);
			//!	参照したファイル名取得
			char*	GetFilePath(void);
			//!	参照可能なファイルの取得
			int		GetReferenceFormat(void);

			//!	参照(読み込み)
			char*	ReadReference(void);
			//!	参照(書き込み)
			char*	WriteReference(void);

			//!	閉じる
			void	CloseFile(void);

		private:
			//!	ファイルの閉じ忘れを確認
			void	CloseCheck(void);
			//!	参照可能なファイルの取得
			void	GetReference(char* pFormat);

		private:
			HANDLE	_hFile;				//!< ファイル読み込み用のハンドル

			byte*	_pBuffer;			//!< 読み込んだデータ
			dword	_dwSize;			//!< 読み込んだデータのサイズ
			bool	_bOpen;				//!< オープンフラグ

			char	_cDirectory[256];	//!< 参照時の初期ディレクトリー
			char	_cPath[256];		//!< 参照したファイル名
			int		_iFormat;			//!< 参照可能なデータ情報
			dword	_dwOriginNum;		//!< オリジナルファイルの数

			char	_cName[32][64];		//!< オリジナルファイル名(説明)
			char	_cExtension[32][8];	//!< オリジナルの拡張子
		};
	}
}

//============================================================================
//	END OF FILE
//============================================================================