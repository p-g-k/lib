//---------------------------------------------------------------------------
//!
//!	@file	Lib.System.DirectX.h
//!	@brief	DirectX関連
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#pragma once

//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.System.h"

namespace Lib
{
	namespace System
	{
		namespace DirectX
		{
			//===========================================================================
			//!	DirectXクラス
			//===========================================================================
			class DirectX
			{
			public:
				//!	コンストラクタ
				DirectX(void);
				//!	デストラクタ
				~DirectX(void);

				//!	初期化
				bool	Init(Window::Window* pWindow);
				//!	解放
				void	Cleanup(void);

				//!	画面のクリア
				void	Clear(dword dwColor = 0, bool bClearZ = true);
				//!	描画開始
				void	BeginScene(dword dwColor = 0);
				//!	描画終了
				void	EndScene(void);

				//!	Direct3Dデバイスの取得
				LPDIRECT3DDEVICE9	GetDevice(void);
				//!	バックバッファフォーマットの取得
				D3DPRESENT_PARAMETERS*	DirectX::GetPresentParameters(void);

			private:
				LPDIRECT3D9				_pD3D;		//!< DirectXオブジェクト
				LPDIRECT3DDEVICE9		_pDevice;	//!< Direct3Dデバイス
				D3DPRESENT_PARAMETERS	_d3dpp;		//!< Direct3Dの設定用
			};
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================