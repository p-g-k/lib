//---------------------------------------------------------------------------
//!
//!	@file	Lib.System.DirectX.cpp
//!	@brief	DirectX関連
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace System
	{
		namespace DirectX
		{
			//---------------------------------------------------------------------------
			//	コンストラクタ
			//---------------------------------------------------------------------------
			DirectX::DirectX(void)
				: _pD3D(null), _pDevice(null)
			{
			}

			//---------------------------------------------------------------------------
			//	デストラクタ
			//---------------------------------------------------------------------------
			DirectX::~DirectX(void)
			{
				Cleanup();
			}

			//---------------------------------------------------------------------------
			//	初期化
			//!	@param pWindow [in] ウィンドウクラス
			//---------------------------------------------------------------------------
			bool	DirectX::Init(Window::Window* pWindow)
			{
				//	Direct3Dオブジェクトの生成
				if( (_pD3D = Direct3DCreate9(D3D_SDK_VERSION)) == null )
				{
					Message("Direct3Dオブジェクトの生成に失敗しました", "エラー");
					return false;
				}

				//	画面のモード取得
				D3DDISPLAYMODE	d3ddm;
				if( FAILED(_pD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3ddm)) )
				{
					Message("画面のモード取得を失敗しました", "エラー");
					return false;
				}

				ZeroMemory(&_d3dpp, sizeof(D3DPRESENT_PARAMETERS));

				bool bFullScreen = pWindow->GetFullScreen();

				//	Direct3DDeviceの設定
				if( bFullScreen )
					_d3dpp.FullScreen_RefreshRateInHz	= D3DPRESENT_RATE_DEFAULT;

				_d3dpp.Windowed					= !bFullScreen;
				_d3dpp.BackBufferWidth			= pWindow->GetWidth();
				_d3dpp.BackBufferHeight			= pWindow->GetHeight();

				_d3dpp.Flags					= 0;
				_d3dpp.BackBufferCount			= 1;
				_d3dpp.BackBufferFormat			= d3ddm.Format;
				_d3dpp.MultiSampleType			= D3DMULTISAMPLE_NONE;
				_d3dpp.SwapEffect				= D3DSWAPEFFECT_DISCARD;
				_d3dpp.hDeviceWindow			= pWindow->GetWindowHandle();

				_d3dpp.EnableAutoDepthStencil	= true;
				_d3dpp.AutoDepthStencilFormat	= D3DFMT_D24S8;
				_d3dpp.PresentationInterval		= D3DPRESENT_INTERVAL_IMMEDIATE;

				D3DCAPS9 Caps;
				ZeroMemory(&Caps, sizeof(Caps));
				//	シェーダーのバージョンを確認
				_pD3D->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &Caps);
				if( Caps.VertexShaderVersion < D3DVS_VERSION( 2, 0 ) )
				{
					Message("デバイスの性能が足りません(VertexShader)", "エラー");
					return false;
				}
				if( Caps.PixelShaderVersion < D3DVS_VERSION( 2, 0 ) )
				{
					Message("デバイスの性能が足りません(PixelShader)", "エラー");
					return false;
				}

				//	Direct3Dデバイスの生成
				if( FAILED(_pD3D->CreateDevice(	D3DADAPTER_DEFAULT,
												D3DDEVTYPE_HAL,
												_d3dpp.hDeviceWindow,
												D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED,
												&_d3dpp, &_pDevice )) )
				{
					Message("Direct3Dデバイスの生成に失敗しました", "エラー");
					return false;
				}

				D3DVIEWPORT9 vp;
				vp.X		= 0;
				vp.Y		= 0;
				vp.Width	= _d3dpp.BackBufferWidth;
				vp.Height	= _d3dpp.BackBufferHeight;
				vp.MinZ		= 0.0f;
				vp.MaxZ		= 1.0f;

				if( FAILED(_pDevice->SetViewport(&vp)) )
					return false;

				//	描画設定
				_pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
				_pDevice->SetRenderState(D3DRS_ALPHATESTENABLE, true);
				_pDevice->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_NOTEQUAL);
				_pDevice->SetRenderState(D3DRS_ALPHAREF, false);

				_pDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);

				_pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
				_pDevice->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);
				_pDevice->SetRenderState(D3DRS_NORMALIZENORMALS, true);
				_pDevice->SetRenderState(D3DRS_ZWRITEENABLE, true);
				_pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
				return true;
			}

			//---------------------------------------------------------------------------
			//	解放
			//---------------------------------------------------------------------------
			void	DirectX::Cleanup(void)
			{
				SafeRelease(_pD3D);
				SafeRelease(_pDevice);
			}

			//---------------------------------------------------------------------------
			//	画面のクリア
			//!	@param dwColor [in] クリア色
			//!	@param bClearZ [in] Zバッファのクリアフラグ
			//---------------------------------------------------------------------------
			void	DirectX::Clear(dword dwColor, bool bClearZ)
			{
				dword dwFlag = D3DCLEAR_TARGET;
				dwFlag |= ( bClearZ )? D3DCLEAR_ZBUFFER: 0;

				_pDevice->Clear(0, null, dwFlag, dwColor, 1.0f, 0);
			}

			//---------------------------------------------------------------------------
			//	描画開始
			//!	@param dwColor [in] クリア色
			//---------------------------------------------------------------------------
			void	DirectX::BeginScene(dword dwColor)
			{
				_pDevice->BeginScene();

				Clear(dwColor);
			}

			//---------------------------------------------------------------------------
			//	描画終了
			//---------------------------------------------------------------------------
			void	DirectX::EndScene(void)
			{
				_pDevice->EndScene();

				//	画面に反映させる
				if( FAILED(_pDevice->Present(null, null, null, null)) )
					_pDevice->Reset(&_d3dpp);
			}

			//---------------------------------------------------------------------------
			//	Direct3Dデバイスの取得
			//!	@return Direct3Dデバイス
			//---------------------------------------------------------------------------
			LPDIRECT3DDEVICE9	DirectX::GetDevice(void)
			{
				return _pDevice;
			}

			//---------------------------------------------------------------------------
			//	バックバッファフォーマットの取得
			//!	@return バックバッファフォーマット
			//---------------------------------------------------------------------------
			D3DPRESENT_PARAMETERS*	DirectX::GetPresentParameters(void)
			{
				return &_d3dpp;
			}
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================