//---------------------------------------------------------------------------
//!
//!	@file	Lib.MeshManager.cpp
//!	@brief	メッシュ管理
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace MeshManager
	{
		//!	メッシュ管理用構造体
		struct MeshData
		{
			char		cFileName[256];	//!< ファイル名
			dword		dwCount;		//!< 使用している数

			LPD3DXMESH	pMesh;			//!< メッシュ
			dword		dwMaterialNum;	//!< 材質数
			Material*	pMaterial;		//!< 材質
		};

		//!	メッシュ管理データ
		std::vector<MeshData> MeshManage;

		//!	メッシュ管理用構造体
		struct SkinMeshData
			: MeshData
		{
			Vertex*			pVertexs;		//!< 頂点情報
			LPD3DXSKININFO	pSkinInfo;		//!< スキン情報
			Matrix*			pMatrix;		//!< マトリックス
			word			wBoneNum;		//!< ボーン数
			Bone*			pBone;			//!< ボーン情報
			word			wMotionNum;		//!< モーション数
			Motion*			pMotion;		//!< モーション情報
			word*			pDrawMotion;	//!< 描画モーション
			float*			pDrawFrame;		//!< 描画フレーム
		};

		//!	メッシュ管理データ
		std::vector<SkinMeshData> SkinMeshManage;

		namespace Metasequoia
		{
			//! 材質
			struct Material
			{
				char	cName[64];		//!< 材質名
				Color4	Color;			//!< 色
				float	fDiffuse;		//!< 拡散光
				float	fAmbient;		//!< 環境光
				float	fEmissive;		//!< 放射光
				float	fSpecular;		//!< 鏡面反射光
				float	fPower;			//!< 鏡面反射の鮮明度
				char	cTexture[64];	//!< テクスチャー名
			};

			//!	メタセコイアオブジェクト
			struct Object
			{
				char					cName[64];	//!< オブジェクト名
				std::vector<Vector3>	Position;	//!< 位置
				std::vector<word>		wFace;		//!< 面毎の頂点数
				std::vector<word>		wIndex;		//!< 頂点番号
				std::vector<dword>		dwMaterial;	//!< 材質番号
				std::vector<Vector2>	UV;			//!< UV座標
				std::vector<Vector3>	Vertex;		//!< 頂点位置
			};

			//!	メタセコイアデータ
			struct Metasequoia
			{
				char					cFileName[256];	//!< ファイル名
				dword					dwMaterialNum;	//!< 材質数
				std::vector<Material>	Material;		//!< 材質
				std::vector<Object>		Object;			//!< オブジェクト
			};

			//!	位置データ
			struct Locate
			{
				char	cName[32];		//!< ボーン名
				char	cParent[32];	//!< 親名
				Vector3	SPos;			//!< 姿勢
				Vector3	Pos;			//!< 姿勢
			};

			//!	ボーンデータ
			struct Bone
			{
				char				cName[32];		//!< ボーン名
				char				cParent[32];	//!< 親名
				Quaternion			SPose;			//!< 姿勢
				Quaternion			Pose;			//!< 姿勢
				char				cStartName[32];	//!< 開始位置名
				char				cEndName[32];	//!< 終了位置名
				Vector3				Pos;			//!< 位置
				Vector3				Center;			//!< 中心位置
				std::vector<dword>	dwIndex;		//!< 影響頂点番号
				std::vector<float>	fInfluence;		//!< 影響力
			};

			//!	アンカー
			struct Anchor
			{
				char					cName[32];	//!< アンカー名
				Vector3					Center;		//!< 中心位置
				std::vector<Vector3>	Position;	//!< 位置
				std::vector<dword>		dwIndex;	//!< 影響頂点番号
				std::vector<float>		fInfluence;	//!< 影響力
			};

			//!	MKIデータ
			struct MKI
			{
				std::vector<Locate>	Locate;	//!< 位置情報
				std::vector<Bone>	Bone;	//!< ボーン情報
			};

			//!	位置データ
			struct Vector
			{
				char					cName[32];	//!< 位置名
				std::vector<word>		wFrame;		//!< キーフレーム
				std::vector<Vector3>	Pos;		//!< 位置
			};

			//!	姿勢データ
			struct Quaternion
			{
				char							cName[32];	//!< 姿勢名
				std::vector<word>				wFrame;		//!< キーフレーム
				std::vector<Lib::Quaternion>	Pose;		//!< 姿勢
			};

			//!	モーションデータ
			struct Motion
			{
				char					cName[32];	//!< モーション名
				word					wFrameNum;	//!< フレーム数
				std::vector<Vector>		Vector;		//!< 位置情報
				std::vector<Quaternion>	Quaternion;	//!< 姿勢情報
			};

			//!	MIKOTOデータ
			struct Mikoto
			{
				MKI					MKI;	//!< ボーン情報
				std::vector<Motion>	Motion;	//!< モーション情報
			};

			//---------------------------------------------------------------------------
			//	メタセコイアデータの作成
			//!	@param ppMetasequoia [out] メタセコイアデータポインタ
			//!	@param pFileName [in] ファイル名
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool CreateMetasequoia(Metasequoia** ppMetasequoia, const char* pFileName)
			{
				dword i, j;
				Lib::CharSet::Lexer Lexer;
				Lexer.LoadBuffer(pFileName);
				*ppMetasequoia = null;

				//	読み込んだデータのチェック
				char* pCheck[6] = { "Metasequoia", "Document", "Format", "Text", "Ver", "1.0" };
				for(i = 0; i < 6; i++)
				{
					if( strcmp(Lexer.Pop(), pCheck[i]) )
					{
						char cTemp[256];
						sprintf(cTemp, "読み込まれたmqoファイルが正しくありません(%s)", pFileName);
						Message(cTemp, "エラー");
						return false;
					}
				}
				//	メタセコイアデータの生成
				Metasequoia* pData = new Metasequoia;
				strcpy(pData->cFileName, pFileName);

				Lexer.Seek("Material"); 
				pData->dwMaterialNum = atoi(Lexer.Pop());
				Lexer.Pop();

				//	材質取得
				for(i = 0; i < pData->dwMaterialNum; i++)
				{
					Material MqoMaterial;
					ZeroMemory(&MqoMaterial, sizeof(Material));
					strcpy(MqoMaterial.cName, Lexer.Pop());
					Lexer.Seek("col");
					Lexer.Pop();
					MqoMaterial.Color.r = (float)atof(Lexer.Pop());
					MqoMaterial.Color.g = (float)atof(Lexer.Pop());
					MqoMaterial.Color.b = (float)atof(Lexer.Pop());
					MqoMaterial.Color.a = (float)atof(Lexer.Pop());
					Lexer.Seek("dif");
					Lexer.Pop();
					MqoMaterial.fDiffuse = (float)atof(Lexer.Pop());
					Lexer.Seek("amb");
					Lexer.Pop();
					MqoMaterial.fAmbient = (float)atof(Lexer.Pop());
					Lexer.Seek("emi");
					Lexer.Pop();
					MqoMaterial.fEmissive = (float)atof(Lexer.Pop());
					Lexer.Seek("spc");
					Lexer.Pop();
					MqoMaterial.fSpecular = (float)atof(Lexer.Pop());
					Lexer.Seek("power");
					Lexer.Pop();
					MqoMaterial.fPower = (float)atof(Lexer.Pop());
					Lexer.Pop();
					//	テクスチャーの読み込み
					if( strcmp(Lexer.Peek(), "tex") == 0 )
					{
						Lexer.Seek("(");
						strcpy(MqoMaterial.cTexture, Lexer.Pop());
						Lexer.Pop();
					}

					pData->Material.push_back(MqoMaterial);
				}

				while( Lexer.Seek("Object") )
				{
					Object Object;
					strcpy(Object.cName, Lexer.Pop());

					//	頂点の取得
					Lexer.Seek("vertex");
					Object.Position.resize(atoi(Lexer.Pop()));
					Lexer.Pop();
					for(i = 0; i < Object.Position.size(); i++)
					{
						Object.Position[i].x =-(float)atof(Lexer.Pop());
						Object.Position[i].y = (float)atof(Lexer.Pop());
						Object.Position[i].z = (float)atof(Lexer.Pop());
					}

					//	面情報の取得
					Lexer.Seek("face");
					dword dwFaceNum = atoi(Lexer.Pop());
					Lexer.Pop();
					for(i = 0; i < dwFaceNum; i++)
					{
						Object.wFace.push_back(atoi(Lexer.Pop()));
						Lexer.Seek("V");
						Lexer.Pop();
						//	頂点番号の取得
						for(j = 0; j < Object.wFace[i]; j++)
							Object.wIndex.push_back(atoi(Lexer.Pop()));
						Lexer.Pop();

						dword dwMaterial = -1;
						//	材質番号の取得
						if( strcmp(Lexer.Peek(), "M") == 0 )
						{
							Lexer.Seek("(");
							dwMaterial = atoi(Lexer.Pop());
							Lexer.Pop();
						}
						Object.dwMaterial.push_back(dwMaterial);

						Vector2 UV[4];
						ZeroMemory(&UV, sizeof(Vector2) * 4);
						//	UV座標の取得
						if( strcmp(Lexer.Peek(), "UV") == 0 )
						{
							Lexer.Seek("(");
							for(j = 0; j < Object.wFace[i]; j++)
							{
								UV[j].x = (float)atof(Lexer.Pop());
								UV[j].y = (float)atof(Lexer.Pop());
							}
							Lexer.Pop();
						}
						for(j = 0; j < Object.wFace[i]; j++)
							Object.UV.push_back(UV[j]);
					}

					pData->Object.push_back(Object);
				}

				*ppMetasequoia = pData;
				return true;
			}

			//---------------------------------------------------------------------------
			//	メッシュの作成
			//!	@param ppMesh [out] メッシュデータポインタ
			//!	@param pMetasequoia [in] メタセコイアデータ
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool CreateMesh(MeshData* pMeshData, Metasequoia* pMetasequoia)
			{
				dword i, j;
				char cPath[256];

				//	ファイルのディレクトリ取得
				strcpy(cPath, pMetasequoia->cFileName);
				for(i = (dword)strlen(cPath); i > 0; i--)
				{
					if( IsDBCSLeadByte(cPath[i - 2]) )
					{
						i--;
						continue;
					}
					if( cPath[i - 1] == '\\' || cPath[i - 1] == '/' )
						break;
				}
				cPath[i] = '\0';

				pMeshData->dwMaterialNum = pMetasequoia->dwMaterialNum;
				pMeshData->pMaterial = new Lib::Material[pMeshData->dwMaterialNum];
				ZeroMemory(pMeshData->pMaterial, sizeof(Lib::Material) * pMeshData->dwMaterialNum);
				//	材質の設定
				for(i = 0; i < pMeshData->dwMaterialNum; i++)
				{
					Lib::Material* pMaterial = &pMeshData->pMaterial[i];
					Material* pMqoMaterial = &pMetasequoia->Material[i];
					Color4 Color = pMqoMaterial->Color;
					float fTmp = pMqoMaterial->fDiffuse;
					pMaterial->Diffuse.a = Color.a;
					pMaterial->Diffuse.r = Color.r * fTmp;
					pMaterial->Diffuse.g = Color.g * fTmp;
					pMaterial->Diffuse.b = Color.b * fTmp;
					fTmp = pMqoMaterial->fAmbient;
					pMaterial->Ambient.r = Color.r * fTmp;
					pMaterial->Ambient.g = Color.g * fTmp;
					pMaterial->Ambient.b = Color.b * fTmp;

					//	テクスチャー読み込み
					if( pMqoMaterial->cTexture[0] != '\0' )
					{
						char cTemp[256];
						sprintf(cTemp, "%s%s", cPath, pMqoMaterial->cTexture);
						Lib::TextureManager::Create(&pMaterial->pTexture, cTemp);

						WIN32_FIND_DATA fd;
						sprintf(cTemp, "%sN%s", cPath, pMqoMaterial->cTexture);
						HANDLE hFind = FindFirstFile(cTemp, &fd);
						BOOL bResult = (hFind != INVALID_HANDLE_VALUE);
						if( bResult )
							Lib::TextureManager::Create(&pMaterial->pNormal, cTemp);
					}
				}

				std::vector<Vertex> Vertexs;
				std::vector<word> wFace;
				word wIndexNum = 0;
				dword dwFaceNum = 0;
				std::vector<word> wIndex;
				std::vector<dword> dwAttribute;

				word k, l;
				char cTemp[7];
				//	面情報の取得
				for(i = 0; i < pMetasequoia->Object.size(); i++)
				{
					Object* pObject = &pMetasequoia->Object[i];
					memcpy(cTemp, pObject->cName, sizeof(char) * 6);
					//	ボーンもしくはアンカーの場合は無視する
					if( cTemp[0] == 'b' )
					{
						cTemp[5] = '\0';
						if( strcmp(cTemp, "bone:") == 0 )
							continue;
					}
					if( cTemp[0] == 'a' )
					{
						cTemp[6] = '\0';
						if( strcmp(cTemp, "anchor") == 0 )
							continue;
					}

					wIndexNum = 0;
					word wTmpIndex[3];
					Lib::Vertex Vertex;
					ZeroMemory(&Vertex, sizeof(Vertex));
					//	面の設定
					for(j = 0; j < pObject->wFace.size(); j++)
					{
						//	線の場合は飛ばす
						if( pObject->wFace[j] < 3 )
							continue;

						//	材質が無い場合エラー回避のため材質を０番目に設定
						if( pObject->dwMaterial[j] == -1 )
							pObject->dwMaterial[j] = 0;

						//	頂点の設定
						for(k = 0; k < 3; k++)
						{
							Vertex.Position = pObject->Position[pObject->wIndex[wIndexNum]];
							Vertex.UV = pObject->UV[wIndexNum];
							//	同位置の頂点があれば使いまわす
							for(l = 0; l < Vertexs.size(); l++)
							{
								if( Vertexs[l].Position == Vertex.Position &&
									Vertexs[l].UV == Vertex.UV )
									break;
							}
							if( l == Vertexs.size() )
							{
								pObject->Vertex.push_back(Vertex.Position);
								Vertexs.push_back(Vertex);
							}

							wTmpIndex[k] = l;
							wIndex.push_back(l);
							wIndexNum++;
						}
						dwAttribute.push_back(pObject->dwMaterial[j]);

						//	四角の場合はもう一つ面を作る
						if( pObject->wFace[j] == 4 )
						{
							wIndex.push_back(wTmpIndex[0]);
							wIndex.push_back(wTmpIndex[2]);
							Vertex.Position = pObject->Position[pObject->wIndex[wIndexNum]];
							Vertex.UV = pObject->UV[wIndexNum];
							//	同位置の頂点があれば使いまわす
							for(l = 0; l < Vertexs.size(); l++)
							{
								if( Vertexs[l].Position == Vertex.Position &&
									Vertexs[l].UV == Vertex.UV )
									break;
							}
							if( l == Vertexs.size() )
							{
								pObject->Vertex.push_back(Vertex.Position);
								Vertexs.push_back(Vertex);
							}

							wIndex.push_back(l);
							dwAttribute.push_back(pObject->dwMaterial[j]);
							wIndexNum++;
							dwFaceNum++;
						}
						dwFaceNum++;
					}
					pObject->Position.clear();
					pObject->wFace.clear();
					pObject->wIndex.clear();
					pObject->dwMaterial.clear();
					pObject->UV.clear();
				}

				//	法線の算出
				for(i = 0; i < dwFaceNum; i++)
				{
					word a, b, c;
					a = wIndex[i * 3 + 0];
					b = wIndex[i * 3 + 1];
					c = wIndex[i * 3 + 2];

					Vector3 Normal, Vec1, Vec2;
					Vec1.x = Vertexs[b].Position.x - Vertexs[a].Position.x;
					Vec1.y = Vertexs[b].Position.y - Vertexs[a].Position.y;
					Vec1.z = Vertexs[b].Position.z - Vertexs[a].Position.z;

					Vec2.x = Vertexs[c].Position.x - Vertexs[a].Position.x;
					Vec2.y = Vertexs[c].Position.y - Vertexs[a].Position.y;
					Vec2.z = Vertexs[c].Position.z - Vertexs[a].Position.z;

					Lib::Math::Cross(&Normal, Vec1, Vec2);
					Lib::Math::Normalize(&Normal, Normal);

					Vertexs[a].Normal.x += Normal.x;
					Vertexs[a].Normal.y += Normal.y;
					Vertexs[a].Normal.z += Normal.z;
					Vertexs[b].Normal.x += Normal.x;
					Vertexs[b].Normal.y += Normal.y;
					Vertexs[b].Normal.z += Normal.z;
					Vertexs[c].Normal.x += Normal.x;
					Vertexs[c].Normal.y += Normal.y;
					Vertexs[c].Normal.z += Normal.z;
				}
				for(i = 0; i < Vertexs.size(); i++)
					Lib::Math::Normalize(&Vertexs[i].Normal, Vertexs[i].Normal);

				void* pVertex;
				void* pIndex;
				dword* pAttribute;

				LPDIRECT3DDEVICE9 pDevice = Lib::System::System::GetInstance()->GetDevice();
				//	メッシュの生成
				D3DXCreateMeshFVF(dwFaceNum, (dword)Vertexs.size(), D3DXMESH_MANAGED, D3DFVF_VERTEX, pDevice, &pMeshData->pMesh);
				LPD3DXMESH pMesh = pMeshData->pMesh;

				//	情報セット
				pMesh->LockVertexBuffer(D3DLOCK_DISCARD, &pVertex);
				memcpy(pVertex, &Vertexs[0], sizeof(Vertex) * Vertexs.size());
				pMesh->UnlockVertexBuffer();

				pMesh->LockIndexBuffer(D3DLOCK_DISCARD, &pIndex);
				memcpy(pIndex, &wIndex[0], sizeof(word) * dwFaceNum * 3);
				pMesh->UnlockIndexBuffer();

				pMesh->LockAttributeBuffer(D3DLOCK_DISCARD, &pAttribute);
				memcpy(pAttribute, &dwAttribute[0], sizeof(dword) * dwFaceNum);
				pMesh->UnlockAttributeBuffer();
				return true;
			}

			//---------------------------------------------------------------------------
			//	MKIデータの作成
			//!	@param pMKI [out] MKIデータポインタ
			//!	@param pFileName [in] ファイル名
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	CreateMKI(MKI* pMKI, const char* pFileName)
			{
				Lib::CharSet::Lexer Lexer;
				Lexer.LoadBuffer(pFileName);

				//	読み込んだデータのチェック
				char* pCheck[4] = { "Mikoto", "Intermediate","Ver", "1" };
				for(dword i = 0; i < 4; i++)
				{
					if( strcmp(Lexer.Pop(), pCheck[i]) )
					{
						char cTemp[256];
						sprintf(cTemp, "読み込まれたMKIファイルが正しくありません(%s)", pFileName);
						Message(cTemp, "エラー");
						return false;
					}
				}

				Lexer.Seek("Skelton");
				Lexer.Seek("name");
				Lexer.Pop();
				Lexer.Pop();
				//	ボーン情報の取得
				while( strcmp(Lexer.Peek(), "}") )
				{
					//	位置
					if( strcmp(Lexer.Pop(), "Locate") == 0 )
					{
						Locate Locate;
						ZeroMemory(&Locate, sizeof(Locate));
						Lexer.Seek("name");
						Lexer.Pop();
						strcpy(Locate.cName, Lexer.Pop());
						if( strcmp(Lexer.Peek(), "coordinate") == 0 )
						{
							Lexer.Seek("=");
							strcpy(Locate.cParent, Lexer.Pop());
						}
						Lexer.Seek("spos");
						Lexer.Seek("(");
						Locate.SPos.x =-(float)atof(Lexer.Pop());
						Locate.SPos.y = (float)atof(Lexer.Pop());
						Locate.SPos.z = (float)atof(Lexer.Pop());
						Lexer.Seek("pos");
						Lexer.Seek("(");
						Locate.Pos.x =-(float)atof(Lexer.Pop());
						Locate.Pos.y = (float)atof(Lexer.Pop());
						Locate.Pos.z = (float)atof(Lexer.Pop());
						pMKI->Locate.push_back(Locate);
					}
					//	姿勢(ボーン)
					else
					{
						Bone Bone;
						ZeroMemory(&Bone, sizeof(Bone));
						Lexer.Seek("name");
						Lexer.Pop();
						strcpy(Bone.cName, Lexer.Pop());
						if( strcmp(Lexer.Peek(), "coordinate") == 0 )
						{
							Lexer.Seek("=");
							strcpy(Bone.cParent, Lexer.Pop());
						}
						Lexer.Seek("srot");
						Lexer.Seek("(");
						Bone.SPose.x =-(float)atof(Lexer.Pop());
						Bone.SPose.y = (float)atof(Lexer.Pop());
						Bone.SPose.z = (float)atof(Lexer.Pop());
						Bone.SPose.w = (float)atof(Lexer.Pop());
						Lexer.Seek("rot");
						Lexer.Seek("(");
						Bone.Pose.x =-(float)atof(Lexer.Pop());
						Bone.Pose.y = (float)atof(Lexer.Pop());
						Bone.Pose.z = (float)atof(Lexer.Pop());
						Bone.Pose.w = (float)atof(Lexer.Pop());
						Lexer.Seek("start");
						Lexer.Pop();
						strcpy(Bone.cStartName, Lexer.Pop());
						Lexer.Seek("end");
						Lexer.Pop();
						strcpy(Bone.cEndName, Lexer.Pop());
						D3DXQuaternionInverse(&Bone.SPose, &Bone.SPose);
						D3DXQuaternionInverse(&Bone.Pose, &Bone.Pose);
						pMKI->Bone.push_back(Bone);
					}
					Lexer.Seek("}");
				}
				return true;
			}

			//---------------------------------------------------------------------------
			//	Motionデータの作成
			//!	@param pMotion [out] Motionデータポインタ
			//!	@param pFileName [in] ファイル名
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	CreateMotion(std::vector<Motion>* pMotion, const char* pFileName)
			{
				Lib::CharSet::Lexer Lexer;
				Lexer.LoadBuffer(pFileName);

				//	読み込んだデータのチェック
				char* pCheck[4] = { "Mikoto", "Motion","Ver", "2" };
				for(dword i = 0; i < 4; i++)
				{
					if( strcmp(Lexer.Pop(), pCheck[i]) )
					{
						char cTemp[256];
						sprintf(cTemp, "読み込まれたMKMファイルが正しくありません(%s)", pFileName);
						Message(cTemp, "エラー");
						return false;
					}
				}

				//	モーションの取得
				while( Lexer.Seek("Motion") )
				{
					Motion Motion;
					ZeroMemory(&Motion, sizeof(Motion));
					Lexer.Seek("name");
					Lexer.Pop();
					strcpy(Motion.cName, Lexer.Pop());
					Lexer.Seek("endframe");
					Lexer.Pop();
					Motion.wFrameNum = atoi(Lexer.Pop());
					Lexer.Seek("loop");
					Lexer.Pop();
					Lexer.Pop();
					//	位置
					while( strcmp(Lexer.Peek(), "Vector") == 0 )
					{
						Vector Vector;
						Lexer.Seek("name");
						Lexer.Pop();
						strcpy(Vector.cName, Lexer.Pop());
						Lexer.Seek("curve");
						Lexer.Pop();
						Lexer.Pop();
						while( strcmp(Lexer.Peek(), "}") )
						{
							Vector.wFrame.push_back(atoi(Lexer.Pop()));
							Vector3 Pos;
							Lexer.Pop();
							Pos.x =-(float)atof(Lexer.Pop());
							Pos.y = (float)atof(Lexer.Pop());
							Pos.z = (float)atof(Lexer.Pop());
							Lexer.Pop();
							Vector.Pos.push_back(Pos);
						}
						Lexer.Pop();
						Motion.Vector.push_back(Vector);
					}

					//	姿勢
					while( strcmp(Lexer.Peek(), "Quaternion") == 0 )
					{
						Quaternion Quaternion;
						Lexer.Seek("name");
						Lexer.Pop();
						strcpy(Quaternion.cName, Lexer.Pop());
						Lexer.Seek("curve");
						Lexer.Pop();
						Lexer.Pop();
						while( strcmp(Lexer.Peek(), "}") )
						{
							Quaternion.wFrame.push_back(atoi(Lexer.Pop()));
							Lib::Quaternion Pose;
							Lexer.Pop();
							Pose.x =-(float)atof(Lexer.Pop());
							Pose.y = (float)atof(Lexer.Pop());
							Pose.z = (float)atof(Lexer.Pop());
							Pose.w = (float)atof(Lexer.Pop());
							Lexer.Pop();
							D3DXQuaternionInverse(&Pose, &Pose);
							Quaternion.Pose.push_back(Pose);
						}
						Lexer.Pop();
						Motion.Quaternion.push_back(Quaternion);
					}
					pMotion->push_back(Motion);
				}

				return true;
			}

			//---------------------------------------------------------------------------
			//	Mikotoデータの作成
			//!	@param ppMikoto [out] Mikotoデータポインタ
			//!	@param pFileName [in] ファイル名
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	CreateMikoto(Mikoto** ppMikoto, const char* pFileName)
			{
				*ppMikoto = null;
				Mikoto* pData = new Mikoto;
				char cTemp[256];
				char cPath[256];
				strcpy(cPath, pFileName);
				cPath[strlen(cPath) - 3] = '\0';

				//	MKIファイルの読み込み
				sprintf(cTemp, "%s%s", cPath, "mki");
				if(	!CreateMKI(&pData->MKI, cTemp) )
				{
					SafeDelete(pData);
					return false;
				}
				//	MKMファイルの読み込み
				sprintf(cTemp, "%s%s", cPath, "mkm");
				if( !CreateMotion(&pData->Motion, cTemp) )
				{
					SafeDelete(pData);
					return false;
				}

				*ppMikoto = pData;
				return true;
			}

			//---------------------------------------------------------------------------
			//	ボーンデータの作成
			//!	@param pSkinMesh [out] スキンメッシュデータポインタ
			//!	@param pMKI [in] MKIデータポインタ
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	CreateBone(SkinMeshData* pSkinMesh, MKI* pMKI)
			{
				word i, j;
				Lib::Bone* pBone;
				//	ボーンの生成
				pSkinMesh->wBoneNum = (word)pMKI->Bone.size();
				pSkinMesh->pBone = new Lib::Bone[pSkinMesh->wBoneNum];
				ZeroMemory(pSkinMesh->pBone, sizeof(Lib::Bone) * pSkinMesh->wBoneNum);
				//	設定
				for(i = 0; i < pSkinMesh->wBoneNum; i++)
				{
					pBone = &pSkinMesh->pBone[i];
					pBone->wParentIndex = 0xFFFF;
					pBone->OriginalPose = pMKI->Bone[i].SPose;
					//	親子付け
					for(j = 0; j < pSkinMesh->wBoneNum; j++)
					{
						if( i == j )
							continue;
						if( strcmp(pMKI->Bone[i].cParent, pMKI->Bone[j].cName) )
							continue;
						pBone->wParentIndex = j;
						break;
					}
					//	位置の設定
					for(j = 0; j < pMKI->Locate.size(); j++)
					{
						if( strcmp(pMKI->Bone[i].cStartName, pMKI->Locate[j].cName) )
							continue;
						pBone->OriginalPos = pMKI->Bone[i].Pos = pMKI->Locate[j].SPos;
						break;
					}

					Matrix Mat;
					//	オフセットの設定
					D3DXMatrixRotationQuaternion(&pBone->OffsetMatrix, &pBone->OriginalPose);
					D3DXQuaternionInverse(&pBone->OriginalPose, &pBone->OriginalPose);
					//	オフセットの設定(親の影響)
					if( pBone->wParentIndex != 0xFFFF )
					{
						j = pBone->wParentIndex;
						Lib::Bone* pParent = &pSkinMesh->pBone[j];
						D3DXMatrixRotationQuaternion(&Mat, &pMKI->Bone[j].SPose);
						D3DXMatrixInverse(&Mat, null, &Mat);
						D3DXVec3TransformCoord(&pMKI->Bone[i].Pos, &pMKI->Bone[i].Pos, &Mat);
						pMKI->Bone[i].Pos += pMKI->Bone[j].Pos;
						//	姿勢の設定
						pBone->OriginalPose *= pMKI->Bone[j].SPose;
					}
					Math::Translation(&Mat, -pMKI->Bone[i].Pos);
					pBone->OffsetMatrix = Mat * pBone->OffsetMatrix;

					//	中心の取得
					D3DXMatrixInverse(&Mat, null, &pBone->OffsetMatrix);
					Vector3 Pos1(0.0f, 0.0f, 0.0f);
					Vector3 Pos2;
					for(j = 0; j < pMKI->Locate.size(); j++)
					{
						if( strcmp(pMKI->Bone[i].cEndName, pMKI->Locate[j].cName) )
							continue;
						Pos2 = pMKI->Locate[j].SPos;
						break;
					}
					D3DXVec3TransformCoord(&Pos1, &Pos1, &Mat);
					D3DXVec3TransformCoord(&Pos2, &Pos2, &Mat);
					pMKI->Bone[i].Center = (Pos1 + Pos2) / 2.0f;
				}
				return true;
			}

			//---------------------------------------------------------------------------
			//	モーションデータの作成
			//!	@param pSkinMesh [out] スキンメッシュデータポインタ
			//!	@param pMikoto [in] Mikotoデータポインタ
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	CreateMotion(SkinMeshData* pSkinMesh, Mikoto* pMikoto)
			{
				dword i, j, k;
				//	モーションの生成
				pSkinMesh->wMotionNum = (word)pMikoto->Motion.size();
				pSkinMesh->pMotion = new Lib::Motion[pSkinMesh->wMotionNum];
				ZeroMemory(pSkinMesh->pMotion, sizeof(Lib::Motion) * pSkinMesh->wMotionNum);
				Lib::Motion* pMotion;
				Motion* pMKMMotion;
				//	モーションの設定
				for(i = 0; i < pSkinMesh->wMotionNum; i++)
				{
					pMotion = &pSkinMesh->pMotion[i];
					pMKMMotion = &pMikoto->Motion[i];
					pMotion->wFrameNum = pMKMMotion->wFrameNum;
					pMotion->pBone = new Lib::Motion::Bone[pSkinMesh->wBoneNum];
					ZeroMemory(pMotion->pBone, sizeof(Lib::Motion::Bone) * pSkinMesh->wBoneNum);
					//	位置の設定
					for(j = 0; j < pMKMMotion->Vector.size(); j++)
					{
						for(k = 0; k < pSkinMesh->wBoneNum; k++)
						{
							if( strcmp(pMKMMotion->Vector[j].cName, pMikoto->MKI.Bone[k].cStartName) )
								continue;
							Lib::Motion::Bone* pBone = &pMotion->pBone[k];
							pBone->wPositionNum = (word)pMKMMotion->Vector[j].wFrame.size();
							pBone->pPositionFrame = new word[pBone->wPositionNum];
							pBone->pPosition = new Vector3[pBone->wPositionNum];
							memcpy(pBone->pPositionFrame, &pMKMMotion->Vector[j].wFrame[0], sizeof(word) * pBone->wPositionNum);
							memcpy(pBone->pPosition, &pMKMMotion->Vector[j].Pos[0], sizeof(Vector3) * pBone->wPositionNum);
						}
					}
					//	姿勢の設定
					for(j = 0; j < pMKMMotion->Quaternion.size(); j++)
					{
						for(k = 0; k < pSkinMesh->wBoneNum; k++)
						{
							if( strcmp(pMKMMotion->Quaternion[j].cName, pMikoto->MKI.Bone[k].cName) )
								continue;
							Lib::Motion::Bone* pBone = &pMotion->pBone[k];
							pBone->wRotationNum = (word)pMKMMotion->Quaternion[j].wFrame.size();
							pBone->pRotationFrame = new word[pBone->wRotationNum];
							pBone->pRotation = new Lib::Quaternion[pBone->wRotationNum];
							memcpy(pBone->pRotationFrame, &pMKMMotion->Quaternion[j].wFrame[0], sizeof(word) * pBone->wRotationNum);
							memcpy(pBone->pRotation, &pMKMMotion->Quaternion[j].Pose[0], sizeof(Lib::Quaternion) * pBone->wRotationNum);
							break;
						}
					}
				}
				return true;
			}

			//---------------------------------------------------------------------------
			//	アンカー情報の作成
			//!	@param pAnchor [out] アンカーデータポインタ
			//!	@param pMetasequoia [in] メタセコイアデータポインタ
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	CreateAnchor(std::vector<Anchor>* pAnchor, Metasequoia* pMetasequoia)
			{
				dword i, j, k, l;
				dword dwIndexNum;
				Object* pObject;
				char cTemp[32];
				ZeroMemory(cTemp, sizeof(cTemp));
				//	アンカーの生成
				for(i = 0; i < pMetasequoia->Object.size(); i++)
				{
					pObject = &pMetasequoia->Object[i];
					memcpy(cTemp, pObject->cName, sizeof(char) * 6);
					//	アンカー以外のオブジェクトの場合は無視
					if( strcmp(cTemp, "anchor") )
						continue;
					for(j = 0; pObject->cName[j] != '\0'; j++)
					{
						if( pObject->cName[j] == '|' )
							break;
					}
					if( pObject->cName[j] == '\0' )
						continue;
					//	アンカー情報の生成
					Anchor Anchor;
					ZeroMemory(&Anchor, sizeof(Anchor));
					strcpy(Anchor.cName, &pObject->cName[j + 1]);
					dwIndexNum = 0;
					for(j = 0; j < pObject->wFace.size(); j++)
					{
						for(k = 0; k < 3; k++)
								Anchor.Position.push_back(pObject->Position[pObject->wIndex[dwIndexNum + k]]);
						dwIndexNum += pObject->wFace[j];
					}
					Vector3 Pos[2];
					Pos[0] = Pos[1] = pObject->Position[0];
					//	中心を求める
					for(j = 1; j < pObject->Position.size(); j++)
					{
						Pos[0].x = ( Pos[0].x > pObject->Position[j].x )? pObject->Position[j].x: Pos[0].x;
						Pos[0].y = ( Pos[0].y > pObject->Position[j].y )? pObject->Position[j].y: Pos[0].y;
						Pos[0].z = ( Pos[0].z > pObject->Position[j].z )? pObject->Position[j].z: Pos[0].z;
						Pos[1].x = ( Pos[1].x < pObject->Position[j].x )? pObject->Position[j].x: Pos[1].x;
						Pos[1].y = ( Pos[1].y < pObject->Position[j].y )? pObject->Position[j].y: Pos[1].y;
						Pos[1].z = ( Pos[1].z < pObject->Position[j].z )? pObject->Position[j].z: Pos[1].z;
					}
					Anchor.Center = (Pos[0] + Pos[1]) / 2.0f;

					pAnchor->push_back(Anchor);
				}

				if( pAnchor->size() == 0 )
					return true;

				j = 0;
				std::vector<word> wIndex;
				//	頂点数の取得
				for(i = 0; i < pMetasequoia->Object.size(); i++)
					j += (dword)pMetasequoia->Object[i].Vertex.size();
				wIndex.resize(j, 0);

				ZeroMemory(cTemp, sizeof(cTemp));
				Anchor* pTmpAnchor;
				//	アンカーの関連付け
				for(i = 0; i < pAnchor->size(); i++)
				{
					pTmpAnchor = &(*pAnchor)[i];
					dwIndexNum = 0;
					for(j = 0; j < pMetasequoia->Object.size(); j++)
					{
						Object* pObject = &pMetasequoia->Object[j];
						if( pObject->Vertex.size() == 0 )
							continue;

						memcpy(cTemp, pObject->cName, sizeof(char) * 5);
						if( strcmp(cTemp, "sdef:" ) &&
							strcmp(&pObject->cName[5], pTmpAnchor->cName) )
							continue;

						Vector3 Normal, Vec;
						//	頂点
						for(k = 0; k < pObject->Vertex.size(); k++)
						{
							//	頂点がアンカー内にあるか調べる
							for(l = 0; l < pTmpAnchor->Position.size(); l += 3)
							{
								//	面の法線取得
								Math::Cross(&Normal, pTmpAnchor->Position[l + 1] - pTmpAnchor->Position[l], pTmpAnchor->Position[l + 2] - pTmpAnchor->Position[l]);
								//	点から面へのベクトル取得
								Vec = (pTmpAnchor->Position[l] + pTmpAnchor->Position[l + 1] + pTmpAnchor->Position[l + 2]) / 3.0f;
								Vec = Vec - pObject->Vertex[k];
								//	点が面の内側にあるか確認
								if( Math::Dot(Normal, Vec) < 0 )
									break;
							}
							//	アンカー内にあれば設定
							if( l == pTmpAnchor->Position.size() )
							{
								pTmpAnchor->dwIndex.push_back(k + dwIndexNum);
								wIndex[k + dwIndexNum]++;
							}
						}
						dwIndexNum += (dword)pObject->Vertex.size();
					}
				}

				dwIndexNum = 0;
				//	関連付けされていない頂点を設定する
				for(i = 0; i < pMetasequoia->Object.size(); i++)
				{
					pObject = &pMetasequoia->Object[i];
					if( pObject->Vertex.size() == 0 )
						continue;
					memcpy(cTemp, pObject->cName, sizeof(char) * 5);
					if( strcmp(cTemp, "sdef:" ) )
					{
						dwIndexNum += (dword)pObject->Vertex.size();
						continue;
					}

					//	一番近いアンカーに関連付ける
					float fDist = 1000000.0f;
					dword dwNum = 0;
					for(j = 0; j < pObject->Vertex.size(); j++)
					{
						if( wIndex[dwIndexNum + j] )
							continue;
						for(k = 0; k < pAnchor->size(); k++)
						{
							pTmpAnchor = &(*pAnchor)[k];
							if( fDist < Math::Length2(pTmpAnchor->Center - pObject->Vertex[j]) )
								continue;
							fDist = Math::Length2(pTmpAnchor->Center - pObject->Vertex[j]);
							dwNum = k;
						}
						pTmpAnchor->dwIndex.push_back(dwIndexNum + j);
						wIndex[dwIndexNum + j]++;
					}
					dwIndexNum += (dword)pObject->Vertex.size();
				}

				//	頂点の影響量設定
				for(i = 0; i < pAnchor->size(); i++)
				{
					pTmpAnchor = &(*pAnchor)[i];
					pTmpAnchor->fInfluence.resize(pTmpAnchor->dwIndex.size(), 1.0f);
					for(j = 0; j < pTmpAnchor->fInfluence.size(); j++)
						pTmpAnchor->fInfluence[j] /= (float)wIndex[pTmpAnchor->dwIndex[j]];
				}

				return true;
			}

			//---------------------------------------------------------------------------
			//!	ボーンに関連付け
			//!	@param pMKI [out] MKIポインタ
			//!	@param pAnchor [in] アンカーポインタ
			//!	@param pMetasequoia [in] メタセコイアデータポインタ
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	SetBone(MKI* pMKI, std::vector<Anchor>* pAnchor, Metasequoia* pMetasequoia)
			{
				dword i, j, k;				//	ボーンとアンカーの関連付け
				if( pAnchor->size() )
				{
					//	一番近いアンカーをセットする
					for(i = 0; i < pAnchor->size(); i++)
					{
						dword dwBoneIndex;
						float fDist = 1000000.0f;
						Anchor* pTmpAnchor = &(*pAnchor)[i];

						for(j = 0; j < pMKI->Bone.size(); j++)
						{
							if( fDist < Math::Length2(pMKI->Bone[j].Center - pTmpAnchor->Center) )
								continue;
							fDist = Math::Length2(pMKI->Bone[j].Center - pTmpAnchor->Center);
							dwBoneIndex = j;
						}
						pMKI->Bone[dwBoneIndex].dwIndex = pTmpAnchor->dwIndex;
						pMKI->Bone[dwBoneIndex].fInfluence = pTmpAnchor->fInfluence;
					}
				}

				dword dwIndexNum = 0;
				Object* pObject;
				char cTemp[32];
				//	アンカーの無い頂点の設定
				for(i = 0; i < pMetasequoia->Object.size(); i++)
				{
					pObject = &pMetasequoia->Object[i];
					if( pObject->Vertex.size() == 0 )
						continue;
					memcpy(cTemp, pObject->cName, sizeof(char) * 6);
					//	ボーン、アンカー、sdefの場合は無視
					if( cTemp[0] == 'a' )
					{
						cTemp[6] = '\0';
						if( strcmp(cTemp, "anchor") == 0 )
							continue;
					}
					cTemp[5] = '\0';
					if( strcmp(cTemp, "bone:") == 0 )
						continue;
					if( strcmp(cTemp, "sdef:" ) == 0 )
					{
						dwIndexNum += (dword)pObject->Vertex.size();
						continue;
					}

					Vector3 Pos[2];
					Pos[0] = Pos[1] = pObject->Vertex[0];
					//	オブジェクトの中心取得
					for(j = 1; j < pObject->Vertex.size(); j++)
					{
						Pos[0].x = ( Pos[0].x > pObject->Vertex[j].x )? pObject->Vertex[j].x: Pos[0].x;
						Pos[0].y = ( Pos[0].y > pObject->Vertex[j].y )? pObject->Vertex[j].y: Pos[0].y;
						Pos[0].z = ( Pos[0].z > pObject->Vertex[j].z )? pObject->Vertex[j].z: Pos[0].z;
						Pos[1].x = ( Pos[1].x < pObject->Vertex[j].x )? pObject->Vertex[j].x: Pos[1].x;
						Pos[1].y = ( Pos[1].y < pObject->Vertex[j].y )? pObject->Vertex[j].y: Pos[1].y;
						Pos[1].z = ( Pos[1].z < pObject->Vertex[j].z )? pObject->Vertex[j].z: Pos[1].z;
					}
					Pos[0] = (Pos[0] + Pos[1]) / 2.0f;

					dword dwNum = 0;
					float fDist = 100000.0f;
					//	一番近いボーンとオブジェクトを探す
					for(j = 0; j < pMKI->Bone.size(); j++)
					{
						if( fDist < Math::Length2(Pos[0] - pMKI->Bone[j].Center) )
							continue;
						fDist = Math::Length2(Pos[0] - pMKI->Bone[j].Center);
						dwNum = j;
					}

					//	ボーンとオブジェクトを関連付け
					for(k = 0; k < pObject->Vertex.size(); k++)
					{
						pMKI->Bone[dwNum].dwIndex.push_back(dwIndexNum + k);
						pMKI->Bone[dwNum].fInfluence.push_back(1.0f);
					}
					dwIndexNum += (dword)pObject->Vertex.size();
				}
				return true;
			}

			//---------------------------------------------------------------------------
			//!	スキン情報の作成
			//!	@param pSkinMesh [out] スキンメッシュデータポインタ
			//!	@param pMetasequoia [in] メタセコイアデータポインタ
			//!	@param pMKI [in] MKIポインタ
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	CreateSkinInfo(SkinMeshData* pSkinMesh, Metasequoia* pMetasequoia, MKI* pMKI)
			{
				dword i;
				std::vector<Anchor> Anchor;
				//	アンカー生成
				if( !CreateAnchor(&Anchor, pMetasequoia) )
					return false;
				//	ボーンに関連付け
				if( !SetBone(pMKI, &Anchor, pMetasequoia) )
					return false;

				LPD3DXSKININFO pSkinInfo;
				//	スキン情報の生成
				D3DXCreateSkinInfoFVF(pSkinMesh->pMesh->GetNumVertices(), D3DFVF_VERTEX, pSkinMesh->wBoneNum, &pSkinInfo);
				//	ボーン設定
				for(i = 0; i < pSkinMesh->wBoneNum; i++)
				{
					if( pMKI->Bone[i].dwIndex.size() == 0 )
						continue;
					pSkinInfo->SetBoneInfluence(i, (dword)pMKI->Bone[i].dwIndex.size(), &pMKI->Bone[i].dwIndex[0], &pMKI->Bone[i].fInfluence[0]);
				}

				pSkinMesh->pSkinInfo = pSkinInfo;

				return true;
			}

			//---------------------------------------------------------------------------
			//!	スキンメッシュデータの作成
			//!	@param pSkinMesh [out] スキンメッシュデータポインタ
			//!	@param pMetasequoia [in] メタセコイアデータポインタ
			//!	@param pMikoto [in] Mikotoデータポインタ
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	CreateSkinMesh(SkinMeshData* pSkinMesh, Metasequoia* pMetasequoia, Mikoto* pMikoto)
			{
				//	ボーンの生成
				if( !CreateBone(pSkinMesh, &pMikoto->MKI) )
					return false;
				//	モーションの生成
				if( !CreateMotion(pSkinMesh, pMikoto) )
					return false;
				//	スキン情報の生成
				if( !CreateSkinInfo(pSkinMesh, pMetasequoia, &pMikoto->MKI) )
					return false;

				return true;
			}
		}

		//---------------------------------------------------------------------------
		//!	メッシュの作成
		//!	@param pFileName [in] ファイル名
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool CreateFromX(const char* pFileName)
		{
			MeshData Mesh;
			strcpy(Mesh.cFileName, pFileName);
			Mesh.dwCount = 0;

			HRESULT	hr;
			LPD3DXBUFFER pMaterialBuffer;
			LPDIRECT3DDEVICE9 pDevice = Lib::System::System::GetInstance()->GetDevice();
			//	メッシュの読み込み
			hr = D3DXLoadMeshFromX( pFileName,
									D3DXMESH_MANAGED,
									pDevice,
									null,
									&pMaterialBuffer,
									null,
									&Mesh.dwMaterialNum,
									&Mesh.pMesh );
			if( FAILED(hr) )
			{
				char cTemp[256];
				sprintf(cTemp, "Mesh(%s)の読み込みに失敗しました", pFileName);
				Message(cTemp, "エラー");
				return false;
			}

			//	法線がない場合、法線を追加する
			if( Mesh.pMesh->GetFVF() != D3DFVF_VERTEX )
			{
				LPD3DXMESH pMeshTemp;
				Mesh.pMesh->CloneMeshFVF(Mesh.pMesh->GetOptions(),
										 D3DFVF_VERTEX,
										 pDevice,
										 &pMeshTemp);
				D3DXComputeNormals(pMeshTemp, null);
				SafeRelease(Mesh.pMesh);
				Mesh.pMesh = pMeshTemp;
			}

			D3DXMATERIAL* pMaterials = (D3DXMATERIAL*)pMaterialBuffer->GetBufferPointer();

			Mesh.pMaterial = new Material[Mesh.dwMaterialNum];
			ZeroMemory(Mesh.pMaterial, sizeof(Material) * Mesh.dwMaterialNum);

			dword i;
			char cPath[256];
			//	ファイルのディレクトリ取得
			strcpy(cPath, pFileName);
			for(i = (dword)strlen(cPath); i > 0; i--)
			{
				if( IsDBCSLeadByte(cPath[i - 2]) )
				{
					i--;
					continue;
				}
				if( cPath[i - 1] == '\\' || cPath[i - 1] == '/' )
					break;
			}
			cPath[i] = '\0';

			//	材質の取得する
			for(i = 0; i < Mesh.dwMaterialNum; i++)
			{
				Material* pMaterial = &Mesh.pMaterial[i];
				pMaterial->Ambient.r = pMaterial->Diffuse.r = pMaterials[i].MatD3D.Diffuse.r;
				pMaterial->Ambient.g = pMaterial->Diffuse.g = pMaterials[i].MatD3D.Diffuse.g;
				pMaterial->Ambient.b = pMaterial->Diffuse.b = pMaterials[i].MatD3D.Diffuse.b;
				pMaterial->Diffuse.a = 1.0f;

				//	テクスチャの読み込み
				if( pMaterials[i].pTextureFilename )
				{
					char cTemp[256];
					sprintf(cTemp, "%s%s", cPath, pMaterials[i].pTextureFilename);
					Lib::TextureManager::Create(&pMaterial->pTexture, cTemp);

					WIN32_FIND_DATA fd;
					sprintf(cTemp, "%sN%s", cPath, pMaterials[i].pTextureFilename);
					HANDLE hFind = FindFirstFile(cTemp, &fd);
					BOOL bResult = (hFind != INVALID_HANDLE_VALUE);
					if( bResult )
						Lib::TextureManager::Create(&pMaterial->pNormal, cTemp);
				}
			}

			SafeRelease(pMaterialBuffer);
			MeshManage.push_back(Mesh);
			return true;
		}

		//---------------------------------------------------------------------------
		//!	メッシュの作成
		//!	@param pFileName [in] ファイル名
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool CreateFromMQO(const char* pFileName)
		{
			//	メタセコイアデータの読み込み
			Metasequoia::Metasequoia* pMetasequoia;
			if( !Metasequoia::CreateMetasequoia(&pMetasequoia, pFileName) )
				return false;

			//	メッシュ情報の設定
			MeshData Mesh;
			strcpy(Mesh.cFileName, pFileName);
			Mesh.dwCount = 0;

			//	メッシュの生成
			Metasequoia::CreateMesh(&Mesh, pMetasequoia);
			MeshManage.push_back(Mesh);

			SafeDelete(pMetasequoia);
			return true;
		}

		//---------------------------------------------------------------------------
		//!	メッシュの作成
		//!	@param pFileName [in] ファイル名
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool CreateFromKMO(const char* pFileName)
		{
			MeshData Mesh;
			strcpy(Mesh.cFileName, pFileName);
			Mesh.dwCount = 0;

			//	読み込み
			Lib::FileIO::File File;
			if( !File.ReadingFile(pFileName) )
				return false;

			dword i;
			char cPath[256];

			//	ファイルのディレクトリ取得
			strcpy(cPath, pFileName);
			for(i = (dword)strlen(cPath); i > 0; i--)
			{
				if( IsDBCSLeadByte(cPath[i - 2]) )
				{
					i--;
					continue;
				}
				if( cPath[i - 1] == '\\' || cPath[i - 1] == '/' )
					break;
			}
			cPath[i] = '\0';

			char cTemp[64];
			cTemp[4] = '\0';
			//	ファイルチェック
			File.Reading(cTemp, 4);
			if( strcmp(cTemp, "KMO ") )
			{
				char cTmp[256];
				sprintf(cTmp, "読み込まれたファイルが壊れているかKMOファイルではありません(%s)", pFileName);
				Message(cTmp, "エラー");
				File.CloseFile();
				return false;
			}
			File.Reading(cTemp, 4);
			float fVersion = (float)atof(cTemp);

			//	材質数取得
			File.Reading(&Mesh.dwMaterialNum, sizeof(dword));

			//	材質の生成
			Mesh.pMaterial = new Material[Mesh.dwMaterialNum];
			ZeroMemory(Mesh.pMaterial, sizeof(Material) * Mesh.dwMaterialNum);

			//	材質の読み込み
			for(i = 0; i < Mesh.dwMaterialNum; i++)
			{
				Material* pMaterial = &Mesh.pMaterial[i];
				File.Reading(&pMaterial->Diffuse, sizeof(Color4));
				File.Reading(&pMaterial->Ambient, sizeof(Color3));

				byte Size;
				//	テクスチャーパス数取得
				File.Reading(&Size, sizeof(byte));
				if( Size )
				{
					File.Reading(cTemp, sizeof(char) * Size);
					cTemp[Size] = '\0';
					char cTmp[256];

					//	テクスチャー読み込み
					sprintf(cTmp, "%s%s", cPath, cTemp);
					Lib::TextureManager::Create(&pMaterial->pTexture, cTmp);

					WIN32_FIND_DATA fd;
					sprintf(cTmp, "%sN%s", cPath, cTemp);
					HANDLE hFind = FindFirstFile(cTmp, &fd);
					BOOL bResult = (hFind != INVALID_HANDLE_VALUE);
					if( bResult )
						Lib::TextureManager::Create(&pMaterial->pNormal, cTmp);
				}
			}

			void*	pVertexs;
			void*	pIndex;
			dword*	pAttribute;
			dword	dwVertexNum;
			dword	dwFaceNum;

			//	頂点数、面数の取得
			File.Reading(&dwVertexNum, sizeof(dword));
			File.Reading(&dwFaceNum, sizeof(dword));

			LPDIRECT3DDEVICE9 pDevice = Lib::System::System::GetInstance()->GetDevice();
			//	メッシュの生成
			D3DXCreateMeshFVF(dwFaceNum, dwVertexNum, D3DXMESH_MANAGED, D3DFVF_VERTEX, pDevice, &Mesh.pMesh);
			LPD3DXMESH pMesh = Mesh.pMesh;

			//	情報セット
			pMesh->LockVertexBuffer(D3DLOCK_DISCARD, &pVertexs);
			File.Reading(pVertexs, sizeof(Vertex) * dwVertexNum);
			pMesh->UnlockVertexBuffer();

			pMesh->LockIndexBuffer(D3DLOCK_DISCARD, &pIndex);
			File.Reading(pIndex, sizeof(word) * dwFaceNum * 3);
			pMesh->UnlockIndexBuffer();

			pMesh->LockAttributeBuffer(D3DLOCK_DISCARD, &pAttribute);
			File.Reading(pAttribute, sizeof(dword) * dwFaceNum);
			pMesh->UnlockAttributeBuffer();

			File.CloseFile();

			MeshManage.push_back(Mesh);
			return true;
		}

		//---------------------------------------------------------------------------
		//!	メッシュの作成
		//!	@param ppMesh [out]	メッシュポインタ
		//!	@param pFileName [in] ファイル名
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool Create(MeshData** ppMesh, const char* pFileName)
		{
			dword i;
			//	既に読み込まれていないか確認
			for(i = 0; i < MeshManage.size(); i++)
			{
				if( strcmp(MeshManage[i].cFileName, pFileName) == 0 )
					break;
			}

			if( i == MeshManage.size() )
			{
				dword j;
				//	拡張子取得
				for(j = (dword)strlen(pFileName); j > 0; j--)
					if( pFileName[j - 1] == '.' )
						break;

				bool bResult;
				//	Xファイルの読み込み
				if( strcmp(&pFileName[j], "x") == 0 )
					bResult = CreateFromX(pFileName);
				//	MQOファイルの読み込み
				else if( strcmp(&pFileName[j], "mqo") == 0 )
					bResult = CreateFromMQO(pFileName);
				//	KMOファイルの読み込み
				else if( strcmp(&pFileName[j], "kmo") == 0 )
					bResult = CreateFromKMO(pFileName);
				else
				{
					char cTemp[256];
					sprintf(cTemp, "%sは対応していないファイルです(Mesh)", pFileName);
					Message(cTemp, "エラー");
					return false;
				}
				if( !bResult )
					return false;
			}

			(*ppMesh) = &MeshManage[i];
			MeshManage[i].dwCount++;
			return true;
		}

		//---------------------------------------------------------------------------
		//!	メッシュの削除
		//!	@param ppMesh [out] メッシュポインタ
		//---------------------------------------------------------------------------
		void Delete(LPD3DXMESH* ppMesh)
		{
			if( *ppMesh == null ) return;

			std::vector<MeshData>::iterator it;
			//	メッシュの管理されている場所を探す
			for(it = MeshManage.begin(); it != MeshManage.end(); it++)
			{
				if( it->pMesh == *ppMesh )
					break;
			}

			//	メッシュが管理されているか確認
			if( it != MeshManage.end() )
			{
				if( --it->dwCount == 0 )
				{
					SafeRelease(it->pMesh);
					for(dword i=0; i < it->dwMaterialNum; i++)
					{
						TextureManager::Delete(&it->pMaterial[i].pTexture);
						TextureManager::Delete(&it->pMaterial[i].pNormal);
					}
					SafeDeleteArray(it->pMaterial);
					MeshManage.erase(it);
				}
			}

			*ppMesh = null;
		}

		//---------------------------------------------------------------------------
		//!	スキンメッシュの作成(MQO,Mikoto)
		//!	@param pFileName [in] ファイル名
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	CreateFromMQOandMIKOTO(const char* pFileName)
		{
			//	メタセコイアデータの読み込み
			Metasequoia::Metasequoia* pMetasequoia;
			if( !Metasequoia::CreateMetasequoia(&pMetasequoia, pFileName) )
				return false;

			SkinMeshData SkinMesh;
			strcpy(SkinMesh.cFileName, pFileName);
			SkinMesh.dwCount = 0;
			//	メッシュの生成
			CreateMesh(&SkinMesh, pMetasequoia);

			//	Mikotoデータの読み込み
			Metasequoia::Mikoto* pMikoto;
			if( !Metasequoia::CreateMikoto(&pMikoto, pFileName) )
			{
				SafeDelete(pMetasequoia);
				return false;
			}

			//	スキン情報の生成
			Metasequoia::CreateSkinMesh(&SkinMesh, pMetasequoia, pMikoto);

			//	頂点情報の複製
			void* pVertexs;
			dword dwVertexNum = SkinMesh.pMesh->GetNumVertices();
			SkinMesh.pVertexs = new Vertex[dwVertexNum];
			SkinMesh.pMesh->LockVertexBuffer(D3DLOCK_READONLY, &pVertexs);
			memcpy(SkinMesh.pVertexs, pVertexs, sizeof(Vertex) * dwVertexNum);
			SkinMesh.pMesh->UnlockVertexBuffer();

			//	その他、必要データの生成
			SkinMesh.pMatrix = new Matrix[SkinMesh.wBoneNum];
			ZeroMemory(SkinMesh.pMatrix, sizeof(Matrix) * SkinMesh.wBoneNum);
			SkinMesh.pDrawMotion = new word;
			*SkinMesh.pDrawMotion = 0xFFFF;
			SkinMesh.pDrawFrame = new float;
			*SkinMesh.pDrawFrame = -1.0f;

			SkinMeshManage.push_back(SkinMesh);

			SafeDelete(pMetasequoia);
			SafeDelete(pMikoto);
			return true;
		}

		//---------------------------------------------------------------------------
		//!	スキンメッシュの作成(KAM)
		//!	@param pFileName [in] ファイル名
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	CreateFromKAM(const char* pFileName)
		{
			SkinMeshData SkinMesh;
			strcpy(SkinMesh.cFileName, pFileName);
			SkinMesh.dwCount = 0;

			//	読み込み
			Lib::FileIO::File File;
			if( !File.ReadingFile(pFileName) )
				return false;

			dword i, j;
			char cPath[256];

			//	ファイルのディレクトリ取得
			strcpy(cPath, pFileName);
			for(i = (dword)strlen(cPath); i > 0; i--)
			{
				if( IsDBCSLeadByte(cPath[i - 2]) )
				{
					i--;
					continue;
				}
				if( cPath[i - 1] == '\\' || cPath[i - 1] == '/' )
					break;
			}
			cPath[i] = '\0';

			char cTemp[64];
			cTemp[4] = '\0';
			//	ファイルチェック
			File.Reading(cTemp, 4);
			if( strcmp(cTemp, "KAM ") )
			{
				char cTmp[256];
				sprintf(cTmp, "読み込まれたファイルが壊れているかKAMファイルではありません(%s)", pFileName);
				Message(cTmp, "エラー");
				File.CloseFile();
				return false;
			}
			File.Reading(cTemp, 4);
			float fVersion = (float)atof(cTemp);

			File.Reading(&SkinMesh.dwMaterialNum, sizeof(dword));

			SkinMesh.pMaterial = new Material[SkinMesh.dwMaterialNum];
			ZeroMemory(SkinMesh.pMaterial, sizeof(Material) * SkinMesh.dwMaterialNum);

			//	材質の読み込み
			for(i = 0; i < SkinMesh.dwMaterialNum; i++)
			{
				Material* pMaterial = &SkinMesh.pMaterial[i];
				File.Reading(&pMaterial->Diffuse, sizeof(Color4));
				File.Reading(&pMaterial->Ambient, sizeof(Color3));

				byte Size;
				//	テクスチャーパス数取得
				File.Reading(&Size, sizeof(byte));
				if( Size )
				{
					File.Reading(cTemp, sizeof(char) * Size);
					cTemp[Size] = '\0';
					char cTmp[256];
					//	テクスチャー読み込み
					sprintf(cTmp, "%s%s", cPath, cTemp);
					Lib::TextureManager::Create(&pMaterial->pTexture, cTmp);

					WIN32_FIND_DATA fd;
					sprintf(cTmp, "%sN%s", cPath, cTemp);
					HANDLE hFind = FindFirstFile(cTmp, &fd);
					BOOL bResult = (hFind != INVALID_HANDLE_VALUE);
					if( bResult )
						Lib::TextureManager::Create(&pMaterial->pNormal, cTmp);
				}
			}

			void*	pVertexs;
			void*	pIndex;
			dword*	pAttribute;
			dword	dwVertexNum;
			dword	dwFaceNum;

			File.Reading(&dwVertexNum, sizeof(dword));
			File.Reading(&dwFaceNum, sizeof(dword));

			SkinMesh.pVertexs = new Vertex[dwVertexNum];
			File.Reading(SkinMesh.pVertexs, sizeof(Vertex) * dwVertexNum);

			LPDIRECT3DDEVICE9 pDevice = Lib::System::System::GetInstance()->GetDevice();
			//	メッシュの生成
			D3DXCreateMeshFVF(dwFaceNum, dwVertexNum, D3DXMESH_MANAGED, D3DFVF_VERTEX, pDevice, &SkinMesh.pMesh);
			LPD3DXMESH pMesh = SkinMesh.pMesh;

			//	情報セット
			pMesh->LockVertexBuffer(D3DLOCK_DISCARD, &pVertexs);
			memcpy(pVertexs, SkinMesh.pVertexs, sizeof(Vertex) * dwVertexNum);
			pMesh->UnlockVertexBuffer();

			pMesh->LockIndexBuffer(D3DLOCK_DISCARD, &pIndex);
			File.Reading(pIndex, sizeof(word) * dwFaceNum * 3);
			pMesh->UnlockIndexBuffer();

			pMesh->LockAttributeBuffer(D3DLOCK_DISCARD, &pAttribute);
			File.Reading(pAttribute, sizeof(dword) * dwFaceNum);
			pMesh->UnlockAttributeBuffer();

			File.Reading(&SkinMesh.wBoneNum, sizeof(word));

			SkinMesh.pBone = new Lib::Bone[SkinMesh.wBoneNum];
			ZeroMemory(SkinMesh.pBone, sizeof(Lib::Bone) * SkinMesh.wBoneNum);

			//	スキン情報の生成
			D3DXCreateSkinInfoFVF(dwVertexNum, D3DFVF_VERTEX, SkinMesh.wBoneNum, &SkinMesh.pSkinInfo);
			LPD3DXSKININFO pSkinInfo = SkinMesh.pSkinInfo;

			//	ボーン情報取得
			for(i = 0; i < SkinMesh.wBoneNum; i++)
			{
				Bone* pBone = &SkinMesh.pBone[i];

				File.Reading(&pBone->wParentIndex, sizeof(word));
				File.Reading(&pBone->OffsetMatrix, sizeof(Matrix));
				File.Reading(&pBone->OriginalPose, sizeof(Quaternion));
				File.Reading(&pBone->OriginalPos, sizeof(Vector3));

				dword dwIndexNum;
				dword* pIndex;
				float* pInfluence;

				File.Reading(&dwIndexNum, sizeof(dword));
				if( dwIndexNum == 0 )
					continue;

				pIndex = new dword[dwIndexNum];
				File.Reading(pIndex, sizeof(dword) * dwIndexNum);
				pInfluence = new float[dwIndexNum];
				File.Reading(pInfluence, sizeof(float) * dwIndexNum);

				pSkinInfo->SetBoneInfluence(i, dwIndexNum, pIndex, pInfluence);

				SafeDeleteArray(pIndex);
				SafeDeleteArray(pInfluence);
			}

			File.Reading(&SkinMesh.wMotionNum, sizeof(word));

			SkinMesh.pMotion = new Motion[SkinMesh.wMotionNum];
			ZeroMemory(SkinMesh.pMotion, sizeof(Motion) * SkinMesh.wMotionNum);

			//	モーション情報取得
			for(i = 0; i < SkinMesh.wMotionNum; i++)
			{
				Motion* pMotion = &SkinMesh.pMotion[i];

				File.Reading(&pMotion->wFrameNum, sizeof(word));

				pMotion->pBone = new Motion::Bone[SkinMesh.wBoneNum];
				ZeroMemory(pMotion->pBone, sizeof(Motion::Bone) * SkinMesh.wBoneNum);

				//	ボーン情報取得
				for(j = 0; j < SkinMesh.wBoneNum; j++)
				{
					Motion::Bone* pBone = &pMotion->pBone[j];

					File.Reading(&pBone->wRotationNum, sizeof(word));
					pBone->pRotationFrame = new word[pBone->wRotationNum];
					File.Reading(pBone->pRotationFrame, sizeof(word) * pBone->wRotationNum);
					pBone->pRotation = new Quaternion[pBone->wRotationNum];
					File.Reading(pBone->pRotation, sizeof(Quaternion) * pBone->wRotationNum);

					File.Reading(&pBone->wPositionNum, sizeof(word));
					pBone->pPositionFrame = new word[pBone->wPositionNum];
					File.Reading(pBone->pPositionFrame, sizeof(word) * pBone->wPositionNum);
					pBone->pPosition = new Vector3[pBone->wPositionNum];
					File.Reading(pBone->pPosition, sizeof(Vector3) * pBone->wPositionNum);
				}
			}

			//	その他、必要データの生成
			SkinMesh.pMatrix = new Matrix[SkinMesh.wBoneNum];
			ZeroMemory(SkinMesh.pMatrix, sizeof(Matrix) * SkinMesh.wBoneNum);
			SkinMesh.pDrawMotion = new word;
			*SkinMesh.pDrawMotion = 0xFFFF;
			SkinMesh.pDrawFrame = new float;
			*SkinMesh.pDrawFrame = -1.0f;

			File.CloseFile();

			SkinMeshManage.push_back(SkinMesh);

			return true;
		}

		//---------------------------------------------------------------------------
		//!	スキンメッシュの作成
		//!	@param ppMesh [out]	メッシュポインタ
		//!	@param pFileName [in] ファイル名
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool Create(SkinMeshData** ppSkinMesh, const char* pFileName)
		{
			dword i;
			//	既に読み込まれていないか確認
			for(i = 0; i < SkinMeshManage.size(); i++)
			{
				if( strcmp(SkinMeshManage[i].cFileName, pFileName) == 0 )
					break;
			}

			if( i == SkinMeshManage.size() )
			{
				dword j;
				//	拡張子取得
				for(j = (dword)strlen(pFileName); j > 0; j--)
					if( pFileName[j - 1] == '.' )
						break;

				bool bResult;
				//	MQO&MIKOTOファイルの読み込み
				if( strcmp(&pFileName[j], "mqo") == 0 )
					bResult = CreateFromMQOandMIKOTO(pFileName);
				//	KAMファイルの読み込み
				else if( strcmp(&pFileName[j], "kam") == 0 )
					bResult = CreateFromKAM(pFileName);
				else
				{
					char cTemp[256];
					sprintf(cTemp, "%sは対応していないファイルです(SkinMesh)", pFileName);
					Message(cTemp, "エラー");
					return false;
				}

				if( !bResult )
					return false;
			}

			(*ppSkinMesh) = &SkinMeshManage[i];
			SkinMeshManage[i].dwCount++;
			return true;
		}

		//---------------------------------------------------------------------------
		//!	スキンメッシュの削除
		//!	@param ppMesh [out] メッシュポインタ
		//---------------------------------------------------------------------------
		void Delete(LPD3DXSKININFO* pSkinInfo)
		{
			if( *pSkinInfo == null ) return;

			std::vector<SkinMeshData>::iterator it;
			//	スキンメッシュの管理されている場所を探す
			for(it = SkinMeshManage.begin(); it != SkinMeshManage.end(); it++)
			{
				if( it->pSkinInfo == *pSkinInfo )
					break;
			}

			//	スキンメッシュが管理されているか確認
			if( it != SkinMeshManage.end() )
			{
				if( --it->dwCount == 0 )
				{
					SafeRelease(it->pMesh);
					SafeRelease(it->pSkinInfo);
					for(dword i=0; i < it->dwMaterialNum; i++)
					{
						TextureManager::Delete(&it->pMaterial[i].pTexture);
						TextureManager::Delete(&it->pMaterial[i].pNormal);
					}
					SafeDeleteArray(it->pMaterial);
					SafeDeleteArray(it->pVertexs);
					SafeDeleteArray(it->pMatrix);
					SafeDeleteArray(it->pBone);
					for(word i = 0; i < it->wMotionNum; i++)
					{
						for(word j = 0; j < it->wBoneNum; j++)
						{
							SafeDeleteArray(it->pMotion[i].pBone[j].pRotationFrame);
							SafeDeleteArray(it->pMotion[i].pBone[j].pRotation);
							SafeDeleteArray(it->pMotion[i].pBone[j].pPositionFrame);
							SafeDeleteArray(it->pMotion[i].pBone[j].pPosition);
						}
						SafeDeleteArray(it->pMotion[i].pBone);
					}
					SafeDeleteArray(it->pMotion);
					SafeDelete(it->pDrawMotion);
					SafeDelete(it->pDrawFrame);

					SkinMeshManage.erase(it);
				}
			}

			*pSkinInfo = null;
		}

		//---------------------------------------------------------------------------
		//!	解放
		//---------------------------------------------------------------------------
		void	Cleanup(void)
		{
			dword i;
			//	未解放のデータがあれば解放しておく
			for(i = 0; i < MeshManage.size(); i++)
			{
				char cTemp[256];
				sprintf(cTemp, "%sが%d個解放されていません", MeshManage[i].cFileName, MeshManage[i].dwCount);
				Message(cTemp, "警告");
				Delete(&MeshManage[i].pMesh);
			}
			for(i = 0; i < SkinMeshManage.size(); i++)
			{
				char cTemp[256];
				sprintf(cTemp, "%sが%d個解放されていません", SkinMeshManage[i].cFileName, SkinMeshManage[i].dwCount);
				Message(cTemp, "警告");
				Delete(&SkinMeshManage[i].pSkinInfo);
			}
		}

		//---------------------------------------------------------------------------
		//	コンストラクタ
		//---------------------------------------------------------------------------
		Mesh::Mesh(void)
			: _pMesh(null), _dwMaterialNum(0)
			, _pMaterial(null)
		{
		}

		//---------------------------------------------------------------------------
		//	デストラクタ
		//!	@param pFileName [in] ファイル名
		//---------------------------------------------------------------------------
		Mesh::Mesh(const char* pFileName)
			: _pMesh(null), _pMaterial(null)
		{
			Load(pFileName);
		}

		//---------------------------------------------------------------------------
		//	コンストラクタ
		//---------------------------------------------------------------------------
		Mesh::~Mesh(void)
		{
			Lib::MeshManager::Delete(&_pMesh);
		}

		//---------------------------------------------------------------------------
		//	読み込み
		//!	@param pFileName [in] ファイル名
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	Mesh::Load(const char* pFileName)
		{
			Lib::MeshManager::Delete(&_pMesh);

			//	メッシュデータの生成
			MeshData* pData;
			if( !Create(&pData, pFileName) )
				return false;

			//	作成したデータのコピー
			_pMesh			= pData->pMesh;
			_dwMaterialNum	= pData->dwMaterialNum;
			_pMaterial		= pData->pMaterial;

			Lib::Math::Identity(&_TransMatrix);

			return true;
		}

		//---------------------------------------------------------------------------
		//	コンストラクタ
		//---------------------------------------------------------------------------
		SkinMesh::SkinMesh(void)
			: _pVertexs(null), _pSkinInfo(null)
			, _wMotion(0), _fFrame(0.0f), _bLoop(false)
			, _wBlendMotion(0xFFFF), _fChangeFrame(0.0f)
			, _pDrawMotion(null), _pDrawFrame(null)
		{
		}

		//---------------------------------------------------------------------------
		//	コンストラクタ
		//!	@param pFileName [in] ファイル名
		//---------------------------------------------------------------------------
		SkinMesh::SkinMesh(const char* pFileName)
			: _pVertexs(null), _pSkinInfo(null)
			, _wMotion(0), _fFrame(0.0f), _bLoop(false)
			, _wBlendMotion(0xFFFF), _fChangeFrame(0.0f)
			, _pDrawMotion(null), _pDrawFrame(null)
		{
			Load(pFileName);
		}

		//---------------------------------------------------------------------------
		//	デストラクタ
		//---------------------------------------------------------------------------
		SkinMesh::~SkinMesh(void)
		{
			Delete(&_pSkinInfo);
		}

		//---------------------------------------------------------------------------
		//	読み込み
		//!	@param pFileName [in] ファイル名
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	SkinMesh::Load(const char* pFileName)
		{
			Delete(&_pSkinInfo);

			//	メッシュデータの生成
			SkinMeshData* pData;
			if( !Create(&pData, pFileName) )
				return false;

			//	作成したデータのコピー
			_pMesh			= pData->pMesh;
			_dwMaterialNum	= pData->dwMaterialNum;
			_pMaterial		= pData->pMaterial;
			_pVertexs		= pData->pVertexs;
			_pSkinInfo		= pData->pSkinInfo;
			_pMatrix		= pData->pMatrix;
			_wBoneNum		= pData->wBoneNum;
			_pBone			= pData->pBone;
			_pMotion		= pData->pMotion;
			_pDrawMotion	= pData->pDrawMotion;
			_pDrawFrame		= pData->pDrawFrame;

			return true;
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================