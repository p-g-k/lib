//---------------------------------------------------------------------------
//!
//!	@file	Lib.InputManager.cpp
//!	@brief	入力関連
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace InputManager
	{
		//!	キーボード情報格納構造体
		struct KeyData
		{
			byte	cDisk[256];		//!< ボタン情報
			byte	cOldDisk[256];	//!< 前回のボタン情報
		};

		//!	マウス情報格納構造体
		struct MouseData
		{
			byte	cButton;	//!< ボタン情報
			byte	cOldButton;	//!< 前回のボタン情報
			int		iX, iY, iZ;	//!< X軸、Y軸、ホイール情報
		};

		//!	パッド情報格納構造体
		struct JoyData
		{
			int		iButton;	//!< ボタン情報
			int		iOldButton;	//!< 前回のボタン情報
			Vector2	LeftStick;	//!< 左スティックの状態
			Vector2	RightStick;	//!< 右スティックの状態
		};

		KeyData					KeyState;		//!< キーボードの状態保存用
		MouseData				MouseState;		//!< Mouseの状態保存用
		std::vector<JoyData>	JoyState;		//!< JoyStickの状態保存用
		dword					dwJoyNum = -1;	//!< JoyStick数

		HWND					hWnd;				//!< ウィンドウハンドル
		LPDIRECTINPUT8			pDInput = null;		//!< DirectInputオブジェクト
		LPDIRECTINPUTDEVICE8	pKeyboard = null;	//!< Inputデバイス
		LPDIRECTINPUTDEVICE8	pMouse = null;		//!< マウスデバイス
		DIMOUSESTATE2			DIMouse;			//!< マウスの情報取得用
		DIJOYSTATE2				DIJoyStick;			//!< パッドの状態取得用

		std::vector<DIDEVICEINSTANCE>		DIDInstance;	//!< JoyStick用デバイスインスタンス
		std::vector<LPDIRECTINPUTDEVICE8>	pJoyStick;		//!< JoyStickデバイス

		//---------------------------------------------------------------------------
		//!	デバイス毎に呼び出されるコールバック関数
		//!	@param pDIDInstance	[in] DirectInputデバイスのインスタンス
		//!	@param pvRef [in] リファレンスデータ
		//!	@return	結果
		//---------------------------------------------------------------------------
		int CALLBACK	EnumJoyCallBack(const DIDEVICEINSTANCE* pDIDInstance, void* pRef)
		{
			std::vector<DIDEVICEINSTANCE>* pTmp = (std::vector<DIDEVICEINSTANCE>*)pRef;
			pTmp->push_back(*pDIDInstance);
			return DIENUM_CONTINUE;
		}

		//---------------------------------------------------------------------------
		//!	ジョイスティックの軸を列挙するコールバック関数
		//!	@param pDDOI [in] DirectInputDeviceインスタンス
		//!	@param pvRef [in] リファレンスデータ
		//!	@return	結果
		//---------------------------------------------------------------------------
		int	CALLBACK	EnumAxesCallBack(LPCDIDEVICEOBJECTINSTANCE pDDOI, LPVOID pvRef)
		{
			HRESULT hr;
			DIPROPRANGE diprg;

			//デバイス入力データの設定
			ZeroMemory(&diprg , sizeof(diprg));
			diprg.diph.dwSize = sizeof(DIPROPRANGE);
			diprg.diph.dwHeaderSize = sizeof(diprg.diph);
			diprg.diph.dwObj = pDDOI->dwType;
			diprg.diph.dwHow = DIPH_BYID;
			diprg.lMin = -1000;
			diprg.lMax = 1000;
			hr = ((LPDIRECTINPUTDEVICE8)pvRef)->SetProperty(DIPROP_RANGE, &diprg.diph);

			if( FAILED(hr) ) return DIENUM_STOP;
			return DIENUM_CONTINUE;
		}

		//---------------------------------------------------------------------------
		//	初期化
		//!	@param hWnd [in] ウィンドウハンドル
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	Init(HWND hWnd)
		{
			ASSERT(pDInput == null, "既にInputManagerは初期化されています");

			HRESULT hr;
			//	DirectInputの生成
			hr = DirectInput8Create(GetModuleHandle(null), DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&pDInput, null);
			if( FAILED(hr) )
			{
				Message("DirectInputの生成に失敗しました", "失敗");
				return false;
			}

			InputManager::hWnd = hWnd;

			//---------------------------------------------------------------------------
			//	キーボードデバイスの作成
			//---------------------------------------------------------------------------
			hr = pDInput->CreateDevice(GUID_SysKeyboard, &pKeyboard, null);
			if( FAILED(hr) )
			{
				Message("キーボードデバイスの生成に失敗しました", "失敗");
				return false;
			}
			hr = pKeyboard->SetDataFormat(&c_dfDIKeyboard);
			if( FAILED(hr) )
			{
				Message("キーボードデバイスの設定に失敗しました", "失敗");
				return false;
			}
			hr = pKeyboard->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
			if( FAILED(hr) )
			{
				Message("キーボードデバイスの強調レベルの設定に失敗しました", "失敗");
				return false;
			}
			pKeyboard->Acquire();
			ZeroMemory(&KeyState, sizeof(KeyState));

			//---------------------------------------------------------------------------
			//	マウスデバイスの作成
			//---------------------------------------------------------------------------
			hr = pDInput->CreateDevice(GUID_SysMouse, &pMouse, null);
			if( FAILED(hr) )
			{
				Message("マウスデバイスの生成に失敗しました。", "失敗");
				return false;
			}
			//	マウスデバイスのフォーマットを設定
			hr = pMouse->SetDataFormat(&c_dfDIMouse2);
			if( FAILED(hr) )
			{
				Message("マウスデバイスの設定に失敗しました。", "失敗");
				return false;
			}
			//	マウスデバイスの協調レベルの設定
			hr = pMouse->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
			if( FAILED(hr) )
			{
				Message("マウスデバイスの強調レベルの設定に失敗しました。", "失敗");
				return false;
			}
			//	マウスデバイスの軸モード設定
			DIPROPDWORD diprop;
			diprop.diph.dwSize		 = sizeof(diprop);
			diprop.diph.dwHeaderSize = sizeof(diprop.diph);
			diprop.diph.dwObj		 = 0;
			diprop.diph.dwHow		 = DIPH_DEVICE;
			diprop.dwData			 = DIPROPAXISMODE_REL;	// 相対モード
			hr = pMouse->SetProperty(DIPROP_AXISMODE, &diprop.diph);
			if( FAILED(hr) )
			{
				Message("マウスデバイスの軸モードの設定に失敗しました。", "失敗");
				return false;
			}
			pMouse->Acquire();
			ZeroMemory(&MouseState, sizeof(MouseState));

			//---------------------------------------------------------------------------
			//	ジョイスティックデバイスの作成
			//---------------------------------------------------------------------------
			pDInput->EnumDevices(DI8DEVCLASS_GAMECTRL, EnumJoyCallBack, &DIDInstance, DIEDFL_ATTACHEDONLY);

			LPDIRECTINPUTDEVICE8 pDevice;
			for(dword i=0; i < DIDInstance.size(); i++)
			{
				//	デバイス生成
				pDInput->CreateDevice(DIDInstance[i].guidInstance, &pDevice, null);
				if( FAILED(hr) ) return false;

				//	デバイスフォーマットの設定
				pDevice->SetDataFormat(&c_dfDIJoystick2);
				//	強調レベルの設定
				hr = pDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
				if( FAILED(hr) )
				{
					SafeRelease(pDevice);
					return false;
				}

				//	ジョイスティックの軸の列挙
				hr = pDevice->EnumObjects(EnumAxesCallBack, pDevice, DIDFT_AXIS);
				if( FAILED(hr) )
				{
					SafeRelease(pDevice);
					return false;
				}
				pDevice->Acquire();
				pJoyStick.push_back(pDevice);
				JoyData State;
				ZeroMemory(&State, sizeof(JoyData));
				JoyState.push_back(State);
			}
			dwJoyNum = 0;

			return true;
		}

		//---------------------------------------------------------------------------
		//	解放
		//---------------------------------------------------------------------------
		void	Cleanup(void)
		{
			SafeRelease(pDInput);
			SafeRelease(pKeyboard);
			SafeRelease(pMouse);
			for(dword i=0; i < pJoyStick.size(); i++)
				SafeRelease(pJoyStick[i]);
		}

		//---------------------------------------------------------------------------
		//	更新
		//---------------------------------------------------------------------------
		void	Update(void)
		{
			HRESULT hr;
			dword i, j;

			//	ウィンドウが作動して無い場合
			if( hWnd != GetForegroundWindow() )
			{
				ZeroMemory(&KeyState, sizeof(KeyData));
				ZeroMemory(&MouseState, sizeof(MouseData));
				for(i = 0; i < JoyState.size(); i++)
					ZeroMemory(&JoyState[i], sizeof(JoyData));

				return;
			}

			//---------------------------------------------------------------------------
			//	キーボードの状態取得
			//---------------------------------------------------------------------------
			memcpy(KeyState.cOldDisk, KeyState.cDisk, sizeof(KeyState.cDisk));
			hr = pKeyboard->GetDeviceState(sizeof(KeyState.cDisk), &KeyState.cDisk);
			if( hr == DIERR_INPUTLOST )
				pKeyboard->Acquire();

			//---------------------------------------------------------------------------
			//	マウスの状態取得
			//---------------------------------------------------------------------------
			hr = pMouse->GetDeviceState(sizeof(DIMouse), &DIMouse);
			if( hr == DIERR_INPUTLOST ) pMouse->Acquire();

			MouseState.cOldButton = MouseState.cButton;
			MouseState.cButton = 0;
			for(i = 0; i < 8; i++)
				MouseState.cButton |= ((DIMouse.rgbButtons[i] & 0x80)? 1: 0) << i;
			MouseState.iX = DIMouse.lX;
			MouseState.iY = DIMouse.lY;
			MouseState.iZ = DIMouse.lZ;

			//---------------------------------------------------------------------------
			//	パッド毎のボタンの情報を取得する
			//---------------------------------------------------------------------------
			for(i = 0; i < dwJoyNum; i++)
			{
				pJoyStick[i]->Poll();
				//	パッドの状態取得
				hr = pJoyStick[i]->GetDeviceState(sizeof(DIJoyStick), &DIJoyStick);
				if( hr == DIERR_INPUTLOST ) pJoyStick[i]->Acquire();

				JoyState[i].iOldButton = JoyState[i].iButton;
				JoyState[i].iButton = 0;

				//	ボタンの状態をセットする
				for(j = 0; j < 28; j++)
					JoyState[i].iButton |= ((DIJoyStick.rgbButtons[j] & 0x80)? 1: 0) << j;

				//	十字キーの状態セット
				JoyState[i].iButton |= (((DIJoyStick.rgdwPOV[0] == 0) | (DIJoyStick.rgdwPOV[0] == 4500) | (DIJoyStick.rgdwPOV[0] == 31500))? 1: 0) << EKeyCode::EJoyStick::UP;
				JoyState[i].iButton |= (((DIJoyStick.rgdwPOV[0] == 13500) | (DIJoyStick.rgdwPOV[0] == 18000) | (DIJoyStick.rgdwPOV[0] == 22500))? 1: 0) << EKeyCode::EJoyStick::DOWN;
				JoyState[i].iButton |= (((DIJoyStick.rgdwPOV[0] == 22500) | (DIJoyStick.rgdwPOV[0] == 27000) | (DIJoyStick.rgdwPOV[0] == 31500))? 1: 0) << EKeyCode::EJoyStick::LEFT;
				JoyState[i].iButton |= (((DIJoyStick.rgdwPOV[0] == 4500) | (DIJoyStick.rgdwPOV[0] == 9000) | (DIJoyStick.rgdwPOV[0] == 13500))? 1: 0) << EKeyCode::EJoyStick::RIGHT;

				//	左スティックの状態取得
				JoyState[i].LeftStick.x = static_cast<float>(DIJoyStick.lX) / 1000.0f;
				JoyState[i].LeftStick.y = static_cast<float>(DIJoyStick.lY) / 1000.0f;
				if( (abs(JoyState[i].LeftStick.x)  < AXIS_MARGIN) && 
					(abs(JoyState[i].LeftStick.y) < AXIS_MARGIN) )
					JoyState[i].LeftStick.x = JoyState[i].LeftStick.y = 0.0f;

				//	右スティックの状態取得
				JoyState[i].RightStick.x = static_cast<float>(DIJoyStick.lZ) / 1000.0f;
				JoyState[i].RightStick.y = static_cast<float>(DIJoyStick.lRz) / 1000.0f;
				if( (abs(JoyState[i].RightStick.x)  < AXIS_MARGIN) &&
					(abs(JoyState[i].RightStick.y) < AXIS_MARGIN) )
					JoyState[i].RightStick.x = JoyState[i].RightStick.y = 0.0f;
			}
		}

		//---------------------------------------------------------------------------
		//	ジョイスティックの数を取得
		//!	@return ジョイスティックの数
		//---------------------------------------------------------------------------
		dword	GetJoyStickNum(void)
		{
			return (dword)pJoyStick.size();
		}

		//---------------------------------------------------------------------------
		//	コンストラクタ
		//---------------------------------------------------------------------------
		Keyboard::Keyboard(void)
		{
		}

		//---------------------------------------------------------------------------
		//	デストラクタ
		//---------------------------------------------------------------------------
		Keyboard::~Keyboard(void)
		{
		}

		//---------------------------------------------------------------------------
		//	キーが押されているかチェック
		//!	@param iKey [in] キー番号
		//!	@return	結果
		//---------------------------------------------------------------------------
		bool	Keyboard::IsKeyPress(int iKey)
		{
			return (KeyState.cDisk[iKey] & 0x80)? true: false;
		}

		//---------------------------------------------------------------------------
		//	キーを押したかチェック
		//!	@param iKey [in] キー番号
		//!	@return	結果
		//---------------------------------------------------------------------------
		bool	Keyboard::IsKeyPush(int iKey)
		{
			return (((KeyState.cDisk[iKey] ^ KeyState.cOldDisk[iKey]) & KeyState.cDisk[iKey]) & 0x80)? true: false;
		}

		//---------------------------------------------------------------------------
		//	キーが離されたかチェック
		//!	@param iKey [in] キー番号
		//!	@return	結果
		//---------------------------------------------------------------------------
		bool	Keyboard::IsKeyRelease(int iKey)
		{
			return (((KeyState.cDisk[iKey] ^ KeyState.cOldDisk[iKey]) & KeyState.cOldDisk[iKey]) & 0x80)? true: false;
		}

		//---------------------------------------------------------------------------
		//	コンストラクタ
		//---------------------------------------------------------------------------
		Mouse::Mouse(void)
		{
		}

		//---------------------------------------------------------------------------
		//	デストラクタ
		//---------------------------------------------------------------------------
		Mouse::~Mouse(void)
		{
		}

		//---------------------------------------------------------------------------
		//	キーが押されているかチェック
		//!	@param iKey [in] キー番号
		//!	@return	結果
		//---------------------------------------------------------------------------
		bool	Mouse::IsKeyPress(int iKey)
		{
			return (MouseState.cButton & (1 << iKey))? true: false;
		}

		//---------------------------------------------------------------------------
		//	キーを押したかチェック
		//!	@param iKey [in] キー番号
		//!	@return	結果
		//---------------------------------------------------------------------------
		bool	Mouse::IsKeyPush(int iKey)
		{
			return (((MouseState.cButton ^ MouseState.cOldButton) & MouseState.cButton) & (1 << iKey))? true: false;
		}

		//---------------------------------------------------------------------------
		//	キーが離されたかチェック
		//!	@param iKey [in] キー番号
		//!	@return	結果
		//---------------------------------------------------------------------------
		bool	Mouse::IsKeyRelease(int iKey)
		{
			return (((MouseState.cButton ^ MouseState.cOldButton) & MouseState.cOldButton) & (1 << iKey))? true: false;
		}

		//---------------------------------------------------------------------------
		//!	Xの移動量取得
		//!	@return	Xの移動量
		//---------------------------------------------------------------------------
		int		Mouse::GetX(void)
		{
			return MouseState.iX;
		}

		//---------------------------------------------------------------------------
		//!	Yの移動量取得
		//!	@return	Yの移動量
		//---------------------------------------------------------------------------
		int		Mouse::GetY(void)
		{
			return MouseState.iY;
		}

		//---------------------------------------------------------------------------
		//!	ホイールの回転量取得
		//!	@return	ホイールの回転量
		//---------------------------------------------------------------------------
		int		Mouse::GetWheel(void)
		{
			return MouseState.iZ;
		}


		//---------------------------------------------------------------------------
		//	ポインターの座標取得
		//!	@return	位置
		//---------------------------------------------------------------------------
		POINT	Mouse::GetPosition(void)
		{
			POINT Point;
			GetCursorPos(&Point);
			ScreenToClient(Lib::System::System::GetInstance()->GetWindow()->GetWindowHandle(), &Point);
			return Point;
		}

		//---------------------------------------------------------------------------
		//	コンストラクタ
		//---------------------------------------------------------------------------
		JoyStick::JoyStick(void)
		{
			_dwNo = dwJoyNum++;

			if( JoyState.size() <= _dwNo )
			{
				char cTemp[256];
				sprintf(cTemp, "JoyStick(%d)が見つかりません", _dwNo);
				Message(cTemp, "エラー");
			}
		}

		//---------------------------------------------------------------------------
		//	デストラクタ
		//---------------------------------------------------------------------------
		JoyStick::~JoyStick(void)
		{
		}

		//---------------------------------------------------------------------------
		//	キーが押されているかチェック
		//!	@param iKey [in] キー番号
		//!	@return	結果
		//---------------------------------------------------------------------------
		bool	JoyStick::IsKeyPress(int iKey)
		{
			if( JoyState.size() <= _dwNo )
				return false;

			return (JoyState[_dwNo].iButton & (1 << iKey))? true: false;
		}

		//---------------------------------------------------------------------------
		//	キーを押したかチェック
		//!	@param iKey [in] キー番号
		//!	@return	結果
		//---------------------------------------------------------------------------
		bool	JoyStick::IsKeyPush(int iKey)
		{
			if( JoyState.size() <= _dwNo )
				return false;

			return (((JoyState[_dwNo].iButton ^ JoyState[_dwNo].iOldButton) & JoyState[_dwNo].iButton) & (1 << iKey))? true: false;
		}

		//---------------------------------------------------------------------------
		//	キーが離されたかチェック
		//!	@param iKey [in] キー番号
		//!	@return	結果
		//---------------------------------------------------------------------------
		bool	JoyStick::IsKeyRelease(int iKey)
		{
			if( JoyState.size() <= _dwNo ) return false;

			return (((JoyState[_dwNo].iButton ^ JoyState[_dwNo].iOldButton) & JoyState[_dwNo].iOldButton) & (1 << iKey))? true: false;
		}

		//---------------------------------------------------------------------------
		//	左スティックの状態取得
		//!	@return	左スティックの状態
		//---------------------------------------------------------------------------
		Vector2	JoyStick::GetAxisLeft(void)
		{
			if( JoyState.size() <= _dwNo )
				return Vector2(0, 0);

			return JoyState[_dwNo].LeftStick;
		}

		//---------------------------------------------------------------------------
		//	右スティックの状態取得
		//!	@return	右スティックの状態
		//---------------------------------------------------------------------------
		Vector2	JoyStick::GetAxisRight(void)
		{
			if( JoyState.size() <= _dwNo )
				return Vector2(0, 0);

			return JoyState[_dwNo].RightStick;
		}

		//---------------------------------------------------------------------------
		//	ジョイスティック番号の設定
		//!	@param dwNo [in] 番号
		//---------------------------------------------------------------------------
		void	JoyStick::SetNo(dword dwNo)
		{
			_dwNo = dwNo;
		}

		//---------------------------------------------------------------------------
		//	ジョイスティック番号の取得
		//!	@return	番号
		//---------------------------------------------------------------------------
		dword	JoyStick::GetNo(void)
		{
			return _dwNo;
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================