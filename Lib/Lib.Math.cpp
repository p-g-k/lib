//---------------------------------------------------------------------------
//!
//!	@file	Lib.Math.cpp
//!	@brief	計算関連
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace Math
	{
		unsigned int x = 123456789;
		unsigned int y = 362436069;
		unsigned int z = 521288629;
		unsigned int w = 88675123;

		//---------------------------------------------------------------------------
		//	初期シードの設定
		//!	@param iSeed [in] シード
		//---------------------------------------------------------------------------
		void	Seed(unsigned int iSeed)
		{
			w = iSeed;
		}

		//---------------------------------------------------------------------------
		//	乱数取得
		//!	@retrun 乱数
		//---------------------------------------------------------------------------
		int		Random(void)
		{
			unsigned int t;
			t = (x^(x << 11));
			x = y; y = z; z = w;
			return (w = (w^(w >> 19))^(t^(t >> 8))) & 0x7fffffff;
		}

		//---------------------------------------------------------------------------
		//	乱数取得
		//!	@param iMin [in] 最小値
		//!	@param iMax [in] 最大値
		//!	@retrun 乱数
		//---------------------------------------------------------------------------
		int		Random(int iMin, int iMax)
		{
			ASSERT(iMin <= iMax, "Randomの最小値と最大値の設定が間違っています。");

			return Random() % (iMax - iMin + 1) + iMin;
		}

		//---------------------------------------------------------------------------
		//	乱数取得
		//!	@param dMin [in] 最小値
		//!	@param dMax [in] 最大値
		//!	@retrun 乱数
		//---------------------------------------------------------------------------
		double	Random(double dMin, double dMax)
		{
			ASSERT(dMin <= dMax, "Randomの最小値と最大値の設定が間違っています。");

			return ((dMax - dMin) * ((double)(Random() & 0x7fff) / 32767.1) + dMin);
		}

		//---------------------------------------------------------------------------
		//	長さの2乗を取得
		//!	@param Vec [in] ベクトル
		//!	@return 長さの2乗
		//---------------------------------------------------------------------------
		float	Length2(const Vector3& Vec)
		{
			return (Vec.x * Vec.x) + (Vec.y * Vec.y) + (Vec.z * Vec.z);
		}

		//---------------------------------------------------------------------------
		//	長さを取得
		//!	@param Vec [in] ベクトル
		//!	@return 長さ
		//---------------------------------------------------------------------------
		float	Length(const Vector3& Vec)
		{
			return sqrtf(Length2(Vec));
		}

		//---------------------------------------------------------------------------
		//	長さの2乗を取得
		//!	@param Vec [in] ベクトル
		//!	@return 長さの2乗
		//---------------------------------------------------------------------------
		float	Length2(const Vector2& Vec)
		{
			return (Vec.x * Vec.x) + (Vec.y * Vec.y);
		}

		//---------------------------------------------------------------------------
		//	長さを取得
		//!	@param Vec [in] ベクトル
		//!	@return 長さ
		//---------------------------------------------------------------------------
		float	Length(const Vector2& Vec)
		{
			return sqrtf(Length2(Vec));
		}

		//---------------------------------------------------------------------------
		//	正規化
		//!	@param pOut [in] 正規化したベクトル
		//!	@param Vec [in] ベクトル
		//---------------------------------------------------------------------------
		void	Normalize(Vector3* pOut, const Vector3& Vec)
		{
			float length = Length(Vec);
			pOut->x = Vec.x / length;
			pOut->y = Vec.y / length;
			pOut->z = Vec.z / length;
		}

		//---------------------------------------------------------------------------
		//	内積
		//!	@param Vec1 [in] ベクトル1
		//!	@param Vec2 [in] ベクトル2
		//!	@return 2つのベクトルの角度(|a||b|cosθ)
		//---------------------------------------------------------------------------
		float	Dot(const Vector3& Vec1, const Vector3& Vec2)
		{
			return (Vec1.x * Vec2.x) + (Vec1.y * Vec2.y) + (Vec1.z * Vec2.z);
		}

		//---------------------------------------------------------------------------
		//	外積
		//!	@param pOut [out] 2つのベクトルに垂直な線
		//!	@param Vec1 [in] ベクトル1
		//!	@param Vec2 [in] ベクトル2
		//---------------------------------------------------------------------------
		void	Cross(Vector3* pOut, const Vector3& Vec1, const Vector3& Vec2)
		{
			pOut->x = (Vec1.y * Vec2.z) - (Vec2.y * Vec1.z);
			pOut->y = (Vec1.z * Vec2.x) - (Vec2.z * Vec1.x);
			pOut->z = (Vec1.x * Vec2.y) - (Vec2.x * Vec1.y);
		}

		//---------------------------------------------------------------------------
		//	初期化
		//!	@param pOut [out] 初期化されるマトリックス
		//---------------------------------------------------------------------------
		void	Identity(Matrix* pOut)
		{
			pOut->_11 = pOut->_22 = pOut->_33 = pOut->_44 = 1.0f;

			pOut->_12 = pOut->_13 = pOut->_14 =
			pOut->_21 = pOut->_23 = pOut->_24 =
			pOut->_31 = pOut->_32 = pOut->_34 =
			pOut->_41 = pOut->_42 = pOut->_43 = 0.0f;
		}

		//---------------------------------------------------------------------------
		//	座標変換行列作成
		//!	@param pOut [out] 変換行列
		//!	@param Position [in] 位置
		//---------------------------------------------------------------------------
		void	Translation(Matrix* pOut, const Vector3& Position)
		{
			pOut->_11 = pOut->_22 = pOut->_33 = pOut->_44 = 1.0f;

			pOut->_12 = pOut->_13 = pOut->_14 =
			pOut->_21 = pOut->_23 = pOut->_24 =
			pOut->_31 = pOut->_32 = pOut->_34 = 0.0f;

			pOut->_41 = Position.x;
			pOut->_42 = Position.y;
			pOut->_43 = Position.z;
		}

		//---------------------------------------------------------------------------
		//	X軸回りの回転行列作成
		//!	@param pOut [out] 変換行列
		//!	@param fAngle [in] 角度
		//---------------------------------------------------------------------------
		void	RotationX(Matrix* pOut, float fAngle)
		{
			float fSin = sinf(fAngle);
			float fCos = cosf(fAngle);

			pOut->_11 = pOut->_44 = 1.0f;

			pOut->_12 = pOut->_13 = pOut->_14 =
			pOut->_21 = pOut->_24 =
			pOut->_31 = pOut->_34 =
			pOut->_41 = pOut->_42 = pOut->_43 = 0.0f;

			pOut->_22 =  fCos;
			pOut->_23 =  fSin;

			pOut->_32 = -fSin;
			pOut->_33 =  fCos;
		}

		//---------------------------------------------------------------------------
		//	Y軸回りの回転行列作成
		//!	@param pOut [out] 変換行列
		//!	@param fAngle [in] 角度
		//---------------------------------------------------------------------------
		void	RotationY(Matrix* pOut, float fAngle)
		{
			float fSin = sinf(fAngle);
			float fCos = cosf(fAngle);

			pOut->_22 = pOut->_44 = 1.0f;

			pOut->_12 = pOut->_14 =
			pOut->_21 = pOut->_23 = pOut->_24 =
			pOut->_32 = pOut->_34 =
			pOut->_41 = pOut->_42 = pOut->_43 = 0;

			pOut->_11 =  fCos;
			pOut->_13 = -fSin;

			pOut->_31 =  fSin;
			pOut->_33 =  fCos;
		}

		//---------------------------------------------------------------------------
		//	Z軸回りの回転行列作成
		//!	@param pOut [out] 変換行列
		//!	@param fAngle [in] 角度
		//---------------------------------------------------------------------------
		void	RotationZ(Matrix* pOut, float fAngle)
		{
			float fSin = sinf(fAngle);
			float fCos = cosf(fAngle);

			pOut->_33 = pOut->_44 = 1.0f;

			pOut->_13 = pOut->_14 =
			pOut->_23 = pOut->_24 =
			pOut->_31 = pOut->_32 = pOut->_34 =
			pOut->_41 = pOut->_42 = pOut->_43 = 0;

			pOut->_11 =  fCos;
			pOut->_12 =  fSin;

			pOut->_21 = -fSin;
			pOut->_22 =  fCos;
		}

		//---------------------------------------------------------------------------
		//!	XYZ軸回りの回転行列作成
		//!	@param pOut [out] 変換行列
		//!	@param Rotate [in] XYZの角度
		//---------------------------------------------------------------------------
		void	RotationXYZ(Matrix* pOut, const Vector3& Rotate)
		{
			float fSX = sinf(Rotate.x);
			float fSY = sinf(Rotate.y);
			float fSZ = sinf(Rotate.z);

			float fCX = cosf(Rotate.x);
			float fCY = cosf(Rotate.y);
			float fCZ = cosf(Rotate.z);

			//	rotationZ * rotationX * rotationY
			pOut->_11 =  fCZ * fCY + fSZ * fSX * fSY;
			pOut->_12 =  fSZ * fCX;
			pOut->_13 = -fCZ * fSY + fSZ * fSX * fCY;
			pOut->_14 =  0;

			pOut->_21 = -fSZ * fCY + fCZ * fSX * fSY;
			pOut->_22 =  fCZ * fCX;
			pOut->_23 =  fSZ * fSY + fCZ * fSX * fCY;
			pOut->_24 =  0;

			pOut->_31 =  fCX * fSY;
			pOut->_32 = -fSX;
			pOut->_33 =  fCX * fCY;
			pOut->_34 =  0;

			pOut->_41 = pOut->_42 = pOut->_43 = pOut->_44 = 1.0f;
		}

		//---------------------------------------------------------------------------
		//	スケール変換行列作成
		//!	@param pOut [out] 変換行列
		//!	@param fScale [in] スケール
		//---------------------------------------------------------------------------
		void	Scaling(Matrix* pOut, float fScale)
		{
			pOut->_12 = pOut->_13 = pOut->_14 =
			pOut->_21 = pOut->_23 = pOut->_24 =
			pOut->_31 = pOut->_32 = pOut->_34 =
			pOut->_41 = pOut->_42 = pOut->_43 = 0.0f;

			pOut->_11 = pOut->_22 = pOut->_33 = fScale;
			pOut->_44 = 1.0f;
		}

		//---------------------------------------------------------------------------
		//	スケール変換行列作成
		//!	@param pOut [out] 変換行列
		//!	@param Scale [in] スケール
		//---------------------------------------------------------------------------
		void	Scaling(Matrix* pOut, const Vector3& Scale)
		{
			pOut->_12 = pOut->_13 = pOut->_14 =
			pOut->_21 = pOut->_23 = pOut->_24 =
			pOut->_31 = pOut->_32 = pOut->_34 =
			pOut->_41 = pOut->_42 = pOut->_43 = 0.0f;

			pOut->_11 = Scale.x;
			pOut->_22 = Scale.y;
			pOut->_33 = Scale.z;
			pOut->_44 = 1.0f;
		}

		//---------------------------------------------------------------------------
		//	任意軸回転行列作成
		//!	@param pOut [out] 変換行列
		//!	@param Axis [in] 回転軸
		//!	@param fAngle [in] 角度
		//---------------------------------------------------------------------------
		void	RotationAxis(Matrix* pOut, const Vector3& Axis, float fAngle)
		{
			float fSin = sinf(fAngle);
			float fCos = cosf(fAngle);

			pOut->_11 = Axis.x * Axis.x * (1 - fCos) + fCos;
			pOut->_12 = Axis.x * Axis.y * (1 - fCos) - Axis.z * fSin;
			pOut->_13 = Axis.x * Axis.z * (1 - fCos) + Axis.y * fSin;

			pOut->_21 = Axis.y * Axis.x * (1 - fCos) + Axis.z * fSin;
			pOut->_22 = Axis.y * Axis.y * (1 - fCos) + fCos;
			pOut->_23 = Axis.y * Axis.z * (1 - fCos) - Axis.x * fSin;

			pOut->_31 = Axis.z * Axis.x * (1 - fCos) - Axis.y * fSin;
			pOut->_32 = Axis.z * Axis.y * (1 - fCos) + Axis.x * fSin;
			pOut->_33 = Axis.z * Axis.z * (1 - fCos) + fCos;

			pOut->_14 = pOut->_24 = pOut->_34 = pOut->_44 = 1.0f;
			pOut->_41 = pOut->_42 = pOut->_43 = 0.0f;
		}

		//---------------------------------------------------------------------------
		//	1つ目のベクトルから2つ目のベクトルに変換する行列作成
		//!	@param pOut [out] 変換行列
		//!	@param Vec1 [in] 変換させる初期ベクトル
		//!	@param Vec2 [in] 変換後のベクトル
		//---------------------------------------------------------------------------
		void	RotationVector(Matrix* pOut, const Vector3& Vec1, const Vector3& Vec2)
		{
			Vector3 Axis;
			//	外積により2つのベクトルに垂直なベクトルを取得
			Cross(&Axis, Vec2, Vec1);
			Normalize(&Axis, Axis);

			//	内積により2つのベクトルのcosθを取得
			float fAngle = Dot(Vec2, Vec1) / (Length(Vec1) * Length(Vec2));
			//	cos⁻¹によりラジアン角取得
			fAngle = acosf(fAngle);

			//	変換行列の生成
			RotationAxis(pOut, Axis, fAngle);
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================