//---------------------------------------------------------------------------
//!
//!	@file	Lib.SceneManager.h
//!	@brief	シーン管理
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#pragma once

//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace SceneManager
	{
		//===========================================================================
		//!	シーンベース
		//===========================================================================
		class SceneBase
		{
		public:
			//!	コンストラクタ
			SceneBase(void) {}
			//!	デストラクタ
			virtual ~SceneBase(void) {};

			//!	初期化
			virtual bool	Init(void) = 0;
			//!	解放
			virtual void	Cleanup(void) = 0;
			//!	初期化(ロード用)
			virtual bool	InitLoad(void) { return true; }
			//!	解放(ロード用)
			virtual void	CleanupLoad(void) {}

			//!	更新
			virtual void	Update(double dElapsedTime) = 0;
			//!	描画
			virtual void	Render(double dElapsedTime) = 0;
			//!	更新(ロード用)
			virtual void	UpdateLoad(double dElapsedTime) {}
			//!	描画(ロード用)
			virtual void	RenderLoad(double dElapsedTime) {}
		};

		//===========================================================================
		//!	シーンマネージャー
		//===========================================================================
		class SceneManager
		{
		public:
			//!	コンストラクタ
			SceneManager(void);
			//!	デストラクタ
			~SceneManager(void);

			//!	更新
			void	Update(double dElapsedTime);
			//!	描画
			void	Render(double dElapsedTime);

			//!	シーン変更
			void	JumpScene(int iSceneNo);
			//!	現在のシーンを保存したままシーン変更する
			void	CallScene(int iSceneNo);
			//!	保存したシーンに戻る
			void	ReturnScene(void);

		private:
			SceneBase*	_pCurrentScene;	//!< 現在のシーン
			SceneBase*	_pBeforeScene;	//!< 前回のシーン
			bool		_bLoad;			//!< ロードフラグ
		};

		//!	シーンのセット
		void	SetScene(int iSceneNo, SceneBase* pScene, bool bLoad = false);
	}
}

//============================================================================
//	END OF FILE
//============================================================================