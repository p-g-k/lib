//---------------------------------------------------------------------------
//!
//!	@file	Lib.h
//!	@brief	Library ヘッダー
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#pragma once

//---------------------------------------------------------------------------
// define
//---------------------------------------------------------------------------
#if(_WIN32_WINNT < 0x501)
#undef	_WIN32_WINNT
#define	_WIN32_WINNT 0x0501
#endif

#define WIN32_LEAN_AND_MEAN

#ifndef	null
#define null 0
#endif	// ~#if null

// デバッグ用マクロ
#ifdef	_DEBUG
#define ASSERT(condition, string)	assert(condition && string)
#else	//~#if _DEBUG
#define ASSERT(condition, string)
#endif	//~#if _DEBUG

typedef unsigned long dword;
typedef unsigned short word;

//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include <windows.h>
#include <stdio.h>
#include <assert.h>
#include <mmsystem.h>
#include <string>
#include <vector>
#include <map>
#include <list>

//	警告対策
#pragma	warning(disable:4996)

//	DirectInputのバージョン設定
#define	DIRECTINPUT_VERSION	0x0800
// DirectX
#include <d3dx9.h>
#include <dinput.h>
#include <dsound.h>

//---------------------------------------------------------------------------
// Library
//---------------------------------------------------------------------------
namespace Lib
{
	typedef D3DXVECTOR2 	Vector2;
	typedef D3DXVECTOR3		Vector3;
	typedef D3DXVECTOR4		Vector4;
	typedef D3DXMATRIX		Matrix;
	typedef D3DXQUATERNION	Quaternion;

	//!	スレッドファンクション
	typedef unsigned int (__stdcall* ThreadFunction)(void*);

	//!	日時
	struct Date
	{
		int iYeah;		//!< 年
		int iMonth;		//!< 月
		int iDay;		//!< 日
		int iHour;		//!< 時
		int iMinute;	//!< 分
		int iSecond;	//!< 秒
	};

	//!	カラー
	struct Color3
	{
		float r;	//!< 赤
		float g;	//!< 緑
		float b;	//!< 青
	};

	//!	カラー(アルファ―付き)
	struct Color4
		: Color3
	{
		float a;	//!< 透過値
	};

	//!	材質
	struct Material
	{
		Color4 Diffuse;	//!< 拡散光
		Color3 Ambient;	//!< 環境光
		LPDIRECT3DTEXTURE9 pTexture;	//!< テクスチャー
		LPDIRECT3DTEXTURE9 pNormal;		//!< 法線マップ
	};

	//!	頂点情報
	struct Vertex
	{
		Vector3 Position;	//!< 位置
		Vector3 Normal;		//!< 法線
		Vector2 UV;			//!< UV座標
	};
	//!	頂点フォーマット
	const dword D3DFVF_VERTEX = D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1;

	//!	ボーン
	struct Bone
	{
		word		wParentIndex;	//!< 親
		Matrix		OffsetMatrix;	//!< オフセットマトリックス
		Matrix		Matrix;			//!< マトリックス
		Quaternion	OriginalPose;	//!< 基本姿勢
		Vector3		OriginalPos;	//!< 基本位置
		Quaternion	BeforePose;		//!< 前回の姿勢
		Vector3		BeforePos;		//!< 前回の位置
		Quaternion	Pose;			//!< 姿勢
		Vector3		Pos;			//!< 位置
	};

	//!	モーション
	struct Motion
	{
		//!	ボーン
		struct Bone
		{
			word		wRotationNum;	//!< 回転キーフレーム数
			word*		pRotationFrame;	//!< 回転キーフレーム
			Quaternion*	pRotation;		//!< 回転
			word		wPositionNum;	//!< 位置キーフレーム数
			word*		pPositionFrame;	//!< 位置キーフレーム
			Vector3*	pPosition;		//!< 位置
		};
		word	wFrameNum;	//!< フレーム数
		Bone*	pBone;		//!< ボーン毎のデータ
	};

	//!	ファイルポインター
	namespace EFilePointer
	{
		const int BEGIN = FILE_BEGIN;
		const int CURRENT = FILE_CURRENT;
		const int END = FILE_END;
	}

	//!	ファイルフォーマット
	namespace EFileFormat
	{
		const int BMP	 = 0x0001;
		const int JPG	 = 0x0002;
		const int PNG	 = 0x0004;
		const int TXT	 = 0x0008;
		const int ORIGIN = 0x4000;
		const int ALL	 = 0x8000;
	}

	//!	フォーマット
	namespace EFormat
	{
		const int LEFT = DT_LEFT;
		const int RIGHT = DT_RIGHT;
		const int TOP = DT_TOP;
		const int BOTTOM = DT_BOTTOM;
	}

	//!	ストリーム再生モード
	namespace EStreamMode
	{
		const int NORMAL = 0;
		const int FADEIN = 1;
		const int FADEOUT = 2;
	}

	//!	テクスチャーフォーマット
	namespace ETextureFormat
	{
		const int MANAGED = 0;
		const int RENDERTARGET = 1;
		const int SHADOWMAP = 2;
	}

	//!	キーコード
	namespace EKeyCode
	{
		//!	マウスコード
		namespace EMouse
		{
			const int LEFT = 0;
			const int RIGHT = 1;
			const int CENTER = 2;
		}
		//!	ジョイステックコード
		namespace EJoyStick
		{
			const int UP = 28;
			const int DOWN = 29;
			const int LEFT = 30;
			const int RIGHT = 31;
		}
	}

	//---------------------------------------------------------------------------
	//	解放用テンプレート
	//---------------------------------------------------------------------------
	template<class A> void SafeRelease(A& Pointer)
	{
		if( Pointer != null )
		{
			Pointer->Release();
			Pointer = null;
		}
	}

	template<class A> void SafeDelete(A& Pointer)
	{
		if( Pointer != null )
		{
			delete Pointer;
			Pointer = null;
		}
	}

	template<class A> void SafeDeleteArray(A& Pointer)
	{
		if( Pointer != null )
		{
			delete [] Pointer;
			Pointer = null;
		}
	}

	//---------------------------------------------------------------------------
	//!	範囲指定
	//!	@param Value [in] 値
	//!	@param Min [in] 最小値
	//!	@param Max [in] 最大値
	//!	@return 結果
	//---------------------------------------------------------------------------
	template<class A, class B, class C> A Clamp(A Value, B Min, C Max)
	{
		return min(max(Value, Min), Max);
	}

	//---------------------------------------------------------------------------
	//! メッセージボックスの表示
	//!	@param pString [in] 表示文字
	//!	@param pCaption [in] 見出し
	//!	@return 結果
	//---------------------------------------------------------------------------
	inline int	Message(const char* pString, const char* pCaption)
	{
		return MessageBox(null, pString, pCaption, MB_OK);
	}
}

// include Library
#include "Lib.CharSet.h"
#include "Lib.FileIO.h"
#include "Lib.Thread.h"
#include "Lib.Math.h"
#include "Lib.Time.h"
#include "Lib.Frame.h"
#include "Lib.InputManager.h"
#include "Lib.Profile.h"
#include "Lib.System.h"
#include "Lib.SoundManager.h"
#include "Lib.FontManager.h"
#include "Lib.Network.h"
#include "Lib.Camera.h"
#include "Lib.Shader.h"
#include "Lib.TextureManager.h"
#include "Lib.SpriteManager.h"
#include "Lib.MeshManager.h"
#include "Lib.SceneManager.h"

//============================================================================
//	END OF FILE
//============================================================================