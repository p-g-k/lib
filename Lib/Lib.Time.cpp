//---------------------------------------------------------------------------
//!
//!	@file	Lib.Time.cpp
//!	@brief	時間関連
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"
#include <time.h>

namespace Lib
{
	namespace Time
	{
		//---------------------------------------------------------------------------
		//	日時取得
		//!	@return 日時
		//---------------------------------------------------------------------------
		Date	GetData(void)
		{
			time_t timer;
			tm* pDate;

			timer = time(null);
			pDate = localtime(&timer);

			Date date;
			date.iYeah	 = pDate->tm_year + 1900;
			date.iMonth	 = pDate->tm_mon + 1;
			date.iDay	 = pDate->tm_mday;
			date.iHour	 = pDate->tm_hour;
			date.iMinute = pDate->tm_min;
			date.iSecond = pDate->tm_sec;
			return date;
		}

		//---------------------------------------------------------------------------
		//	コンストラクタ
		//---------------------------------------------------------------------------
		Timer::Timer(void)
		{
			LARGE_INTEGER li;
			QueryPerformanceFrequency(&li);
			_dFreq = (double)li.QuadPart;

			Reset();
		}

		//---------------------------------------------------------------------------
		//	リセット
		//---------------------------------------------------------------------------
		void	Timer::Reset(void)
		{
			LARGE_INTEGER li;
			QueryPerformanceCounter(&li);
			_dBegin = (double)li.QuadPart;
		}

		//---------------------------------------------------------------------------
		//	時間取得
		//!	@return	経過時間
		//---------------------------------------------------------------------------
		double	Timer::Get(void)
		{
			LARGE_INTEGER li;
			QueryPerformanceCounter(&li);
			return (((double)li.QuadPart - _dBegin) * 1000.0) / _dFreq;
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================