//---------------------------------------------------------------------------
//!
//!	@file	Lib.Profile.cpp
//!	@brief	プロファイラー
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"

#ifdef	_DEBUG

namespace Lib
{
	namespace Profile
	{
		//	インスタンスの実体生成
		Profiler Profiler::_Instance;

		//---------------------------------------------------------------------------
		//	コンストラクタ
		//---------------------------------------------------------------------------
		Profiler::Profiler(void)
			: _pRoot(null), _pCurrent(null)
			, _dwLogCount(0), _iTargetFPS(60)
		{
			Init();
		}

		//---------------------------------------------------------------------------
		//	デストラクタ
		//---------------------------------------------------------------------------
		Profiler::~Profiler(void)
		{
			Cleanup();
		}

		//---------------------------------------------------------------------------
		//	初期化
		//---------------------------------------------------------------------------
		void	Profiler::Init(void)
		{
			//	ログ情報の取得スペースを生成する
			_pRoot = new Log[LOG_MAX];
			ZeroMemory(_pRoot, sizeof(Log) * LOG_MAX);

			//	ルートログを生成する
			_pCurrent = CreateLog("root", null);
		}

		//---------------------------------------------------------------------------
		//	解放
		//---------------------------------------------------------------------------
		void	Profiler::Cleanup(void)
		{
			SafeDeleteArray(_pRoot);
		}

		//---------------------------------------------------------------------------
		//	フレームの開始をプロファイラーに伝える
		//---------------------------------------------------------------------------
		void	Profiler::Start(void)
		{
			Log* pLog = _pRoot;

			//	全てのログを更新する
			if( pLog->pChiled )
				UpdateLog(pLog->pChiled);

			_Timer.Reset();
		}

		//---------------------------------------------------------------------------
		//	計測開始
		//	@param pName [in] プロファイル名
		//---------------------------------------------------------------------------
		void	Profiler::Begin(const char* pName)
		{
			//	現在のカレントログを親に設定する
			Log* pParent  = _pCurrent;
			Log* pBrother = null;

			//	指定のプロファイル名を持つ既存のログが存在するか探す
			Log* pLog = pParent->pChiled;
			while( pLog )
			{
				if( pLog->pName == pName ) break;
				pBrother = pLog;
				pLog	 = pLog->pBrother;
			}

			if( pLog == null )
			{
				//	指定のプロファイルが存在しないので、新規ログを生成
				pLog = CreateLog(pName, pParent);

				if( pParent->pChiled )
					pBrother->pBrother = pLog;
				else
					pParent->pChiled = pLog;
			}

			pLog->dBeginTime = _Timer.Get();
			//	カレントログに自分をセット
			_pCurrent = pLog;
		}

		//---------------------------------------------------------------------------
		//	計測終了
		//	@param pName [in] プロファイル名
		//---------------------------------------------------------------------------
		void	Profiler::End(const char* pName)
		{
			if( strcmp(_pCurrent->pName, pName) )
			{
				char cTemp[256];
				sprintf(cTemp, "計測開始と計測終了が対になっていません(%s)", pName);
				Message(cTemp, "警告");
				ASSERT(false, "エラー");
			}

			_pCurrent->dEndTime = _Timer.Get();
			double dTime = _pCurrent->dEndTime - _pCurrent->dBeginTime;

			//	情報更新
			_pCurrent->iTmpCount++;
			_pCurrent->dTmpTotalTime += dTime;
			_pCurrent->dTmpMinTime = ( _pCurrent->dTmpMinTime > dTime )? dTime: _pCurrent->dTmpMinTime;
			_pCurrent->dTmpMaxTime = ( _pCurrent->dTmpMaxTime < dTime )? dTime: _pCurrent->dTmpMaxTime;

			//	カレントを親に戻す
			_pCurrent = _pCurrent->pParent;
		}

		//---------------------------------------------------------------------------
		//!	ルートログの取得
		//!	@return	ルートログ
		//---------------------------------------------------------------------------
		Log*	Profiler::GetRootLog(void)
		{
			return _pRoot;
		}

		//---------------------------------------------------------------------------
		//!	インスタンスの取得
		//!	@return インスタンス
		//---------------------------------------------------------------------------
		Profiler*	Profiler::GetInstance(void)
		{
			return &_Instance;
		}

		//---------------------------------------------------------------------------
		//	計測終了
		//	@param pName [in] プロファイル名
		//	@param pParent [in] 親となるログ情報ポインタ
		//	@param 新規のログ情報ポインタ
		//---------------------------------------------------------------------------
		Log*	Profiler::CreateLog(const char* pName, Log* pParent)
		{
			ASSERT(_dwLogCount + 1 < LOG_MAX, "これ以上のログの追加はできません");

			//	ログの初期化
			Log* pLog = &_pRoot[_dwLogCount];
			pLog->pParent	= pParent;
			pLog->pBrother	= null;
			pLog->pChiled	= null;

			pLog->dBeginTime	= 0;
			pLog->dEndTime		= 0;
			pLog->iTmpCount		= 0;
			pLog->dTmpTotalTime	= 0;
			pLog->dTmpMinTime	= 0xFFFFFFFF;
			pLog->dTmpMaxTime	= 0;

			pLog->pName			= pName;
			pLog->iCount		= 0;
			pLog->dTotalTime	= 0;
			pLog->dAverageTime	= 0;
			pLog->dMinTime		= 0xFFFFFFFF;
			pLog->dMaxTime		= 0;

			_dwLogCount++;

			return pLog;
		}

		//---------------------------------------------------------------------------
		//	ログ情報の更新
		//!	@param pLog [in,out] 更新されるログ情報
		//---------------------------------------------------------------------------
		void	Profiler::UpdateLog(Log* pLog)
		{
			while( pLog )
			{
				//	ログ情報の更新
				pLog->iCount		= pLog->iTmpCount;
				pLog->dTotalTime	= pLog->dTmpTotalTime;
				pLog->dAverageTime	= pLog->iCount != 0 ? (pLog->dTotalTime / (double)pLog->iCount) : 0;
				pLog->dMinTime		= pLog->dTmpMinTime;
				pLog->dMaxTime		= pLog->dTmpMaxTime;

				//	計測用変数の初期化
				pLog->dBeginTime	= 0;
				pLog->dEndTime		= 0;

				//	子がいれば再帰的に更新する
				if( pLog->pChiled )
					UpdateLog(pLog->pChiled);

				pLog = pLog->pBrother;
			}
		}

		//---------------------------------------------------------------------------
		//	コンストラクタ
		//!	@param pName [in] プロファイル名
		//---------------------------------------------------------------------------
		ScopeProfiler::ScopeProfiler(const char* pName)
			: _pName(pName)
		{
			Lib::Profile::Profiler::GetInstance()->Begin(pName);
		}

		//---------------------------------------------------------------------------
		//	デストラクタ
		//---------------------------------------------------------------------------
		ScopeProfiler::~ScopeProfiler(void)
		{
			Lib::Profile::Profiler::GetInstance()->End(_pName);
		}
	}
}

#endif // ~#if _DEBUG

//============================================================================
//	END OF FILE
//============================================================================