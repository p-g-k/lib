//---------------------------------------------------------------------------
//!
//!	@file	Lib.Frame.h
//!	@brief	フレーム
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#pragma once

//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace Frame
	{
		//===========================================================================
		//!	フレーム制御クラス
		//===========================================================================
		class FrameControl
		{
		public:
			//! コンストラクタ
			FrameControl(void);

			//!	更新
			bool	Update(void);
			//!	描画フレームの更新
			bool	UpdateRender(void);

			//!	スキップモード変更(trueの時に速度が遅い場合、描画の回数を減らす)
			void	SetSkip(bool bSkip);
			//!	1秒間に実行するフレーム数の設定
			void	SetTargetFPS(int iTargetFPS);
			//!	1秒間に実行するフレーム数の取得
			int		GetTargetFPS(void);
			//!	1フレームの経過時間
			double	GetElapsedTime(void);
			//!	1フレームの描画の経過時間
			double	GetRenderElapsedTime(void);
			//!	1フレームにかける目標時間
			double	GetTargetFrameTime(void);
			//!	経過時間の取得(秒単位)
			dword	GetTime(void);

		private:
			Time::Timer	_Timer;				//!< タイマー

			bool	_bSkip;					//!< 描画のスキップの有無
			int		_iTargetFPS;			//!< 1秒間に実行するフレーム数
			double	_dElapsedTime;			//!< 1フレームの経過時間取得用
			double	_dRenderElapsedTime;	//!< 1フレームの描画の経過時間取得用
			double	_dFrameTime;			//!< 1フレームにかける時間
			double	_dSecCount;				//!< 1秒間のカウント
			double	_dNextFrame;			//!< 次のフレームまでの予定時間
			bool	_bRender;				//!< 描画フラグ
			dword	_dwTime;				//!< 経過時間(秒単位)
		};

		//===========================================================================
		//!	FPS測定クラス
		//===========================================================================
		class FPSCounter
		{
		public:
			//!	コンストラクタ
			FPSCounter(void);

			//!	更新
			void	Update(double dElapsedTime);
			//!	FPS取得
			double	GetFPS(void);

		private:
			double	_dFPS;		//!< 現在のFPS
			double	_dTime;		//!< 1フレーム毎の経過時間
			int		_iFrame;	//!< 1秒間に実行されているフレーム数
		};
	}
}

//============================================================================
//	END OF FILE
//============================================================================