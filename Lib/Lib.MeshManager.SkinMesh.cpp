//---------------------------------------------------------------------------
//!
//!	@file	Lib.MeshManager.SkinMesh.cpp
//!	@brief	メッシュ管理
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace MeshManager
	{
		//---------------------------------------------------------------------------
		//	更新
		//---------------------------------------------------------------------------
		void	SkinMesh::Update(void)
		{
			UpdateMotion();
			UpdateBone();
		}

		//---------------------------------------------------------------------------
		//	更新
		//!	@param fFrame [in] 進めるフレーム
		//---------------------------------------------------------------------------
		void	SkinMesh::Update(float fFrame)
		{
			//	フレームを進める
			_fFrame += fFrame;

			//	モーション移行
			if( _fChangeFrame )
			{
				//	モーション移行終了判断
				if( _fFrame >= _fChangeFrame )
				{
					_fFrame -= _fChangeFrame;
					_fChangeFrame = 0;
				}
				return;
			}
			//	通常モーション
			if( _fFrame >= (float)_pMotion[_wMotion].wFrameNum )
			{
				if( _bLoop )
					_fFrame -= (float)_pMotion[_wMotion].wFrameNum;
				else
					_fFrame = (float)_pMotion[_wMotion].wFrameNum;
			}
		}

		//---------------------------------------------------------------------------
		//	描画
		//!	@param pShader [in] シェーダー
		//!	@param pTechnique [in] テクニック
		//---------------------------------------------------------------------------
		void	SkinMesh::Render(Shader::Shader* pShader, char* pTechnique, dword dwPass)
		{
			if( _wMotion != *_pDrawMotion ||
				_fFrame != *_pDrawFrame )
			{
				*_pDrawMotion = _wMotion;
				*_pDrawFrame = _fFrame;
				Update();
				UpdateSkinMesh();
			}
			Mesh::Render(pShader, pTechnique, dwPass);
		}

		//---------------------------------------------------------------------------
		//	描画
		//!	@param i [in] 材質番号
		//---------------------------------------------------------------------------
		void	SkinMesh::Render(dword i)
		{
			if( _wMotion != *_pDrawMotion ||
				_fFrame != *_pDrawFrame )
			{
				*_pDrawMotion = _wMotion;
				*_pDrawFrame = _fFrame;
				Update();
				UpdateSkinMesh();
			}
			Mesh::Render(i);
		}

		//---------------------------------------------------------------------------
		//	モーション
		//!	@param wMotion [in] モーション番号
		//!	@param bLoop [in] ループフラグ
		//!	@param fFrame [in] 移行フレーム
		//---------------------------------------------------------------------------
		void	SkinMesh::SetMotion(word wMotion, bool bLoop , float fFrame)
		{
			_wMotion = wMotion;
			_bLoop = bLoop;
			_fChangeFrame = fFrame;
			_wBlendMotion = 0xFFFF;
			_fFrame = 0;
		}

		//---------------------------------------------------------------------------
		//	モーション
		//!	@param wMotion1 [in] モーション番号
		//!	@param wMotion2 [in] ブレンドするモーション番号
		//!	@param fPower [in] ブレンド係数(0以下の場合、両方を減衰なく動かす)
		//!	@param bLoop [in] ループフラグ
		//!	@param fFrame [in] 移行フレーム
		//---------------------------------------------------------------------------
		void	SkinMesh::SetMotionBlend(word wMotion1, word wMotion2, float fPower, bool bLoop, float fFrame)
		{
			_wMotion = wMotion1;
			_bLoop = bLoop;
			_fChangeFrame = fFrame;
			_wBlendMotion = wMotion2;
			_fBlendPower = fPower;
			_fFrame = 0;
		}

		//---------------------------------------------------------------------------
		//	フレームのリセット
		//---------------------------------------------------------------------------
		void	SkinMesh::Reset(void)
		{
			_fFrame = 0;
		}

		//---------------------------------------------------------------------------
		//	モーションが終了しているか確認
		//!	@return 終了フラグ
		//---------------------------------------------------------------------------
		bool	SkinMesh::IsEnd(void)
		{
			return _fFrame == (float)_pMotion[_wMotion].wFrameNum;
		}

		//---------------------------------------------------------------------------
		//	ブレンドの度合い(係数)のセット
		//!	@param fPower [in] ブレンド係数
		//---------------------------------------------------------------------------
		void	SkinMesh::SetBlendPower(float fPower)
		{
			_fBlendPower = fPower;
		}

		//---------------------------------------------------------------------------
		//	ブレンドの度合い(係数)の取得
		//!	@return ブレンド係数
		//---------------------------------------------------------------------------
		float	SkinMesh::GetBlendPower(void)
		{
			return _fBlendPower;
		}

		//---------------------------------------------------------------------------
		//	現在のフレーム取得
		//!	@return フレーム
		//---------------------------------------------------------------------------
		float	SkinMesh::GetFrame(void)
		{
			return _fFrame;
		}

		//---------------------------------------------------------------------------
		//	モーション移行フラグの取得		
		//!	@return モーション移行フラグ
		//---------------------------------------------------------------------------
		bool	SkinMesh::GetMotionShiftFlag(void)
		{
			return _fChangeFrame > 0;
		}

		//---------------------------------------------------------------------------
		//	モーション数の取得
		//!	@return モーション数
		//---------------------------------------------------------------------------
		word	SkinMesh::GetMotionNum(void)
		{
			return _wMotionNum;
		}

		//---------------------------------------------------------------------------
		//	現在のモーション取得
		//!	@return モーション
		//---------------------------------------------------------------------------
		word	SkinMesh::GetMotion(void)
		{
			return _wMotion;
		}

		//---------------------------------------------------------------------------
		//	ボーンの取得
		//!	@return ボーン
		//---------------------------------------------------------------------------
		Matrix*	SkinMesh::GetBone(word wNo)
		{
			return &_pBone[wNo].Matrix;
		}

		//---------------------------------------------------------------------------
		//	スキンメッシュのフレーム更新
		//!	@param wNo [in] ボーン番号
		//!	@param fFrame [in] フレーム
		//!	@param pOutPose [out] 姿勢
		//!	@param pOutPos [out] 位置
		//---------------------------------------------------------------------------
		void	SkinMesh::UpdateSkinMeshFrame(word wNo, float fFrame, Quaternion* pOutPose, Vector3* pOutPos)
		{
			int i;
			float t;

			Motion::Bone* pBone = &_pMotion[_wMotion].pBone[wNo];

			if( pBone->wRotationNum == 0 )
				*pOutPose = _pBone[wNo].OriginalPose;
			else if( pBone->wRotationNum == 1 )
				*pOutPose = pBone->pRotation[0];
			else if( fFrame >= pBone->pRotationFrame[pBone->wRotationNum - 1] )
				*pOutPose = pBone->pRotation[pBone->wRotationNum - 1];
			else
			{
				//	フレームから姿勢を算出
				for(i = 0; i < pBone->wRotationNum - 1; i++)
				{
					if( fFrame >= pBone->pRotationFrame[i] && fFrame < pBone->pRotationFrame[i + 1] )
					{
						t = (fFrame - (float)pBone->pRotationFrame[i]) / (float)(pBone->pRotationFrame[i + 1] - pBone->pRotationFrame[i]);
						D3DXQuaternionSlerp(pOutPose, &pBone->pRotation[i], &pBone->pRotation[i + 1], t);
						break;
					}
				}
			}

			if( pBone->wPositionNum == 0 )
				*pOutPos = _pBone[wNo].OriginalPos;
			else if( pBone->wPositionNum == 1 )
				*pOutPos = pBone->pPosition[0];
			else if( fFrame >= pBone->pRotationFrame[pBone->wPositionNum - 1] )
				*pOutPos = pBone->pPosition[pBone->wPositionNum - 1];
			else
			{
				//	フレームから位置を算出
				for(i = 0; i < pBone->wPositionNum - 1; i++)
				{
					if( fFrame >= pBone->pPositionFrame[i] && fFrame < pBone->pPositionFrame[i + 1] )
					{
						t = (fFrame - (float)pBone->pPositionFrame[i]) / (float)(pBone->pPositionFrame[i + 1] - pBone->pPositionFrame[i]);
						*pOutPos = pBone->pPosition[i] + (pBone->pPosition[i + 1] - pBone->pPosition[i]) * t;
						break;
					}
				}
			}
		}

		//---------------------------------------------------------------------------
		//	スキンメッシュのフレーム更新
		//!	@param wNo [in] ボーン番号
		//!	@param fFrame1 [in] フレーム
		//!	@param fFrame2 [in] フレーム
		//!	@param pOutPose [out] 姿勢
		//!	@param pOutPos [out] 位置
		//---------------------------------------------------------------------------
		void	SkinMesh::UpdateSkinMeshFrame(word wNo, float fFrame1, float fFrame2, Quaternion* pOutPose, Vector3* pOutPos)
		{
			Quaternion Pose1, Pose2;
			Vector3 Pos1, Pos2;

			//	モーションの算出
			UpdateSkinMeshFrame(wNo, fFrame1, &Pose1, &Pos1);

			word wMotion = _wMotion;
			_wMotion = _wBlendMotion;
			UpdateSkinMeshFrame(wNo, fFrame2, &Pose2, &Pos2);
			_wMotion = wMotion;

			//	位置、姿勢の算出
			if( _fBlendPower >= 0 )
			{
				D3DXQuaternionSlerp(&Pose1, &_pBone[wNo].OriginalPose, &Pose1, 1.0f - _fBlendPower);
				D3DXQuaternionSlerp(&Pose2, &_pBone[wNo].OriginalPose, &Pose2, _fBlendPower);
				Pos1 = (_pBone[wNo].OriginalPos - Pos1) * (1.0f - _fBlendPower);
				Pos2 = (_pBone[wNo].OriginalPos - Pos2) * _fBlendPower;
			}
			else
			{
				D3DXQuaternionSlerp(&Pose1, &_pBone[wNo].OriginalPose, &Pose1, 0.5f);
				D3DXQuaternionSlerp(&Pose2, &_pBone[wNo].OriginalPose, &Pose2, 0.5f);
				Pos1 = (_pBone[wNo].OriginalPos - Pos1) * 0.5f;
				Pos2 = (_pBone[wNo].OriginalPos - Pos2) * 0.5f;
			}

			Pose1 = Pose1 - _pBone[wNo].OriginalPose;
			Pose2 = Pose2 - _pBone[wNo].OriginalPose;
			*pOutPose	= _pBone[wNo].OriginalPose + Pose1 + Pose2;
			*pOutPos	= _pBone[wNo].OriginalPos + Pos1 + Pos2;
		}

		//---------------------------------------------------------------------------
		//	モーションの移行
		//---------------------------------------------------------------------------
		void	SkinMesh::UpdateMotionShift(void)
		{
			word i;
			//	モーション移行の進行度算出
			float t = _fFrame / _fChangeFrame;

			for(i = 0; i < _wBoneNum; i++)
			{
				if( _wBlendMotion == 0xFFFF )
					UpdateSkinMeshFrame(i, 0.0f, &_pBone[i].Pose, &_pBone[i].Pos);
				else
					UpdateSkinMeshFrame(i, 0.0f, 0.0f, &_pBone[i].Pose, &_pBone[i].Pos);

				//	現在のボーンの位置と姿勢を算出
				D3DXQuaternionSlerp(&_pBone[i].Pose, &_pBone[i].BeforePose, &_pBone[i].Pose, t);
				_pBone[i].Pos = _pBone[i].BeforePos + (_pBone[i].Pos - _pBone[i].BeforePos) * t;
			}
		}

		//---------------------------------------------------------------------------
		//	モーションの更新
		//---------------------------------------------------------------------------
		void	SkinMesh::UpdateMotion(void)
		{
			word i;
			//	モーションの移行
			if( _fChangeFrame )
				UpdateMotionShift();
			//	モーションの更新
			else if( _wBlendMotion == 0xFFFF )
			{
				for(i = 0; i < _wBoneNum; i++)
					UpdateSkinMeshFrame(i, _fFrame, &_pBone[i].Pose, &_pBone[i].Pos);
			}
			//	ブレンドモーションの更新
			else
			{
				float fFrame = (float)_pMotion[_wBlendMotion].wFrameNum * (_fFrame / (float)_pMotion[_wMotion].wFrameNum);
				for(i = 0; i < _wBoneNum; i++)
					UpdateSkinMeshFrame(i, _fFrame, fFrame, &_pBone[i].Pose, &_pBone[i].Pos);
			}
		}

		//---------------------------------------------------------------------------
		//	ボーンの更新
		//---------------------------------------------------------------------------
		void	SkinMesh::UpdateBone(void)
		{
			for(word i=0; i < _wBoneNum; i++)
			{
				D3DXMatrixRotationQuaternion(&_pBone[i].Matrix, &_pBone[i].Pose);
				_pBone[i].Matrix._41 = _pBone[i].Pos.x;
				_pBone[i].Matrix._42 = _pBone[i].Pos.y;
				_pBone[i].Matrix._43 = _pBone[i].Pos.z;

				if( _pBone[i].wParentIndex != 0xFFFF )
					_pBone[i].Matrix *= _pBone[_pBone[i].wParentIndex].Matrix;
				_pMatrix[i] = _pBone[i].OffsetMatrix * _pBone[i].Matrix;

				if( _fChangeFrame == 0 )
				{
					_pBone[i].BeforePose = _pBone[i].Pose;
					_pBone[i].BeforePos = _pBone[i].Pos;
				}
			}
		}

		//---------------------------------------------------------------------------
		//	スキンメッシュの更新
		//---------------------------------------------------------------------------
		void	SkinMesh::UpdateSkinMesh(void)
		{
			void* pVertexs;
			_pMesh->LockVertexBuffer(0, &pVertexs);
			_pSkinInfo->UpdateSkinnedMesh(_pMatrix, null, _pVertexs, pVertexs);
			_pMesh->UnlockVertexBuffer();
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================