//---------------------------------------------------------------------------
//!
//!	@file	Lib.Network.Client.cpp
//!	@brief	通信関連(クライアント)
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"
#include <winsock2.h>

namespace Lib
{
	namespace Network
	{
		//	WinSockの初期化
		bool	Init(void);
		//	WinSockの解放
		void	Cleanup(void);

		//===========================================================================
		//!	クライアントクラス
		//===========================================================================
		class Client
			: public ClientBase
		{
		public:
			//---------------------------------------------------------------------------
			//!	コンストラクタ
			//---------------------------------------------------------------------------
			Client(void)
			{
			}

			//---------------------------------------------------------------------------
			//!	デストラクタ
			//---------------------------------------------------------------------------
			~Client(void)
			{
				Cleanup();
			}

			//---------------------------------------------------------------------------
			//!	初期化
			//!	@param iPort [in] 使用するポート
			//!	@param iClientMax [in] 最大クライアント接続数
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	Init(char* pIpAddress, int iPort)
			{
				//	WinSockの初期化
				if( !Network::Init() )
					return false;

				// ソケットの作成
				_Socket = socket(AF_INET, SOCK_STREAM, 0);
				if( _Socket == INVALID_SOCKET )
					return false;

				struct sockaddr_in Server;
				// 接続先指定用構造体の準備
				Server.sin_family = AF_INET;
				Server.sin_port = htons(iPort);
				Server.sin_addr.S_un.S_addr = inet_addr(pIpAddress);

				//	サーバに接続
				if( connect(_Socket, (struct sockaddr*)&Server, sizeof(Server)) == SOCKET_ERROR )
						return false;

				fd_set fds;
				//	FD_SET初期化
				FD_ZERO(&fds);
				// 接続待ちする為にソケットをセット
				FD_SET(_Socket, &fds);

				struct timeval tv;
				// 時間設定
				tv.tv_sec = 10;
				tv.tv_usec = 0;

				// 設定された時間まで接続待ちをする
				if( select(0, &fds, null, null, &tv) < 1 )
					return false;

				char cData[32];
				ZeroMemory(cData, sizeof(cData));
				//	クライアントに接続完了を伝える
				Receive(cData, sizeof(cData));

				//	接続出来なければソケットを閉じる
				if( strcmp(cData, "Connected") )
					return false;

				return true;
			}

			//---------------------------------------------------------------------------
			//!	解放
			//---------------------------------------------------------------------------
			void	Cleanup(void)
			{
				//	通信を切断
				shutdown(_Socket, SD_BOTH);
				//	ソケットを閉じる
				closesocket(_Socket);

				//	WinSockの解放
				Network::Cleanup();
			}

			//---------------------------------------------------------------------------
			//!	全員に送信
			//!	@param pData [in] 送信するデータ
			//!	@param iSize [in] データサイズ
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	Send(void* pData, int iSize)
			{
				//	データ送信
				if( send(_Socket, (char*)pData, iSize, 0) < 1 )
					return false;

				return true;
			}

			//---------------------------------------------------------------------------
			//!	受信
			//!	@param pData [out] データの受け皿
			//!	@param iSize [in] サイズ/読み込んだサイズ
			//!	@return 受信したデータサイズ
			//---------------------------------------------------------------------------
			int		Receive(void* pData, int iSize)
			{
				fd_set fds;
				//	FD_SET初期化
				FD_ZERO(&fds);
				// 接続待ちする為にソケットをセット
				FD_SET(_Socket, &fds);

				struct timeval tv;
				// 時間設定
				tv.tv_sec = 0;
				tv.tv_usec = 0;

				// 設定された時間まで接続待ちをする
				if( select(0, &fds, null, null, &tv) < 1 )
					return -1;

				//	データ受信
				iSize = recv(_Socket, (char*)pData, iSize, 0);

				return ( iSize > 0 )? iSize: -1;
			}

		private:
			SOCKET	_Socket;	//!< ソケット
		};

		//---------------------------------------------------------------------------
		//	クライアントクラス生成
		//!	@param ppClient [out] クライアントベースのポインタ
		//!	@param iPort [in] ポート番号
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	Create(ClientBase** ppClient, char* pIpAddress, int iPort)
		{
			Client* pNewClient = new Client;
			*ppClient = pNewClient;

			return pNewClient->Init(pIpAddress, iPort);
		}

		//---------------------------------------------------------------------------
		//	クライアントクラス解放
		//!	@param ppServer [out] クライアントベースのポインタ
		//---------------------------------------------------------------------------
		void	Delete(ClientBase** ppClient)
		{
			SafeDelete(*ppClient);
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================