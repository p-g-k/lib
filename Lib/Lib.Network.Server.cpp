//---------------------------------------------------------------------------
//!
//!	@file	Lib.Network.Server.cpp
//!	@brief	通信関連(サーバー)
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"
#include <winsock2.h>

#pragma comment(lib, "ws2_32.lib")

namespace Lib
{
	namespace Network
	{
		//	カウンター
		int	iUseCount = 0;

		//!	クライアントデータ
		struct ClientData
		{
			bool	bUse;	//!< 使用フラグ
			SOCKET	Socket;	//!< ソケット
		};

		//---------------------------------------------------------------------------
		//!	WinSockの初期化
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	Init(void)
		{
			//	カウンターをインクリメント
			iUseCount++;

			//	既に初期化されている場合は省略
			if( iUseCount > 1 )
				return true;

			//	WSADATA構造体
			WSADATA wsaData;
			int iResult;

			//	WinSock初期化
			iResult = WSAStartup(MAKEWORD(2, 0), &wsaData);
			//	エラー時の処理
			switch( iResult )
			{
				case WSASYSNOTREADY:
					Message("WSASYSNOTREADY ネットワークサブシステムがネットワークへの接続を準備できていない", "エラー");
					return false;
				case WSAVERNOTSUPPORTED:
					Message("WSAVERNOTSUPPORTED 要求されたwinsockのバージョンがサポートされていない", "エラー");
					return false;
				case WSAEPROCLIM:
					Message("WSAEPROCLIM winsockが処理できる最大プロセス数に達した", "エラー");
					return false;
			}

			return true;
		}

		//---------------------------------------------------------------------------
		//!	WinSockの解放
		//---------------------------------------------------------------------------
		void	Cleanup(void)
		{
			iUseCount--;

			//	全ての処理が終わっている場合、解放する
			if( iUseCount == 0 )
				WSACleanup();
		}

		//===========================================================================
		//!	サーバークラス
		//===========================================================================
		class Server
			: public ServerBase
		{
		public:
			//---------------------------------------------------------------------------
			//!	コンストラクタ
			//---------------------------------------------------------------------------
			Server(void)
			{
			}

			//---------------------------------------------------------------------------
			//!	デストラクタ
			//---------------------------------------------------------------------------
			~Server(void)
			{
				Cleanup();
			}

			//---------------------------------------------------------------------------
			//!	初期化
			//!	@param iPort [in] 使用するポート
			//!	@param iClientMax [in] 最大クライアント接続数
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	Init(int iPort, int iClientMax)
			{
				//	WinSockの初期化
				if( !Network::Init() )
					return false;

				//	ソケット作成
				_Socket = socket(AF_INET, SOCK_STREAM, 0);
				if( _Socket == INVALID_SOCKET )
					return false;

				struct sockaddr_in Addr;
				//	ソケットの設定
				Addr.sin_family = AF_INET;
				Addr.sin_port = htons(iPort);
				Addr.sin_addr.S_un.S_addr = INADDR_ANY;
				bind(_Socket, (struct sockaddr*)&Addr, sizeof(Addr));

				//	クライアントからの接続要求を待てる状態にする
				listen(_Socket, iClientMax);

				//	クライアントの格納領域を作成
				ClientData Dummy;
				memset(&Dummy, 0, sizeof(Dummy));
				Dummy.bUse = false;
				_Client.resize(iClientMax, Dummy);

				_iNum = 0;

				return true;
			}

			//---------------------------------------------------------------------------
			//!	解放
			//---------------------------------------------------------------------------
			void	Cleanup(void)
			{
				//	全クライアントを閉じる
				for(int i=0; i < (int)_Client.size(); i++)
					CloseClient(i);

				_Client.clear();

				//	通信を切断
				shutdown(_Socket, SD_BOTH);
				//	ソケットを閉じる
				closesocket(_Socket);

				//	WinSockの解放
				Network::Cleanup();
			}

			//---------------------------------------------------------------------------
			//!	接続待ち開始
			//!	@param iMicroSecond [in] マイクロ秒単位の待ち時間
			//!	@return クライアントの番号(失敗した場合-1を返す)
			//---------------------------------------------------------------------------
			int		Accept(int iMicroSecond)
			{
				fd_set fds;
				//	FD_SET初期化
				FD_ZERO(&fds);
				// 接続待ちする為にソケットをセット
				FD_SET(_Socket, &fds);

				struct timeval tv;
				// 時間設定
				tv.tv_sec = ( iMicroSecond < 1000000 )? 0: iMicroSecond / 1000000;
				tv.tv_usec = ( iMicroSecond >= 1000000 )? iMicroSecond: iMicroSecond % 1000000;

				// 設定された時間まで接続待ちをする
				if( select(0, &fds, null, null, &tv) < 1 )
					OutputDebugString("接続待ちタイムアウト\n");
				else
				{
					ClientData Client;
					Client.bUse = true;
					struct sockaddr_in ClientAddress;
					int iLength = sizeof(ClientAddress);
					//	TCPクライアントからの接続要求を受け付ける
					Client.Socket = accept(_Socket, (struct sockaddr*)&ClientAddress, &iLength);

					if( Client.Socket != INVALID_SOCKET )
					{
						int i = 0;
						//	未使用の領域を検索
						while( _Client[i].bUse )
							i++;

						//	クライアントをセット
						_Client[i] = Client;

						char* pData = "Connected";
						//	クライアントに接続完了を伝える
						Send(i, pData, (int)strlen(pData) + 1); 
						_iNum++;

						return i;
					}
				}

				return -1;
			}

			//---------------------------------------------------------------------------
			//!	接続を切る
			//!	@param iNo [in] 番号
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	CloseClient(int iNo)
			{
				//	接続数を上回っている場合失敗
				if( iNo >= (int)_Client.size() )
					return false;

				//	通信を切断
				shutdown(_Client[iNo].Socket, SD_BOTH);
				//	ソケットを閉じる
				closesocket(_Client[iNo].Socket);

				//	iNo番を未使用にする
				_Client[iNo].bUse = false;

				_iNum--;

				return true;
			}

			//---------------------------------------------------------------------------
			//!	全員に送信
			//!	@param pData [in] 送信するデータ
			//!	@param iSize [in] データサイズ
			//---------------------------------------------------------------------------
			void	Send(void* pData, int iSize)
			{
				for(int i=0; i < (int)_Client.size(); i++)
				{
					if( _Client[i].bUse )
						Send(i, pData, iSize);
				}
			}

			//---------------------------------------------------------------------------
			//!	指定した相手に送信
			//!	@param iNo [in] 番号
			//!	@param pData [in] 送信するデータ
			//!	@param iSize [in] データサイズ
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	Send(int iNo, void* pData, int iSize)
			{
				//	データ送信
				if( send(_Client[iNo].Socket, (char*)pData, iSize, 0) < 1 )
					return false;

				return true;
			}

			//---------------------------------------------------------------------------
			//!	受信
			//!	@param pData [out] データの受け皿
			//!	@param pSize [in/out] サイズ/読み込んだサイズ
			//!	@return 受信したクライアントの番号(失敗した場合-1を返す)
			//---------------------------------------------------------------------------
			int		Receive(void* pData, int* pSize)
			{
				int iSize = *pSize;
				for(int i = 0; i < (int)_Client.size(); i++)
				{
					//	読み込み
					*pSize = Receive(i, pData, iSize);
					//	読み込めていなければ次へ進める
					if( *pSize < 0 )
						continue;

					return i;
				}

				return -1;
			}

			//---------------------------------------------------------------------------
			//!	指定した相手から受信
			//!	@param iNo [in] 番号
			//!	@param pData [out] データの受け皿
			//!	@param iSize [in] サイズ
			//!	@return 受信したクライアントの番号(失敗した場合-1を返す)
			//---------------------------------------------------------------------------
			int		Receive(int iNo, void* pData, int iSize)
			{
				fd_set fds;
				//	FD_SET初期化
				FD_ZERO(&fds);
				// 接続待ちする為にソケットをセット
				FD_SET(_Client[iNo].Socket, &fds);

				struct timeval tv;
				// 時間設定
				tv.tv_sec = 0;
				tv.tv_usec = 0;

				// 設定された時間まで接続待ちをする
				if( select(0, &fds, null, null, &tv) < 1 )
					return -1;

				//	送信
				iSize = recv(_Client[iNo].Socket, (char*)pData, iSize, 0);

				return ( iSize > 0 )? iSize: -1;
			}

			//---------------------------------------------------------------------------
			//!	クライアント数の取得
			//!	@return クライアント数
			//---------------------------------------------------------------------------
			int		GetClientNum(void)
			{
				return _iNum;
			}

		private:
			SOCKET					_Socket;	//!< ソケット
			std::vector<ClientData>	_Client;	//!< クライアント
			int						_iNum;		//!< クライアント数	
		};

		//---------------------------------------------------------------------------
		//	サーバークラス生成
		//!	@param ppServer [out] サーバーベースのポインタ
		//!	@param iPort [in] ポート番号
		//!	@param iClientMax [in] 最大クライアント数
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	Create(ServerBase** ppServer, int iPort, int iClientMax)
		{
			Server* pNewServer = new Server;
			*ppServer = pNewServer;

			return pNewServer->Init(iPort, iClientMax);
		}

		//---------------------------------------------------------------------------
		//	サーバークラス解放
		//!	@param ppServer [out] サーバーベースのポインタ
		//---------------------------------------------------------------------------
		void	Delete(ServerBase** ppServer)
		{
			SafeDelete(*ppServer);
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================